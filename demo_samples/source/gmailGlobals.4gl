GLOBALS
  DEFINE userid, search STRING
  DEFINE ses WEB.SESSION
  DEFINE url web.URL
  DEFINE rs WEB.Response
  DEFINE folders DYNAMIC ARRAY OF RECORD
                                    id, name STRING
                                  END RECORD

  DEFINE emails DYNAMIC ARRAY OF RECORD
                                   id, threadid, title, message STRING
                                 END RECORD
END GLOBALS