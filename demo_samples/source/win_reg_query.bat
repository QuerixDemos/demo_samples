

echo "Registry Information" > regrep.txt
echo "" >> regrep.txt

echo "****************************************************" >> regrep.txt
echo "*** HKEY_CURRENT_USER\Software\Querix            ***" >> regrep.txt
echo "****************************************************" >> regrep.txt

REG QUERY "HKEY_CURRENT_USER\Software\Querix" /s >> regrep.txt


echo "****************************************************" >> regrep.txt
echo "*** HKEY_CURRENT_USER\Environment                ***" >> regrep.txt
echo "****************************************************" >> regrep.txt

REG QUERY "HKEY_CURRENT_USER\Environment" /s >> regrep.txt

echo "****************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Querix           ***" >> regrep.txt
echo "****************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Querix" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Informix           ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Informix" /s >> regrep.txt


echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Google             ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Google" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\GitForWindows      ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\GitForWindows" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Oracle            ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Oracle" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\PostgreSQL         ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\PostgreSQL" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\MySQL AB           ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\MySQL AB" /s >> regrep.txt


echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\GitForWindows      ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\GitForWindows" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\IBM                ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\IBM" /s >> regrep.txt


echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\RegisteredApplications ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\RegisteredApplications" /s >> regrep.txt



echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer"  ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer" >> regrep.txt



echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Install Check" /s ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Install Check" /s >> regrep.txt


echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Installed Components" /s ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Installed Components" /s >> regrep.txt

echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\ODBC ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC" /s >> regrep.txt



echo "" >> regrep.txt
echo "******************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\ODBCINST.INI       ***" >> regrep.txt
echo "******************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\ODBCINST.INI" /s >> regrep.txt


echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft /s           ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

REG QUERY "HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft" /s >> regrep.txt




echo "*********************************************************" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET COMMANDS                                      ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*********************************************************" >> regrep.txt


echo "User Information" >> regrep.txt
echo "" >> regrep.txt

echo "****************************************************" >> regrep.txt
echo "*** net user %username%                          ***" >> regrep.txt
echo "****************************************************" >> regrep.txt

net user %username%

echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET VIEW                                          ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET VIEW >> regrep.txt

echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET ACCOUNTS                                      ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET ACCOUNTS >> regrep.txt

NET VIEW >> regrep.txt

echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET START                                         ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET START >> regrep.txt



echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET USE                                           ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET USE >> regrep.txt


echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET USER                                          ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET USER >> regrep.txt


echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** NET USER %USERNAME%                               ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

NET USER %USERNAME% >> regrep.txt


echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** sc query qxweb                                    ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

sc query qxweb >> regrep.txt



echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** sc query mpssvc                                   ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

sc query mpssvc >> regrep.txt



echo "" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*** IPCONFIG ALL                                      ***" >> regrep.txt
echo "*********************************************************" >> regrep.txt

ipconfig /all >> regrep.txt


echo "*********************************************************" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*********************************************************" >> regrep.txt
echo "*********************************************************" >> regrep.txt
dir %LYCIA_CONFIG_PATH% >> regrep.txt
dir %LYCIA_DIR% >> regrep.txt


