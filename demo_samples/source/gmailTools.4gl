GLOBALS "gmailGlobals.4gl"

FUNCTION sendEmail(to, subject, body)
  DEFINE to, subject, body STRING
  DEFINE rq WEB.Request
  
  RETURN
  
  CAll url.setPath("gmail/v1/users/" || userid || "/messages/send")                  
  CALL rq.header("Authorization", fgl_getenv("OAUTH_TOKEN_TYPE") || " " || fgl_getenv("OAUTH_ACCESS_TOKEN"))  
  CALL rq.setMethod("PUT")
  CAll rq.setURL(url)
  
  -- TODO: make correct request for sending email
  
  CALL ses.perform(rq) RETURNING rs
  
END FUNCTION

FUNCTION getHeader(headers, name)
  DEFINE name STRING
  DEFINE i INT
  DEFINE headers util.JSONArray
  FOR i = 1 TO headers.getLength()
    IF headers.get(i).get("name") = name THEN
      RETURN headers.get(i).get("value")
    END IF 
  END FOR
  RETURN ""
END FUNCTION

FUNCTION displayEmailBody(html)
  DEFINE html STRING  
  DEFINE txt TEXT  
  LOCATE txt IN FILE "browser.html" 
  LET txt = html
  DISPLAY "qx://application/browser.html" TO message 
END FUNCTION

FUNCTION loadEmailBody(i)
  DEFINE i INT
  DEFINE rq WEB.Request
  DEFINE parts util.JSONArray
  DEFINE response util.JSONObject
  DEFINE data STRING
  
  IF emails[i].message.getLength() > 0 THEN
    CALL displayEmailBody(emails[i].message)
    RETURN
  END IF
  
  CALL displayEmailBody("")
  
  IF emails[i].id.subString(1, 5) = "node_" THEN
    RETURN
  END IF
  
  CAll url.setPath("gmail/v1/users/" || userid || "/messages/" || emails[i].id)                  
  CALL rq.header("Authorization", fgl_getenv("OAUTH_TOKEN_TYPE") || " " || fgl_getenv("OAUTH_ACCESS_TOKEN"))  
  CALL rq.setMethod("GET")
  CAll rq.setURL(url)
  
  CALL ses.perform(rq) RETURNING rs
  
  IF rs.GetHTTPCode() == 200 THEN
    CALL util.JSON.parse(rs.getBody(), response)
    LET parts = response.get("payload").get("parts")
    LET data = parts.get(2).get("body").get("data")
    LET emails[i].message = util.Strings.base64URLDecodeToString(data)
    CALL displayEmailBody(emails[i].message)
  ELSE    
    DISPLAY "URL : ", url.getURL()
    DISPLAY rs.GetHTTPCode() || ": " || rs.getBody()
  END IF
  
END FUNCTION

FUNCTION loadEmailTitle(i)
  DEFINE i INT
  DEFINE rq WEB.Request
  DEFINE response util.JSONObject
  DEFINE headers util.JSONArray
  DEFINE metadataHeaders DYNAMIC ARRAY OF STRING
  
  CAll url.setPath("gmail/v1/users/" || userid || "/messages/" || emails[i].id)                  
  CALL rq.header("Authorization", fgl_getenv("OAUTH_TOKEN_TYPE") || " " || fgl_getenv("OAUTH_ACCESS_TOKEN"))  
  CALL rq.setMethod("GET")
  CALL rq.param("format", "metadata")
  CALL rq.param("metadataHeaders", "From")
  CALL rq.param("metadataHeaders", "To")
  CALL rq.param("metadataHeaders", "Date")
  CALL rq.param("metadataHeaders", "Subject")
  CAll rq.setURL(url)
  
  CALL ses.perform(rq) RETURNING rs
  
  IF rs.GetHTTPCode() == 200 THEN
    CALL util.JSON.parse(rs.getBody(), response)
    LET headers = response.get("payload").get("headers")
    LET emails[i].title = "From\t: " || getHeader(headers, "From") || "\n" ||
                          "To\t\t: " || getHeader(headers, "To") || "\n" ||
                          "Date\t: " || getHeader(headers, "Date") || "\n" ||
                          "Subj\t: " || getHeader(headers, "Subject")
  ELSE    
    DISPLAY "URL : ", url.getURL()
    DISPLAY rs.GetHTTPCode() || ": " || rs.getBody()
  END IF
END FUNCTION

FUNCTION loadEmails(folder)
  DEFINE folder STRING
  DEFINE rq WEB.Request
  DEFINE i, k INT
  DEFINE response, email util.JSONObject
  DEFINE messages util.JSONArray
    
  CAll url.setPath("gmail/v1/users/" || userid || "/messages")                  
  CALL rq.header("Authorization", fgl_getenv("OAUTH_TOKEN_TYPE") || " " || fgl_getenv("OAUTH_ACCESS_TOKEN"))  
  CALL rq.setMethod("GET")
  CALL rq.param("labelIds", folder)
  CALL rq.param("maxResults", 20)
  DISPLAY search
  CALL rq.param("q", search)
  CAll rq.setURL(url)
  
  CALL ses.perform(rq) RETURNING rs
  
  CALL emails.clear()
  
  IF rs.GetHTTPCode() == 200 THEN
    CALL util.JSON.parse(rs.getBody(), response)
    LET messages = response.get("messages")
    FOR i = 1 TO messages.getLength()
      LET email = messages.get(i)
      LET emails[i].id = email.get("id")
      LET emails[i].threadid = email.get("threadId")
      CALL loadEmailTitle(i)
    END FOR    
    CALL sortEmails(1)
  ELSE    
    DISPLAY "URL : ", url.getURL()
    DISPLAY rs.GetHTTPCode() || ": " || rs.getBody()
  END IF

END FUNCTION

FUNCTION loadLabels()
  DEFINE rq WEB.Request
  DEFINE i,t INT  
  DEFINE response, folder util.JSONObject
  DEFINE labels util.JSONArray
  DEFINE str STRING
  {
  CALL folders.Append([ "INBOX", "Inbox" ] )
  CALL folders.Append([ "SENT", "Sent" ] )
  CALL folders.Append([ "UNREAD", "Unread" ] )
  CALL folders.Append([ "DRAFT", "Draft" ] )
  CALL folders.Append([ "STARRED", "Starred" ] )
  CALL folders.Append([ "SPAM", "Spam" ] )
  CALL folders.Append([ "TRASH", "Trash" ] )
  RETURN
  }  
  CAll url.setPath("gmail/v1/users/" || userid || "/labels")                  
  CALL rq.header("Authorization", fgl_getenv("OAUTH_TOKEN_TYPE") || " " || fgl_getenv("OAUTH_ACCESS_TOKEN"))  
  CALL rq.setMethod("GET")
  CAll rq.setURL(url)
  
  CALL ses.perform(rq) RETURNING rs
  
  IF rs.GetHTTPCode() == 200 THEN
    CALL util.JSON.parse(rs.getBody(), response)
    LET labels = response.get("labels")
    FOR i = 1 TO labels.getLength()
      LET folder = labels.get(i)
      LET str = folder.get("messageListVisibility")
      --IF str <> "hide" THEN
        CALL folders.Append([ folder.get("id"), folder.get("name") ] )
      --END IF
    END FOR
  ELSE    
    DISPLAY "URL : ", url.getURL()
    DISPLAY rs.GetHTTPCode() || ": " || rs.getBody()
  END IF
END FUNCTION

FUNCTION sortEmails(i)
  DEFINE i, k INT
  DEFINE idx DYNAMIC ARRAY OF INT
  DEFINE threadid STRING
  
  IF i > emails.getSize() THEN
    RETURN
  END IF
  
  LET threadid = emails[i].threadid
  
  IF threadid = "" THEN
    CALL sortEmails(i + 1)
    RETURN
  END IF
  
  FOR k = i + 1 TO emails.getSize()
    IF emails[k].threadid = threadid THEN
      CALL idx.Append(k)
    END IF
  END FOR
  IF idx.getSize() = 0 THEN
    LET emails[i].threadid = ""
    CALL sortEmails(i + 1)
  ELSE
    LET threadid = "node_" || emails[i].id
    CALL emails.insert(i, [threadid, "", emails[i].title, ""])
    LET emails[i + 1].threadid = threadid
    FOR k = 1 TO idx.getSize()
      CALL emails.insert(i + 1 + k, emails[idx[k] + 1])
      LET emails[i + 1 + k].threadid = threadid
      CALL emails.delete(idx[k] + 2)
    END FOR
    CALL sortEmails(i + idx.getSize() + 2)
  END IF
END FUNCTION