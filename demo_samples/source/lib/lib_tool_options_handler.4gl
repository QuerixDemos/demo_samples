################################################################################
# FUNCTION handle_dirty_exit()
#
#
################################################################################ 
FUNCTION handle_dirty_exit()
	#User forced the application to close down
	#ROLLBACK WORK
	#DISCONNECT ALL
	CALL errorlog("Client Side Termination")
	EXIT PROGRAM 1
END FUNCTION
################################################################################
# END FUNCTION handle_dirty_exit()
################################################################################ 