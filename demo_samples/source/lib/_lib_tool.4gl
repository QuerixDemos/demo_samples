###########################################################################
# Module Scope Variables
###########################################################################
DEFINE ml_db VARCHAR(30)  --DB name
DEFINE ml_db_set BOOLEAN --true = variable initialized/set false=not initialized


###########################################################################
# FUNCTION ui_init()
#
#
###########################################################################
FUNCTION ui_init()
	DEFINE pOptions SMALLINT
	
	CALL processUrlArguments()
	CALL initToolbar()
	OPTIONS FIELD ORDER UNCONSTRAINED
	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
END FUNCTION



###########################################################################
# FUNCTION get_db()
#
# Accessor Method for ml_db_set = status, if DB was set
###########################################################################
FUNCTION get_db()
	
	IF ml_db_set = FALSE THEN
		IF fgl_getenv("DB")	 IS NULL THEN
			#CALL fgl_winmessage("DB not specified","You need to specify the value of DB (Database Name)\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_db(trim(fgl_getenv("DB")))	
		END IF
	END IF

	RETURN ml_db
END FUNCTION



###########################################################################
# FUNCTION get_db_set()
#
# Accessor Method for ml_db_set  = status, if DB was set
###########################################################################
FUNCTION get_db_set()
		RETURN ml_db_set
END FUNCTION




###########################################################################
# FUNCTION set_db(p_db)
#
# Accessor Method for ml_db 
# DEFINE ml_db VARCHAR(30)  --db name
# DEFINE ml_db_set BOOLEAN --true = variable initialized/set false=not initialized
###########################################################################
FUNCTION set_db(p_db)
	DEFINE p_db STRING

	IF p_db IS NOT NULL THEN
		LET ml_db = p_db
		CALL fgl_setenv("DB",ml_db)
		LET ml_db_set = TRUE
	END IF
END FUNCTION



	