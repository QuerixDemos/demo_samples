######################################################################################
# MODULE SCOPE Variables
######################################################################################
	DEFINE modu_help_initialized BOOLEAN
	
	DEFINE modu_rec_help_page DYNAMIC ARRAY OF RECORD
		pageId STRING,
		baseFolderID1 STRING,
		baseFolderID2 STRING,
		pageUrl STRING
		END RECORD

	DEFINE modu_rec_help_element DYNAMIC ARRAY OF RECORD
		pageId STRING,  --PK
		elementId STRING, --PK
		elementUrl STRING
		END RECORD

######################################################################################
# FUNCTION onlineHelp(p_page_Id,p_element_id)
#
#
######################################################################################
FUNCTION onlineHelp(p_page_Id,p_element_id)
	DEFINE p_page_Id STRING
	DEFINE p_element_id STRING
	DEFINE l_helpUrl STRING	
#Note: we need to bring this to a DB lookup
#Have to speak to Eugenia if I can get some kind of table of URLs and ElementIds
#Also, if they are static or keep changing

	LET p_page_Id = p_page_Id.toUpperCase()

	#just temporary until we use DB 
	IF modu_help_initialized = FALSE THEN
		CALL initTableDataHelpPage()
		CALL initTableDataHelpElement()
		LET modu_help_initialized = TRUE
	END IF	

	#direct to the website or show window to choose help links.. (for multiple topics in one demo)
	
	IF p_page_Id IS NULL THEN
		CALL winHelp(p_element_id)	
	ELSE

		LET l_helpUrl = getBaseUrl()
		LET l_helpUrl = l_helpUrl, getDBPageUrl(p_page_Id)
		LET l_helpUrl = l_helpUrl, getDBElementUrl(p_page_Id, p_element_id)
		#DISPLAY "->", l_helpUrl, "<-"	
		#CALL WinShellExec(l_helpUrl)
		call ui.Interface.frontCall("standard","launchurl",[l_helpUrl],[])
	END IF	
		
END FUNCTION		
######################################################################################
# END FUNCTION onlineHelp(p_page_Id,p_element_id)
######################################################################################


######################################################################################
# FUNCTION onlineHelpLookupUrl(p_help_id,p_element_id)
#
#
######################################################################################		
FUNCTION onlineHelpLookupUrl(p_help_id,p_element_id)
	DEFINE p_help_id,p_element_id STRING
	DEFINE l_module_id STRING

	#getDBUrl(p_help_id)


END FUNCTION
######################################################################################
# END FUNCTION onlineHelpLookupUrl(p_help_id,p_element_id)
######################################################################################		


######################################################################################
# FUNCTION initTableDataHelpPage()
#
#
######################################################################################
FUNCTION initTableDataHelpPage()
	DEFINE l_idx INT
#	DEFINE recHelp DYNAMIC ARRAY OF RECORD
#		helpId STRING,
#		baseFolderID STRING,
#		pageUrl STRING
#		END RECORD
		
#http://querix.com/go/maia/Content/ap/p1/p11.htm?Highlight=P11
	
	LET l_idx = 1	
	LET modu_rec_help_page[l_idx].pageId = "GroupBox"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/groupbox.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "BorderPanel"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/borderpanel.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "CoordinatePanel"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/coordpanel.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "GridPanel"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/gridpanel.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "StackPanel"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/stackpanel.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Tab"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/tab.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "ScrollViewer"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/scrollviewer.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Table"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/table_container.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "TreeTable"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/containers/treetable.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "fgl_winprompt"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/functions/Dialog_box/fgl_winprompt.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "fgl_winquestion"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/functions/Dialog_box/fgl_winquestion.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "fgl_winmessage"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/functions/Dialog_box/fgl_winmessage.htm"
		
	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Label"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/label.htm"
	
	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "TextField"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/textfield.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Calendar"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/calendar.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Timeedit"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/timeeditfield.htm"
	
	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Button"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/button.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Checkbox"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/checkbox.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Combobox"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/combobox.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Listbox"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/listbox.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Progressbar"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/progressbar.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Radiobutton"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/radiobutton.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Scrollbar"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/scrollbar.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "TextArea"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/textarea.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Webcomponent"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/webcomponent.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "Browser"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/browser.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "FormPropertyEvent"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/properties/events.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "OPTIONS"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/statements/OPTIONS/options.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "fgl_dialog_fieldorder()"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/functions/Input/fgl_dialog_fieldorder.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "form-tabindex"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/properties/tabindex.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "form-required"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/properties/field/required.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "options-field-order"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/statements/OPTIONS/cursor_movement_settings.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "input-without-defaults"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/statements/INPUT/Binding_clause/without_defaults_for_input.htm"	

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "spinner"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/ui/forms/widgets/spinner.htm"	
	
	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "drag-and-drop"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/statements/DISPLAY_ARRAY/Control_clause/drag_drop_control_clause/drag_drop_for_display_array.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "file_dialog"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/functions/dialog_box/fgl_file_dialog.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "StringBuffer"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "Default.htm#06_reference/4gl/methods/base/base_stringbuffer/base.stringbuffer.htm"

	LET l_idx = l_idx+1
	LET modu_rec_help_page[l_idx].pageId = "JSON_Object"
	LET modu_rec_help_page[l_idx].baseFolderID1 = ""
	LET modu_rec_help_page[l_idx].baseFolderID2 = ""
	LET modu_rec_help_page[l_idx].pageUrl = "index.htm#09_auxiliary/json/util_jsonobject/util.jsonobject_class.htm"


END FUNCTION	
######################################################################################
# END FUNCTION initTableDataHelpPage()
######################################################################################


######################################################################################
# FUNCTION initTableDataHelpElement()
#
#
######################################################################################
FUNCTION initTableDataHelpElement()
	DEFINE l_idx INT
#	DEFINE recHelp DYNAMIC ARRAY OF RECORD
#		pageId STRING,  --PK
#		elementId STRING, --PK
#		elementUrl STRING
#		END RECORD
			
	#LET l_idx = 1	
	#LET modu_rec_help_element[l_idx].pageId = "P11"
	#LET modu_rec_help_element[l_idx].elementId = "ap"
	#LET modu_rec_help_element[l_idx].elementUrl = "#Huho"
#
#	LET l_idx = l_idx+1

END FUNCTION
######################################################################################
# END FUNCTION initTableDataHelpElement()
######################################################################################


######################################################################################
# FUNCTION getDBPageUrl(p_page_Id)  --DB / needs replacing / removing
#
#
######################################################################################
FUNCTION getDBPageUrl(p_page_Id)  --DB / needs replacing / removing
	DEFINE p_page_Id STRING
	DEFINE l_idx INT
	DEFINE urlPageSegment STRING
	#LET p_page_Id = p_page_Id.toUpperCase()
	FOR l_idx = 1 TO modu_rec_help_page.getsize()
		IF modu_rec_help_page[l_idx].pageId.toUpperCase() = p_page_Id.toUpperCase() THEN

			LET urlPageSegment =	modu_rec_help_page[l_idx].baseFolderID1, "/"
			
			IF  modu_rec_help_page[l_idx].baseFolderID2 IS NOT NULL THEN
				LET urlPageSegment =	urlPageSegment, trim(modu_rec_help_page[l_idx].baseFolderID2),	"/"
			END IF
			
			LET urlPageSegment =	urlPageSegment, trim(modu_rec_help_page[l_idx].pageUrl)
			
			RETURN urlPageSegment
		END IF
	END FOR

END FUNCTION	
######################################################################################
# END FUNCTION getDBPageUrl(p_page_Id)  --DB / needs replacing / removing
######################################################################################


######################################################################################
# FUNCTION getDBElementUrl(p_page_Id, p_element_id)  --DB / needs replacing / removing
#
#
######################################################################################
FUNCTION getDBElementUrl(p_page_Id, p_element_id)  --DB / needs replacing / removing
	DEFINE p_page_Id STRING
	DEFINE p_element_id STRING
	DEFINE l_idx INT
	DEFINE urlElementSegment STRING
			
	FOR l_idx = 1 TO modu_rec_help_page.getsize()
		IF		modu_rec_help_element[l_idx].pageId = p_page_Id --PK
			AND modu_rec_help_element[l_idx].elementId = p_element_id THEN --PK

			LET urlElementSegment = "#", modu_rec_help_element[l_idx].elementUrl
			RETURN urlElementSegment
		END IF
	END FOR

END FUNCTION	
######################################################################################
# END FUNCTION getDBElementUrl(p_page_Id, p_element_id)  --DB / needs replacing / removing
######################################################################################


######################################################################################
# FUNCTION getBaseUrl()
#
#
######################################################################################
#needs to be in a setings/config db table
FUNCTION getBaseUrl()
	DEFINE l_help_url STRING
	LET l_help_url = fgl_getenv("HELP_URL")  --inetEnv: HELP_URL=http://querix.com/go/lycia/Default.htm

	IF l_help_url IS NULL THEN
		LET l_help_url = "http://querix.com/go/lycia/" 
	END IF
	
	RETURN l_help_url
END FUNCTION	
######################################################################################
# END FUNCTION getBaseUrl()
######################################################################################


######################################################################################
# FUNCTION winHelp(p_group_help)	
#
#
######################################################################################
FUNCTION winHelp(p_group_help)		
	DEFINE p_group_help STRING
	DEFINE menuSelection SMALLINT
	DEFINE listBox STRING
	DEFINE lbx ui.ListBox #Define a variable of dataType ui.ListBox
	DEFINE l_helpUrl STRING
	
	OPEN WINDOW wWinHelp WITH FORM "form/help_win" ATTRIBUTE(BORDER, STYLE="CENTER")
	LET lbx = ui.ListBox.ForName("listBox") #Bind this variable to an ui element in the current window-form
	
	LET p_group_help = p_group_help.toUpperCase()

	CALL populateWinHelp(p_group_help,lbx)
	
	WHILE TRUE
	
		INPUT BY NAME listBox WITHOUT DEFAULTS
			#BEFORE INPUPT
			# We keep the help list open until the user selects cancel
			#	CALL dialog.setActionHidden("ACCEPT",TRUE)
			#	CALL fgl_dialog_setkeylabel("CACNEL","")
				
			ON ACTION "ACCEPT"  #, "doubleclick")
					LET l_helpUrl = getBaseUrl()
					LET l_helpUrl = l_helpUrl, listBox
					CALL ui.Interface.frontCall("standard","launchurl",[l_helpUrl],[])

			ON ACTION "doubleclick"
					LET l_helpUrl = getBaseUrl()
					LET l_helpUrl = l_helpUrl, listBox
					CALL ui.Interface.frontCall("standard","launchurl",[l_helpUrl],[])
				
			ON ACTION "CANCEL"
				EXIT WHILE
				
		END INPUT
			
	END WHILE
	
	CLOSE WINDOW wWinHelp	

END FUNCTION
######################################################################################
# END FUNCTION winHelp(p_group_help)	
######################################################################################


######################################################################################
# FUNCTION populateWinHelp(p_group_help,p_Lbx)
#
#
######################################################################################
FUNCTION populateWinHelp(p_group_help,p_Lbx)
	DEFINE p_group_help STRING
	DEFINE p_Lbx ui.ListBox #Define a variable of dataType ui.ListBox	

	LET p_group_help = p_group_help.toLowerCase()

	CASE p_group_help
		WHEN "environment"
			CALL p_Lbx.AddItem("Default.htm#04_configuration/02_env_vars/env_var.htm",
												"Lycia Environment Variables")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Environment/fgl_getenv.htm",
												"fgl_getenv()")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Environment/fgl_setenv.htm",
												"fgl_setenv()")

			CALL p_Lbx.AddItem("index.htm#04_configuration/00_configuration.htm",
												"Lycia Configuration")


		WHEN "listbox"
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/widgets/listbox.htm",
												"ListBox")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/widgets/listboxitem.htm",
												"ListBoxItem")

		WHEN "tabcontainer"
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/containers/tab.htm",
												"Tab")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/containers/tabpage.htm",
												"TabPage")

		WHEN "tableall"
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/containers/table.htm",
												"Table")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/properties/ismultiselect.htm",
												"isMultiSelect")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_dialog/field_manipulation_methods/ui.dialog.isrowselected.htm",
												"ui.Dialog.isRowSelected()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/l1/Form_specification_file/INSTRUCTIONS/legacy_grid_array.htm",
												"PER Form -> SCREEN GRID")
	
		WHEN "help"
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_interface/ui.interface.frontcall/standard.htm?Highlight=launchurl",
												"Online Help using ui.interface.frontcall()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Help/showhelp.htm?Highlight=showhelp",
												"Classic 4GL function showhelp()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/statements/OPTIONS/help_file.htm?Highlight=help file",
												"Classic 4GL Help File Reference")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/statements/MENU/Control_clause/help_for_menu.htm?Highlight=help clause",
												"Classic 4GL HELP clause")
												
		WHEN "windowdecoration"
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Window/fgl_settitle.htm?Highlight=fgl_settitle",
												"fgl_settitle()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_interface/ui.interface.setimage.htm?Highlight=setimage",
												"ui.Interface.setImage()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_interface/ui.interface.settext.htm?Highlight=settext",
												"ui.interface.settext()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_window/ui.window.settext.htm?Highlight=settext",
												"ui.window.settext()")
												

		WHEN "input"
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/statements/INPUT/input.htm",
												"INPUT statement")	
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/statements/OPTIONS/options.htm",
												"OPTIONS")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Input/fgl_dialog_fieldorder.htm?Highlight=fgl_dialog_fieldorder",
												"fgl_dialog_fieldorder()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/functions/Input/input_funcs.htm",
												"Input functions")
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/properties/field/required.htm",
												"Form Field property <required=boolean>")							


		WHEN "util_regex"
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/regular_expressions/regular_expressions.htm",
												"Regular Expressions")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/regular_expressions/regex_characters.htm",
												"RegEx characters")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/regular_expressions/regex_algorithms.htm",
												"RegEx algorithms")
		
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/properties/ismultiselect.htm",
												"isMultiSelect")
			CALL p_Lbx.AddItem("Default.htm#06_reference/4gl/methods/ui/ui_dialog/field_manipulation_methods/ui.dialog.isrowselected.htm",
												"ui.Dialog.isRowSelected()")
			CALL p_Lbx.AddItem("Default.htm#06_reference/ui/forms/l1/Form_specification_file/INSTRUCTIONS/legacy_grid_array.htm",
												"PER Form -> SCREEN GRID")
												
												
		OTHERWISE
			CALL fgl_winmessage("p_group_help","Invalid argument passed to winHelp()","error")
	END CASE
	
END FUNCTION
######################################################################################
# END FUNCTION populateWinHelp(p_group_help,p_Lbx)
######################################################################################