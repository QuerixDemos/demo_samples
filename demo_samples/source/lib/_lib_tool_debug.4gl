###########################################################################
# Module Scope Variables
###########################################################################
DEFINE ml_debug SMALLINT  --vdom = 1 or 0
DEFINE ml_debug_set BOOLEAN --true = variable initialized/set false=not initialized

###########################################################################
# FUNCTION get_debug_set()
#
# Accessor Method for ml_debug_set  = status, if debug was set
###########################################################################
FUNCTION get_debug_set()
		RETURN ml_debug_set
END FUNCTION


###########################################################################
# FUNCTION get_debug()
#
# Accessor Method for ml_debug_set = status, if DEBUG was set
###########################################################################
FUNCTION get_debug()
	
	IF ml_debug_set = FALSE THEN
		IF fgl_getenv("DEBUG")	 IS NULL THEN
			#CALL fgl_winmessage("DB not specified","You need to specify the value of DB (Database Name)\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_debug(trim(fgl_getenv("DEBUG")))	
		END IF
	END IF

	RETURN ml_debug
END FUNCTION


###########################################################################
# FUNCTION set_debug(p_debug)
#
# Accessor Method for ml_debug 
###########################################################################
FUNCTION set_debug(p_debug)
	DEFINE p_debug SMALLINT

	IF p_debug = 0 OR p_debug = 1 THEN
		LET ml_debug = p_debug
		CALL fgl_setenv("DEBUG",ml_debug)
		LET ml_debug_set = TRUE		
	END IF
END FUNCTION