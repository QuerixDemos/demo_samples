
###########################################################################################################
# FUNCTION hideStringPartial(argStr)
# 
# take string i.e. password and only keep first and last letter. remaining letters will be shown as asterix *
# i.e. Treehouse -> T*******e
###########################################################################################################
FUNCTION hideStringPartial(argStr)
	DEFINE argStr STRING
	DEFINE strLength, i smallint
	DEFINE retStr STRING
	
	LET strLength = argStr.getLength()
	IF strLength > 0 THEN
		FOR i = 1 TO strLength
			LET retStr = retStr.append("*")  --[i] = "*"	
		END FOR
		
		LET retStr[1] = argStr[1]
		LET retStr[strLength] = argStr[strLength]
	END IF
		
	RETURN retStr
	
END FUNCTION
