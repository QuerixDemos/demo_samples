###########################################################################
# Module Scope Variables
###########################################################################
DEFINE ml_argStr1 VARCHAR(50)  -- 1st program string argument
DEFINE ml_argStr1_set BOOLEAN --true = variable initialized/set false=not initialized
DEFINE ml_argStr2 VARCHAR(50)  -- 2nd program string argument
DEFINE ml_argStr2_set BOOLEAN --true = variable initialized/set false=not initialized
DEFINE ml_argInt1 VARCHAR(50)  -- 1st program integer argument
DEFINE ml_argInt1_set BOOLEAN --true = variable initialized/set false=not initialized
DEFINE ml_argInt2 VARCHAR(50)  -- 2nd program integer argument
DEFINE ml_argInt2_set BOOLEAN --true = variable initialized/set false=not initialized

###########################################################################
# FUNCTION processUrlArguments()
#
#
###########################################################################
FUNCTION processUrlArguments()
	DEFINE i,s SMALLINT
	DEFINE argSegment, argVar, argVal STRING
	DEFINE workingSide CHAR
	DEFINE tempStr STRING
		
	IF num_args() = 0 THEN   --if there are no arguments, only set NONE VDOM as the default - otherwise, don't do anything
		CALL set_vDom(0)
	ELSE   
		FOR i = 1 TO num_args()	--for each argument
			LET argSegment = ""
			LET argVar = ""
			LET argVal = ""
			
			LET argSegment = arg_val(i)
			LET workingSide = "L"  --we start processing the left side / argument name
			
			FOR s = 1 TO argSegment.getlength()
				IF workingSide = "L" THEN
					IF argSegment[s] <> "=" THEN
						LET argVar = argVar.append(argSegment[s])
					ELSE
						LET workingSide = "R"  --we start processing the right side / argument value 
					END IF
				ELSE  --workingSide = "R"
					LET argVal = argVal.append(argSegment[s])
				END IF
			END FOR

			CASE UPSHIFT(argVar)
				WHEN "DEBUG"
					CALL set_debug(argVal)
				WHEN "VDOM"
 					CALL set_vDom(argVal)
 				WHEN "DB"
 					CALL set_db(argVal)
 				WHEN "ARGINT1"
 					CALL set_argInt1(argVal)
 				WHEN "ARGINT2"
 					CALL set_argInt2(argVal)
 				WHEN "ARGSTR1"
 					CALL set_argStr1(argVal)
 				WHEN "ARGSTR2"
 					CALL set_argStr2(argVal)
			END CASE
		END FOR
	END IF
END FUNCTION


###########################################################################
# FUNCTION set_argInt1(p_argInt1)
#
# Accessor Method for ml_argInt1
###########################################################################
FUNCTION set_argInt1(p_argInt1)
	DEFINE p_argInt1 SMALLINT

	IF ml_argInt1_set = FALSE OR ml_argInt1_set IS NULL THEN
		LET ml_argInt1 = p_argInt1
		CALL fgl_setenv("ARGINT1",ml_argInt1)
		LET ml_argInt1_set = TRUE		
	END IF
END FUNCTION


###########################################################################
# FUNCTION get_argInt1()
#
# Accessor Method for ml_argInt1 
###########################################################################
FUNCTION get_argInt1()
	
	IF ml_argInt1_set = FALSE OR ml_argInt1_set IS NULL THEN
		IF fgl_getenv("ARGINT1") IS NULL THEN
			CALL fgl_winmessage("argInt1 not specified","You need to specify the value of argInt1\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_argInt1(fgl_getenv("ARGINT1"))	
		END IF
	END IF

	RETURN ml_argInt1
END FUNCTION

###########################################################################
# FUNCTION get_argInt1_set()
#
# Accessor Method for ml_argInt1_set
###########################################################################
FUNCTION get_argInt1_set()
		RETURN ml_argInt1_set
END FUNCTION



###########################################################################
# FUNCTION set_argInt2(p_argInt2)
#
# Accessor Method for ml_argInt2
###########################################################################
FUNCTION set_argInt2(p_argInt2)
	DEFINE p_argInt2 SMALLINT

	IF ml_argInt2_set = FALSE OR ml_argInt2_set IS NULL THEN
		LET ml_argInt2 = p_argInt2
		CALL fgl_setenv("ARGINT2",ml_argInt2)
		LET ml_argInt2_set = TRUE		
	END IF
END FUNCTION

###########################################################################
# FUNCTION get_argInt2()
#
# Accessor Method for ml_argInt2 
###########################################################################
FUNCTION get_argInt2()
	
	IF ml_argInt2_set = FALSE OR ml_argInt2_set IS NULL THEN
		IF fgl_getenv("ARGINT2") IS NULL THEN
			CALL fgl_winmessage("ARGINT2 not specified","You need to specify the value of argInt2\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_argInt2(fgl_getenv("ARGINT2"))	
		END IF
	END IF

	RETURN ml_argInt2
END FUNCTION

###########################################################################
# FUNCTION get_argInt2_set()
#
# Accessor Method for ml_argInt2_set
###########################################################################
FUNCTION get_argInt2_set()
		RETURN ml_argInt2_set
END FUNCTION

###########################################################################
# FUNCTION set_argStr1(p_argStr1)
#
# Accessor Method for ml_argStr1
###########################################################################
FUNCTION set_argStr1(p_argStr1)
	DEFINE p_argStr1 STRING

	IF ml_argStr1_set = FALSE OR ml_argStr1_set IS NULL THEN
		LET ml_argStr1 = p_argStr1
		CALL fgl_setenv("ARGSTR1",ml_argStr1)
		LET ml_argStr1_set = TRUE		
	END IF
END FUNCTION


###########################################################################
# FUNCTION get_argStr1()
#
# Accessor Method for ml_argStr1 
###########################################################################
FUNCTION get_argStr1()
	
	IF ml_argStr1_set = FALSE OR ml_argStr1_set IS NULL THEN
		IF fgl_getenv("ARGSTR1") IS NULL THEN
			CALL fgl_winmessage("argStr1 not specified","You need to specify the value of argStr1\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_argStr1(fgl_getenv("ARGSTR1"))	
		END IF
	END IF

	RETURN ml_argStr1
END FUNCTION

###########################################################################
# FUNCTION get_argStr1_set()
#
# Accessor Method for ml_argStr1_set
###########################################################################
FUNCTION get_argStr1_set()
		RETURN ml_argStr1_set
END FUNCTION




###########################################################################
# FUNCTION set_argStr2(p_argStr2)
#
# Accessor Method for ml_argStr2
###########################################################################
FUNCTION set_argStr2(p_argStr2)
	DEFINE p_argStr2 STRING

	IF ml_argStr2_set = FALSE OR ml_argStr2_set IS NULL THEN
		LET ml_argStr2 = p_argStr2
		CALL fgl_setenv("ARGSTR2",ml_argStr2)
		LET ml_argStr2_set = TRUE		
	END IF
END FUNCTION


###########################################################################
# FUNCTION get_argStr2()
#
# Accessor Method for ml_argStr2 
###########################################################################
FUNCTION get_argStr2()
	
	IF ml_argStr2_set = FALSE OR ml_argStr2_set IS NULL THEN
		IF fgl_getenv("ARGSTR2") IS NULL THEN
			CALL fgl_winmessage("argStr2 not specified","You need to specify the value of argStr2\nIn the environment or in the URL!","error")		
		ELSE
			CALL set_argStr2(fgl_getenv("ARGSTR2"))	
		END IF
	END IF

	RETURN ml_argStr2
END FUNCTION

###########################################################################
# FUNCTION get_argStr2_set()
#
# Accessor Method for ml_argStr2_set
###########################################################################
FUNCTION get_argStr2_set()
		RETURN ml_argStr2_set
END FUNCTION


