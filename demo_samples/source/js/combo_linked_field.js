/**
 *  Copyright 2019 Querix Ltd
 *  Released under the MIT license.
 *  
 *  Linked[to Combobox] Field
 *  It would also be very very productive to have some kind of automatic DISPLAY TO <other field>. 
 *  Basically, the user selects a list item, the var-value will be shown in the comboBox text field and the 
 *  LABEL will be displayed to another form field/label (which must exist in the form). 
 *  To take kandooERP as an example, this feature would save >5000 lines of code AND aid in separating 
 *  the business logic from the presentation logic.
 */
(function(querix){
  if (window.top.document.querySelector('.qx-vdom')) {
    var findByIdentifier = function (qx, identifier) {
      return qx.r2v.registry().filter(function filterByIdentifier(x) {
        return x.bareValue && x.bareValue('Identifier') === identifier;
      });
    }
    var findByClassName = function (qx, className) {
      var allWidgets = qx.r2v.registry();
      return allWidgets.filter(function filterByClassName(x) {
        var result;
        if (x.hasClass !== undefined) {
          result = x.hasClass(className);
        } else {
          result = false;
        }
        return result;
      });
    }
    querix.plugins.wrappers.combo_linked_field = {
      render: function () {
        var x = this.ctx();
        var info = x.bareValue('Wrapper');
        var self = this;
        var result = this.wrappedProperties.render.call(self);
        var selector = info['Parameter'] || '';
        var text = x.displayValue();
        var method;
        if (selector[0] === '.') {
          method = findByClassName;
          if (selector.indexOf('.qx-text') > -1) {
            selector = selector.replace('.qx-text', '').trim();
          }
          if (selector.indexOf(' ') > -1) {
            selector = selector.split(' ').shift();
          }
          selector = selector.substring(1);
        } else {
          method = findByIdentifier;
        }
        method(x.qx, selector).forEach(function (x) {
          x.setBareValue('Text', text);
        });
        return result;
      }
    }
    return;
  }

  var $ = querix.$;
  var api = querix.plugins.api;

  var get$ = function(el) {
    try{
      if (el) {
        return el.getWindow().$;
      } else {
        return api.getWindow(api.topWindow()).$;
      }
    } catch (e) {
      if (console) {
        console.error('(Cannot get jQuery object of the top window',e.stack);
      }
      return null;
    }
  }

  var checkUnset = function (value, defaultValue) {
    if (typeof value === 'object' &&
      (value === querix.reactive.unset || value === querix.reactive.undef)) {
      return defaultValue;
    }
    return value;
  }

  querix.plugins.wrappers.combo_linked_field = {
    wrap: function(childId, combo, param) {
      var instance;
      var setTextValue = function(targetSelector, text) {
        //Find widget
        var normalizedSelector = (targetSelector || '').toLowerCase();
        var target = $$(normalizedSelector);
        if (target.length > 0) {
          //Narrow to the text part of the widget
          var targetText = target.find('.qx-text:first');
          if (targetText.length > 0) {
            //Set value
            var tag = targetText[0].tagName.toLowerCase();
            if (tag === 'input' || tag === 'textarea') {
              targetText.val(text);
            } else {
              targetText[0].innerHTML = text;
              if ((text || '').trim().length === 0) {
                target.hasClass('qx-has-text') && target.removeClass('qx-has-text');
              } else {
                !target.hasClass('qx-has-text') && target.addClass('qx-has-text');
              }
            }
          }
        }
      }

      if (combo.ty() === 'ComboBox') {
        var $$ = get$(combo), dom, 
        $text = combo.find('.qx-text:first'), 
        $select = combo.find('select:first'), 
        maxlength;
        if (param && param.substr) {
          if (param[0] !== '.' && param[0] !== '#' && param[0] !== ' ') {
            param = '.qx-identifier-' + param;
          }
          var targetSelector = '.qx-on-' + querix.childId + param + ':first';
          instance = {
            prop: function(path,v) {
              if (path === 'Text') {
                v = checkUnset(v, '');
                setTextValue(targetSelector, v);
              }
              return true;
            },
            attach: function() {
              return true;
            },
            remove: function() {
            }
          }
          $text.on('input paste drop', function(e) {
            setTextValue(targetSelector, e.target.value);
          });
          $select.on('input', function(e) {
            var text;
            var option = e.target.options[e.target.selectedIndex];
            if (option) {
              if (combo.hasClass('qx-c-field-show-value')) {
                text = option.value;
              } else {
                text = option.getAttribute('data-text') || option.value;
              }
              setTextValue(targetSelector, text);
            }
          });
        }
      }
      return instance;
    }
  }
})(querix);
