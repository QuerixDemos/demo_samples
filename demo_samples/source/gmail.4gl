GLOBALS "gmailGlobals.4gl"

MAIN
  DEFINE folderid, to, subject, body STRING

  LET userid = fgl_getenv("OAUTH_EMAIL");
  CALL url.setScheme("https")
  CAll url.setHost("www.googleapis.com")

  OPEN FORM form_g FROM "gmail"
  DISPLAY FORM form_g
  
  CALL loadLabels()  
  
  DIALOG ATTRIBUTE (UNBUFFERED)
    DISPLAY ARRAY folders TO folders.*
      BEFORE ROW
        LET folderid = folders[arr_curr()].id
        CALL loadEmails(folderid)
        DISPLAY folders[arr_curr()].name TO tc_items
    END DISPLAY
    
    DISPLAY ARRAY emails TO emails.*
      BEFORE ROW
        CALL loadEmailBody(arr_curr())
    END DISPLAY
    
    INPUT BY NAME search
    END INPUT
    
    ON ACTION Cancel
      EXIT DIALOG
      
    ON ACTION searchEmail      
      CALL loadEmails(folderid)
      
    ON ACTION newemail
      OPEN WINDOW gmailnew WITH FORM "gmailnew"
      DISPLAY userid TO from
      INPUT BY NAME to, subject, body
        ON ACTION ACCEPT
          CALL sendEmail(to, subject, body)
        ON ACTION CANCEL
          EXIT INPUT
      END INPUT
      CLOSE WINDOW gmailnew
  END DIALOG
END MAIN