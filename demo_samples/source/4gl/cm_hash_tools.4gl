##################################################################################
# FUNCTION get_target()
#
#
##################################################################################
FUNCTION get_target(p_hide,p_default,p_description)
	DEFINE p_hide STRING
	DEFINE p_default STRING
	DEFINE p_description STRING
	DEFINE l_target_hash_id STRING
	DEFINE l_description STRING
	
	LET l_description = "Do not use/select ", trim(p_hide), " !"
	LET l_target_hash_id = p_default

	OPEN WINDOW w_cm_hashmap_target WITH FORM ("form/cm_hashmap_target") ATTRIBUTE(BORDER,style="center")

	DISPLAY p_description TO lb_description
	DISPLAY p_description TO lb_dynamic_desription


	INPUT l_target_hash_id WITHOUT DEFAULTS FROM target
		AFTER INPUT
			IF l_target_hash_id = p_hide THEN
				CALL fgl_winmessage("Invalid Selection","You can not join the same HashMap","Error")
				CONTINUE INPUT
			END IF
	END INPUT	

	CLOSE WINDOW w_cm_hashmap_target

	RETURN downshift(l_target_hash_id)
END FUNCTION
##################################################################################
# END FUNCTION get_target()
##################################################################################

##################################################################################
# FUNCTION show_hash_keys_list(p_arr_hkey)
#
#
##################################################################################	
FUNCTION show_hash_keys_list(p_arr_hkey)
	DEFINE p_arr_hkey DYNAMIC ARRAY OF VARIANT

	OPEN WINDOW cm_hash_key_list WITH FORM "form/cm_hash_key_list" ATTRIBUTE(BORDER, style="center")

	DISPLAY ARRAY p_arr_hkey TO sc_reference.*
		BEFORE DISPLAY
			 CALL dialog.setActionHidden("ACCEPT",TRUE)
	END DISPLAY

	CLOSE WINDOW cm_hash_key_list 	
				
END FUNCTION				
##################################################################################
# END FUNCTION show_hash_keys_list(p_arr_hkey)
##################################################################################