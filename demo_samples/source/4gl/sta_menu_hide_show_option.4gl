MAIN
	DEFINE l_info STRING
	DEFINE l_timer INT
	
	
	LET l_info = "Shows HIDE/SHOW OPTION for the MENU block. The HIDE/SHOW statement address the menu item name (identifier=label) none-case sensitive"

	OPEN WINDOW wScreen WITH FORM "form/sta_menu_hide_show_option"
	DISPLAY l_info TO fInfo
	MENU
		BEFORE MENU
#---------------------------------
#			CALL fgl_dialog_setkeylabel("MyF5","MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",11)
#			#CALL fgl_dialog_setkeylabel("F5","MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",11)
#
#			CALL fgl_dialog_setkeylabel("Hide MyF5","Hide MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_1_24px.svg",21)
#			CALL fgl_dialog_setkeylabel("Show MyF5","Show MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_2_24px.svg",22)
#
#			CALL fgl_dialog_setkeylabel("hide myf5","hide myf5","{CONTEXT}/public/querix/icon/svg/24/ic_4_24px.svg",31)
#			CALL fgl_dialog_setkeylabel("show myf5","show myf5","{CONTEXT}/public/querix/icon/svg/24/ic_5_24px.svg",32)
#
#			CALL fgl_dialog_setkeylabel("HIDE MYF5","HIDE MYF5","{CONTEXT}/public/querix/icon/svg/24/ic_7_24px.svg",41)
#			CALL fgl_dialog_setkeylabel("SHOW MYF5","SHOW MYF5","{CONTEXT}/public/querix/icon/svg/24/ic_8_24px.svg",42)
#---------------------------------

			CALL fgl_dialog_setkeylabel("F5","MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",11)
			#CALL fgl_dialog_setkeylabel("F5","MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",11)

			CALL fgl_dialog_setkeylabel("F7","Hide MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_1_24px.svg",21)
			CALL fgl_dialog_setkeylabel("F8","Show MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_2_24px.svg",22)

			CALL fgl_dialog_setkeylabel("F9","hide myf5","{CONTEXT}/public/querix/icon/svg/24/ic_4_24px.svg",31)
			CALL fgl_dialog_setkeylabel("F10","show myf5","{CONTEXT}/public/querix/icon/svg/24/ic_5_24px.svg",32)

			CALL fgl_dialog_setkeylabel("F11","HIDE MYF5","{CONTEXT}/public/querix/icon/svg/24/ic_7_24px.svg",41)
			CALL fgl_dialog_setkeylabel("F12","SHOW MYF5","{CONTEXT}/public/querix/icon/svg/24/ic_8_24px.svg",42)
			
			DISPLAY "Time=", current
			
# The menu item presentation can also be customized using the KEY as the identifier 
#			CALL fgl_dialog_setkeylabel("F7","Hide MyF5")
#			CALL fgl_dialog_setkeylabel("F8","Show MyF5")
#
#			CALL fgl_dialog_setkeylabel("F9","hide myf5")
#			CALL fgl_dialog_setkeylabel("F10","show myf5")
#
#			CALL fgl_dialog_setkeylabel("F11","HIDE MYF5")
#			CALL fgl_dialog_setkeylabel("F12","SHOW MYF5")

		ON IDLE 60
			LET l_timer = l_timer +60
			DISPLAY "Timer=", l_timer
			DISPLAY "Time=", current
			IF l_timer mod 120 = 0 THEN
				HIDE OPTION "MyF5"
				DISPLAY "HIDE OPTION MyF5"
				CALL fgl_dialog_setkeylabel("MyF5","Hide MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",1)
			ELSE
				SHOW OPTION "MyF5"
				DISPLAY "SHOW OPTION MyF5"
				CALL fgl_dialog_setkeylabel("MyF5","Show MyF5","{CONTEXT}/public/querix/icon/svg/24/ic_message_24px.svg",1)
			END IF
			
	  COMMAND KEY(F5) "MyF5" "XMyF5 Key menu item"
	  	CALL fgl_winmessage("MyF5","MyF5 Menu item","info")
			
	  COMMAND KEY(F7) # "Hide MyF5" "Hide MyF5 MENU item"
	  	HIDE OPTION "MyF5"
	  	
	  	HIDE OPTION "Hide MyF7"
	  	HIDE OPTION "Hide MyF9"
	  	HIDE OPTION "Hide MyF11"
	  		  	
	  	SHOW OPTION "MyF8"	  	
	  	SHOW OPTION "MyF10"
	  	SHOW OPTION "MyF12"
	  	
	  	HIDE OPTION "F7"
	  	HIDE OPTION "F9"
	  	HIDE OPTION "F11"	  	
	  	SHOW OPTION "F8"	  	
	  	SHOW OPTION "F10"
	  	SHOW OPTION "F12"

	  	CALL fgl_winmessage("HideMyF5","HideMy F5 MENU item","info")
	  COMMAND KEY(F8) "Show MyF5" "Show MyF5 MENU item"
	  	SHOW OPTION "MyF5"

	  	SHOW OPTION "Hide MyF7"
	  	SHOW OPTION "Hide MyF9"
	  	SHOW OPTION "Hide MyF11"
	  	
	  	HIDE OPTION "MyF8"	  	
	  	HIDE OPTION "MyF10"
	  	HIDE OPTION "MyF12"


	  	SHOW OPTION "F7"
	  	SHOW OPTION "F9"
	  	SHOW OPTION "F11"	  	
	  	HIDE OPTION "F8"	  	
	  	HIDE OPTION "F10"
	  	HIDE OPTION "F12"
	  	
	  	CALL fgl_winmessage("ShowMyF5","Show MyF5 MENU item","info")

	  COMMAND KEY(F9) "hide myf5" "Hide myf5 MENU item"
	  	HIDE OPTION "myf5"
	  	CALL fgl_winmessage("HideF5","Hide My F5 MENU item","info")
	  COMMAND KEY(F10) "show myf5" "Show myf5 MENU item"
	  	SHOW OPTION "myf5"
	  	CALL fgl_winmessage("Show myf5","Show myf5 MENU item","info")

	  COMMAND KEY(F11) "HIDE MYF5" "Hide MYF5 MENU item"
	  	HIDE OPTION "MYF5"
	  	CALL fgl_winmessage("Hide MYF5","Hide MYF5 MENU item","info")
	  COMMAND KEY(F12) "SHOW MYF5" "Show MYF5 MENU item"
	  	SHOW OPTION "MYF5"
	  	CALL fgl_winmessage("Show MYF5","Show MYF5 MENU item","info")

		COMMAND KEY(CANCEL) "Exit" "Exit Program"
			EXIT MENU
	END MENU
	
	
END MAIN