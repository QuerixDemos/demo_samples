
##################################################################################
# FUNCTION show_key_list(p_arr_hkey)
#
#
##################################################################################	
FUNCTION show_key_list(p_arr_key)
	DEFINE p_arr_key DYNAMIC ARRAY OF VARIANT

	OPEN WINDOW cm_json_key_list WITH FORM "form/cm_json_key_list" ATTRIBUTE(BORDER, style="center")

	DISPLAY ARRAY p_arr_key TO sc_reference.*
		BEFORE DISPLAY
			 CALL dialog.setActionHidden("ACCEPT",TRUE)
	END DISPLAY

	CLOSE WINDOW cm_json_key_list 	
				
END FUNCTION				
##################################################################################
# END FUNCTION show_key_list(p_arr_hkey)
##################################################################################