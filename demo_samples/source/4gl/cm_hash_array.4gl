DEFINE dt_arr_HASH_VALUE TYPE AS DYNAMIC ARRAY OF MONEY
DEFINE dt_rec_HASH_ELEMENT TYPE AS RECORD #used for customSort() arguments
	hkey VARIANT, 
	hValue dt_arr_HASH_VALUE 
END RECORD
	    
##################################################################################
# MAIN
#
#
##################################################################################     
MAIN
  DEFINE l_ht HASHMAP OF dt_arr_HASH_VALUE	#hashMap for temp operations

  DEFINE l_hm1 HASHMAP OF dt_arr_HASH_VALUE		#hashMap example 1
  DEFINE l_hm1_value_array OF dt_arr_HASH_VALUE
	DEFINE l_hm1_row_hkey VARIANT
  DEFINE l_hm1_idx SMALLINT			#array index for l_hm1

  DEFINE l_hm2 HASHMAP OF dt_arr_HASH_VALUE		#hashMap example 2
  DEFINE l_hm2_value_array OF dt_arr_HASH_VALUE
	DEFINE l_hm2_row_hkey VARIANT
  DEFINE l_hm2_idx SMALLINT			#array index for l_hm2
  
	DEFINE l_hkey VARIANT					#hash element key
  DEFINE l_hvalue dt_arr_HASH_VALUE
  
  DEFINE i INT
  DEFINE l_idx1 SMALLINT
  DEFINE l_idx2 SMALLINT
	DEFINE l_msg STRING
	DEFINE l_search STRING	#??variant ? key ?
	DEFINE l_arr_hkey DYNAMIC ARRAY OF VARIANT #used to demonstrate the method .getKeys() for hash keys
	DEFINE l_target_hash_id STRING
  
  DEFINE l_jsonObj util.JSONObject
	DEFINE l_json_string STRING
	DEFINE l_choice CHAR(1)

	DEFER INTERRUPT
	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	
	CALL ui_init()
	
	OPEN WINDOW cm_hash_array WITH FORM "form/cm_hash_array"
	CALL ui.Interface.setText("Hash Value Array")
  	  
  CALL init(l_hm1, l_hm2) 
  
  CALL update_display(l_hm1, l_hm2)

  ##############################
	# DIALOG START
	###############################
  DIALOG ATTRIBUTE(UNBUFFERED)
		#------------------------------------------------------------------
  	# l_hm1 HashMap 1 
		#------------------------------------------------------------------
	  INPUT ARRAY l_hm1 WITHOUT DEFAULTS FROM sc_hm1_inp.* 
	  	BEFORE ROW
	  		LET l_hm1_idx = DIALOG.getCurrentRow("sc_hm1_inp")	  		
	  			  		
	  		IF l_hm1_idx > 0 THEN
	  			LET l_hm1_row_hkey = l_hm1.GetKey(l_hm1_idx)
	  			CALL l_hm1_value_array.clear()
	  			FOR i = 1 TO  l_hm1[l_hm1_row_hkey].getSize()
	  				LET l_hm1_value_array[i] = l_hm1[l_hm1_row_hkey][i]
					END FOR
				END IF

				DISPLAY l_hm1_idx TO hm1_bucketArrIndex
				DISPLAY l_hm1_row_hkey TO hm1_row_key
				DISPLAY ARRAY l_hm1_value_array TO sc_hm1_val_arr.* WITHOUT SCROLL
				END DISPLAY
				
	  	#############################################################	
			#dataType Methods
			############################################################# 		
			# .getSize() - returns count of elements in hash map	  		
	  	ON ACTION "getSize()"
	  		LET l_msg = "The size of l_hm1 -> l_hm1.getSize() = ", trim(l_hm1.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			# .delete(INT index) - Deletes element with index
			On ACTION "Delete(INT index)" 
				IF l_hm1_idx > 0 THEN
	  			DISPLAY l_hm1.delete(l_hm1_idx)
	  			LET l_msg = "l_hm1.delete(",trim(l_hm1_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF
	  	
	  	# .delete(INT first, INT last) - Deletes elements/range from first to the last	
			ON ACTION "Delete(INT first, INT last)"				
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_hm1.getlength()) OR (l_idx2 > l_hm1.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_hm1.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_hm1.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range

			# .remove(VARIANT key) - Deletes element with key 'key'
			ON ACTION "Remove(VARIANT key)" 				
				LET l_hkey = fgl_winprompt(1,1,"Enter Key to be deleted for the selected table",l_hm1_row_hkey,30,0)
	  		IF l_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_hm1.remove(l_hkey)
	  		END IF

			# .clear() - Removes all elements from hash map
	    ON ACTION "Clear()"
				LET l_choice = fgl_winbutton("Delete ALL hashMap buckets?","Do you want to delete all HashMap buckets/data?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	CALL l_hm1.clear()
	      END IF	      

			# .keyExists(key variant) - Returns true/false if the key exists
			ON ACTION "keyExists(key)"
				CALL fgl_winmessage(".keyExists(key) Needs implementing","This Method .keyExists(key) needs implementing","ERROR")

#				IF l_hm1.keyExists() THEN
#					CALL fgl_winmessage(".keyExists()","Key does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".keyExists()","Key does NOT exist (TRUE)","ERROR")
#				END IF					

			# .valueExists(value) - Returns true/false if this value exists in the hashMap
			ON ACTION "valueExists(value)"
				CALL fgl_winmessage(".valueExists(value) Needs implementing","This Method .valueExists(value) needs implementing","ERROR")

#				IF l_hm1.valueExists() THEN
#					CALL fgl_winmessage(".valueExists()","Value does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".valueExists()","Value does NOT exist (TRUE)","ERROR")
#				END IF			
				
			# .getKey(INT index) - Returns element's value by index
			ON ACTION "GetKey(INT index)" 
				MESSAGE "l_hm1_idx=", trim(l_hm1_idx)
				LET l_msg ="getKey(", trim(l_hm1_idx),  ")\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")
				MESSAGE l_msg
				
			# .getValue(INT index) - Returns element's VALUE by index
			ON ACTION "GetValue(INT index)" 
				LET l_msg ="getValue(", trim(l_hm1_idx),  ")", "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")
				MESSAGE l_msg


			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/Dictionary
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm1_row_hkey,30,0)
				IF l_hm1.Contains(l_hkey) THEN				
					CALL display_HashPair(l_hkey,l_hm1_value_array,"l_hm1.contains() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .find(key) - checks if the key exists, if true, return value otherwise return NULL
			ON ACTION "Find(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm1_row_hkey,30,0)
				
				IF l_hm1.find(l_hkey) THEN
					CALL l_hm1.find(l_hkey) RETURNING l_hvalue  				
					CALL display_HashPair(l_hkey, l_hm1[l_hkey],"l_hm1.contains() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .getKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
			ON ACTION "GetKeys()" 
				CALL l_hm1.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			# KeySort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by key. Any further operations, like Insert, Delete and etc., 
			# will be much slower, just because any such operation will trigger 
			# sorting by key. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			# 'direction’ is an optional parameter, it’s 1 by default.

			# .keySort(1) Ascending - sorts hashmap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) ASC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm1.keySort(1)
				MESSAGE "l_hm1: Sorted by key in ascending order"

			# .keySort(0) Descending- sorts hashmap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) DESC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm1.keySort(0)
				MESSAGE "l_hm1: Sorted by key in ascending order"

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			# .valueSort(1) Ascending - sorts hashmap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) ASC" 
				CALL l_hm1.ValueSort(1)
				MESSAGE "l_hm1: Sorted by value in ascending order"

			# .valueSort(0) Descending - sorts hashmap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) DESC" 
				CALL l_hm1.ValueSort(0)
				MESSAGE "l_hm1: Sorted by value in ascending order"

			#--------------------------------------------------------------------------
			# .customSort(funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			#--------------------------------------------------------------------------

			# .customSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" 
				MESSAGE "Sorting by DOB (date of birth) in Ascending Order"
				CALL l_hm1.CustomSort("CompareAsc")

			# .customSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareDESC"
				MESSAGE "Sorting by DOB (date of birth) in Descending Order" 
				CALL l_hm1.CustomSort("CompareDESC")

			# .resetSorting() - resets value sorts and back hashmap to the initial sorting.
			ON ACTION "ResetSorting()" 
				CALL l_hm1.resetSorting()

			# .copyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
			ON ACTION "CopyTo(hashMap)" 
				
				LET l_target_hash_id = get_target("hm1","hm2","Specify the COPY Target hashMap")
				MESSAGE "COPY ", trim(l_target_hash_id), " to l_hm1"
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.CopyTo(l_hm1)
						WHEN "hm2"
							CALL l_hm1.CopyTo(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			# LET and  .insert() work identical except, insert() can only insert new pairs in haschBuckets
			# but LET can also be used to change values
			# To change a key, you need to insert a new hashMapBucket and delete the old one
			ON ACTION "LET"
				CALL input_HashPair(l_hm1_row_hkey, l_hm1_value_array) RETURNING l_hm1_row_hkey, l_hm1_value_array
				IF NOT int_flag THEN
					#LET
					LET l_hm1[l_hm1_row_hkey] = l_hm1_value_array
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input aborted by user"
				END IF

	    # .insert(key,value) - Inserts a new hashMap pair / bucket
	    ON ACTION "INSERT"
	    	CALL input_HashPair(l_hm1_row_hkey, l_hm1_value_array) RETURNING l_hm1_row_hkey, l_hm1_value_array
				IF NOT int_flag THEN
					#.INSERT()
					CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_array)
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input aborted by user"
				END IF


	      
			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht) - Append element from 'l_ht' hash map.
				
				LET l_target_hash_id = get_target("hm1","hm2","Specify the Join Source HashMap")
				MESSAGE "Add (Join) l_hm1 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.Join(l_hm1)
						WHEN "hm2"
							CALL l_hm1.Join(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF		

			# DISPLAY the HashMap to the console				
			ON ACTION "DISPLAY HashMap TO console"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_hm1 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

			# DISPLAY the value of a hashMap bucket
			# NOTE: if the key does not exist, it will insert a new bucket with this key
			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_hm1_row_hkey,30,0)

				IF l_hm1.Contains(l_hkey) THEN	
					OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_hm1[l_hkey] TO console 
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hashmap_console
				ELSE
					LET l_msg = "Hash Key ", l_hm1 ," does not exist"
					CALL fgl_winmessage("Unknown HashKey",l_msg,"info")
					ERROR l_msg
				END IF
				
###########################################################

			# util.JSONObject.toFGL() / .util.JSON.parse()
			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #Append
							CALL util.JSON.parse(l_json_string, l_ht)
							CALL l_hm1.Join(l_ht)
						
						WHEN "O" #Overwrite
							CALL util.JSON.parse(l_json_string, l_hm1)
							
						WHEN "C" #Cancel
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			# util.JSONObject.fromFGL() / jsonObj.toString()
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console.fm2" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
				
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_hm1) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console
	  END INPUT

		#----------------------------------------
		#INPUT ARRAY for the hashMap bucket array 
		INPUT ARRAY l_hm1_value_array WITHOUT DEFAULTS FROM sc_hm1_val_arr.* 
		END INPUT
		


		#------------------------------------------------------------------
  	# HM2 HashMap 2 (l_hm2)
		#------------------------------------------------------------------
	  INPUT ARRAY l_hm2 WITHOUT DEFAULTS FROM sc_hm2_inp.* 
	  	BEFORE ROW
	  		LET l_hm2_idx = DIALOG.getCurrentRow("sc_hm2_inp")	  		
	  			  		
	  		IF l_hm2_idx > 0 THEN
	  			LET l_hm2_row_hkey = l_hm2.GetKey(l_hm2_idx)
	  			CALL l_hm2_value_array.clear()
	  			FOR i = 1 TO  l_hm2[l_hm2_row_hkey].getSize()
	  				LET l_hm2_value_array[i] = l_hm2[l_hm2_row_hkey][i]
					END FOR
				END IF

				DISPLAY l_hm2_idx TO hm2_bucketArrIndex
				DISPLAY l_hm2_row_hkey TO hm2_row_key
				DISPLAY ARRAY l_hm2_value_array TO sc_hm2_val_arr.* WITHOUT SCROLL
				END DISPLAY
				
	  	#############################################################	
			#dataType Methods
			############################################################# 		
			# .getSize() - returns count of elements in hash map	  		
	  	ON ACTION "getSize()"
	  		LET l_msg = "The size of l_hm2 -> l_hm2.getSize() = ", trim(l_hm2.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			# .delete(INT index) - Deletes element with index
			On ACTION "Delete(INT index)" 
				IF l_hm2_idx > 0 THEN
	  			DISPLAY l_hm2.delete(l_hm2_idx)
	  			LET l_msg = "l_hm2.delete(",trim(l_hm2_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF
	  	
	  	# .delete(INT first, INT last) - Deletes elements/range from first to the last	
			ON ACTION "Delete(INT first, INT last)"				
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_hm2.getlength()) OR (l_idx2 > l_hm2.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_hm2.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_hm2.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range

			# .remove(VARIANT key) - Deletes element with key 'key'
			ON ACTION "Remove(VARIANT key)" 				
				LET l_hkey = fgl_winprompt(1,1,"Enter Key to be deleted for the selected table",l_hm2_row_hkey,30,0)
	  		IF l_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_hm2.remove(l_hkey)
	  		END IF

			# .clear() - Removes all elements from hash map
	    ON ACTION "Clear()"
				LET l_choice = fgl_winbutton("Delete ALL hashMap buckets?","Do you want to delete all HashMap buckets/data?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	CALL l_hm2.clear()
	      END IF	      

			# .keyExists(key variant) - Returns true/false if the key exists
			ON ACTION "keyExists(key)"
				CALL fgl_winmessage(".keyExists(key) Needs implementing","This Method .keyExists(key) needs implementing","ERROR")

#				IF l_hm2.keyExists() THEN
#					CALL fgl_winmessage(".keyExists()","Key does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".keyExists()","Key does NOT exist (TRUE)","ERROR")
#				END IF					

			# .valueExists(value) - Returns true/false if this value exists in the hashMap
			ON ACTION "valueExists(value)"
				CALL fgl_winmessage(".valueExists(value) Needs implementing","This Method .valueExists(value) needs implementing","ERROR")

#				IF l_hm2.valueExists() THEN
#					CALL fgl_winmessage(".valueExists()","Value does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".valueExists()","Value does NOT exist (TRUE)","ERROR")
#				END IF			
				
			# .getKey(INT index) - Returns element's value by index
			ON ACTION "GetKey(INT index)" 
				MESSAGE "l_hm2_idx=", trim(l_hm2_idx)
				LET l_msg ="getKey(", trim(l_hm2_idx),  ")\nKey = ", trim(l_hm2.GetKey(l_hm2_idx)), "\nValue=",trim(l_hm2.getValue(l_hm2_idx)),"\nArray index=", trim(l_hm2_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")
				MESSAGE l_msg
				
			# .getValue(INT index) - Returns element's VALUE by index
			ON ACTION "GetValue(INT index)" 
				LET l_msg ="getValue(", trim(l_hm2_idx),  ")", "\nValue=",trim(l_hm2.getValue(l_hm2_idx)),"\nKey = ", trim(l_hm2.GetKey(l_hm2_idx)), "\nArray index=", trim(l_hm2_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")
				MESSAGE l_msg


			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/Dictionary
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm2_row_hkey,30,0)
				IF l_hm2.Contains(l_hkey) THEN				
					CALL display_HashPair(l_hkey,l_hm2_value_array,"l_hm2.contains() returned true / (l_hm2 contains key)",".contains(key) method returns true if the key exists")
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .find(key) - checks if the key exists, if true, return value otherwise return NULL
			ON ACTION "Find(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm2_row_hkey,30,0)
				
				IF l_hm2.find(l_hkey) THEN
					CALL l_hm2.find(l_hkey) RETURNING l_hvalue  				
					CALL display_HashPair(l_hkey, l_hm2[l_hkey],"l_hm2.contains() returned true / (l_hm2 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .getKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
			ON ACTION "GetKeys()" 
				CALL l_hm2.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			# KeySort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by key. Any further operations, like Insert, Delete and etc., 
			# will be much slower, just because any such operation will trigger 
			# sorting by key. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			# 'direction’ is an optional parameter, it’s 1 by default.

			# .keySort(1) Ascending - sorts hashmap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) ASC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm2.keySort(1)
				MESSAGE "l_hm2: Sorted by key in ascending order"

			# .keySort(0) Descending- sorts hashmap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) DESC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm2.keySort(0)
				MESSAGE "l_hm2: Sorted by key in ascending order"

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			# .valueSort(1) Ascending - sorts hashmap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) ASC" 
				CALL l_hm2.ValueSort(1)
				MESSAGE "l_hm2: Sorted by value in ascending order"

			# .valueSort(0) Descending - sorts hashmap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) DESC" 
				CALL l_hm2.ValueSort(0)
				MESSAGE "l_hm2: Sorted by value in ascending order"

			#--------------------------------------------------------------------------
			# .customSort(funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			#--------------------------------------------------------------------------

			# .customSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" 
				MESSAGE "Sorting by DOB (date of birth) in Ascending Order"
				CALL l_hm2.CustomSort("CompareAsc")

			# .customSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareDESC"
				MESSAGE "Sorting by DOB (date of birth) in Descending Order" 
				CALL l_hm2.CustomSort("CompareDESC")

			# .resetSorting() - resets value sorts and back hashmap to the initial sorting.
			ON ACTION "ResetSorting()" 
				CALL l_hm2.resetSorting()

			# .copyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
			ON ACTION "CopyTo(hashMap)" 
				
				LET l_target_hash_id = get_target("hm2","hm2","Specify the COPY Target hashMap")
				MESSAGE "COPY ", trim(l_target_hash_id), " to l_hm2"
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm2.CopyTo(l_hm1)
						WHEN "hm2"
							CALL l_hm2.CopyTo(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			# LET and  .insert() work identical except, insert() can only insert new pairs in haschBuckets
			# but LET can also be used to change values
			# To change a key, you need to insert a new hashMapBucket and delete the old one
			ON ACTION "LET"
				CALL input_HashPair(l_hm2_row_hkey, l_hm2_value_array) RETURNING l_hm2_row_hkey, l_hm2_value_array
				IF NOT int_flag THEN
					#LET
					LET l_hm2[l_hm2_row_hkey] = l_hm2_value_array
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input aborted by user"
				END IF

	    # .insert(key,value) - Inserts a new hashMap pair / bucket
	    ON ACTION "INSERT"
	    	CALL input_HashPair(l_hm2_row_hkey, l_hm2_value_array) RETURNING l_hm2_row_hkey, l_hm2_value_array
				IF NOT int_flag THEN
					#.INSERT()
					CALL l_hm2.Insert(l_hm2_row_hkey, l_hm2_value_array)
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input aborted by user"
				END IF


	      
			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht) - Append element from 'l_ht' hash map.
				
				LET l_target_hash_id = get_target("hm2","hm2","Specify the Join Source HashMap")
				MESSAGE "Add (Join) l_hm2 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm2.Join(l_hm1)
						WHEN "hm2"
							CALL l_hm2.Join(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF		

			# DISPLAY the HashMap to the console				
			ON ACTION "DISPLAY HashMap"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_hm2 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

			# DISPLAY the value of a hashMap bucket
			# NOTE: if the key does not exist, it will insert a new bucket with this key
			ON ACTION "DISPLAY HashBucket"
				LET l_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_hm2_row_hkey,30,0)

				IF l_hm2.Contains(l_hkey) THEN	
					OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_hm2[l_hkey] TO console 
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hashmap_console
				ELSE
					LET l_msg = "Hash Key ", l_hm2 ," does not exist"
					CALL fgl_winmessage("Unknown HashKey",l_msg,"info")
					ERROR l_msg
				END IF
				
###########################################################

			# util.JSONObject.toFGL() / .util.JSON.parse()
			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #Append
							CALL util.JSON.parse(l_json_string, l_ht)
							CALL l_hm2.Join(l_ht)
						
						WHEN "O" #Overwrite
							CALL util.JSON.parse(l_json_string, l_hm2)
							
						WHEN "C" #Cancel
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			# util.JSONObject.fromFGL() / jsonObj.toString()
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console.fm2" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
				
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_hm2) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console
	  END INPUT

		#----------------------------------------
		#INPUT ARRAY for the hashMap bucket array 
		INPUT ARRAY l_hm2_value_array WITHOUT DEFAULTS FROM sc_hm2_val_arr.* 
		END INPUT

		
		#DIALOG Actions
		#------------------------------------------------
		ON ACTION "REFRESH" #"DISPLAY UPDATE"
			CALL update_display(l_hm1,l_hm2)		

		ON ACTION "EXIT"
			EXIT DIALOG

	END DIALOG 
	#END DIALOG --------------------------------------------------------------------

END MAIN
##################################################################################
# END MAIN
##################################################################################     



##################################################################################
# FUNCTION CompareAsc(p_hash_bucket1, p_hash_bucket2)
#
# In this example, we calculate the sum and sort based on it's value
##################################################################################  
FUNCTION CompareAsc(p_hash_bucket1, p_hash_bucket2)
	DEFINE p_hash_bucket1 dt_rec_HASH_ELEMENT	
	DEFINE p_hash_bucket2 dt_rec_HASH_ELEMENT
	DEFINE l_dict1_sum MONEY
	DEFINE l_dict2_sum MONEY
	DEFINE l_hkey1 VARIANT
	DEFINE l_hkey2 VARIANT	
	DEFINE i SMALLINT
	DEFINE i2 SMALLINT

	LET l_dict1_sum = 0
	LET l_dict2_sum = 0

  FOR i = 1 TO p_hash_bucket1.hValue.getSize()
  	LET l_dict1_sum = l_dict1_sum + p_hash_bucket1.hValue[i]
  END FOR

  FOR i = 1 TO p_hash_bucket2.hValue.getSize()
  	LET l_dict2_sum = l_dict2_sum + p_hash_bucket2.hValue[i]
  END FOR
	
  RETURN l_dict1_sum < l_dict2_sum
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_hash_bucket1, p_hash_bucket2)
##################################################################################  


##################################################################################
# FUNCTION CompareAsc(p_hash_bucket1, p_hash_bucket2)
#
#
##################################################################################  
FUNCTION CompareDesc(p_hash_bucket1, p_hash_bucket2)
	DEFINE p_hash_bucket1 dt_rec_HASH_ELEMENT	
	DEFINE p_hash_bucket2 dt_rec_HASH_ELEMENT
	DEFINE l_dict1_sum MONEY
	DEFINE l_dict2_sum MONEY
	DEFINE l_hkey1 VARIANT
	DEFINE l_hkey2 VARIANT	
	DEFINE i SMALLINT
	DEFINE i2 SMALLINT

	LET l_dict1_sum = 0
	LET l_dict2_sum = 0
	
  FOR i = 1 TO p_hash_bucket1.hValue.getSize()
  	LET l_dict1_sum = l_dict1_sum + p_hash_bucket1.hValue[i]
  END FOR

  FOR i = 1 TO p_hash_bucket2.hValue.getSize()
  	LET l_dict2_sum = l_dict2_sum + p_hash_bucket2.hValue[i]
  END FOR

	
  RETURN l_dict1_sum >= l_dict2_sum
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_hash_bucket1, p_hash_bucket2)
##################################################################################  


##################################################################################
# FUNCTION update_display(p_hm1,p_hm2)
#
#
##################################################################################     
FUNCTION update_display(p_hm1,p_hm2)
	DEFINE p_hm1 HASHMAP OF dt_arr_HASH_VALUE
	DEFINE p_hm2 HASHMAP OF dt_arr_HASH_VALUE
	 
	DISPLAY ARRAY p_hm1 TO sc_hm1_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
	DISPLAY ARRAY p_hm2 TO sc_hm2_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
			
END FUNCTION
##################################################################################
# END FUNCTION update_display(p_hm1,p_hm2)
##################################################################################     


##################################################################################
# FUNCTION init()
#
#
##################################################################################     
FUNCTION init(p_hm1, p_hm2)
  DEFINE p_hm1 HASHMAP OF dt_arr_HASH_VALUE
  DEFINE p_hm2 HASHMAP OF dt_arr_HASH_VALUE

	DEFINE l_hkey VARIANT
	DEFINE l_rec_hvalue dt_arr_HASH_VALUE
	DEFINE l_arr_size SMALLINT
	DEFINE l_arr2_size SMALLINT
  DEFINE i SMALLINT
  DEFINE i2 SMALLINT

  LET l_arr_size = 20
  LET l_arr2_size = 50
  
  FOR i = 1 TO l_arr_size
  	LET l_hkey = "hm1_key_", trim(i)

		FOR i2 = 1 TO l_arr2_size
	  	LET p_hm1[l_hkey][i2]= 0.33 + (i*87.11) + (i2*3.73)
	  END FOR
	END FOR

  FOR i = 1 TO l_arr_size
  	LET l_hkey = "hm2_key_", trim(i)

		FOR i2 = 1 TO l_arr2_size
	  	LET p_hm2[l_hkey][i2]= 100.44 + (i*87.11) + (i2*3.73)
	  END FOR
	END FOR
	
END FUNCTION
##################################################################################
# END FUNCTION init()
##################################################################################     


##################################################################################
# FUNCTION debug(p_hm1, p_hm2)
#
#
##################################################################################     
FUNCTION debug(p_hm1, p_hm2)
  DEFINE p_hm1  HASHMAP OF STRING
  DEFINE p_hm2  HASHMAP OF STRING
  
	DEFINE i SMALLINT
	
  DISPLAY "HashMap 1 data:"
  FOR i = 1 TO p_hm1.GetLength()
    DISPLAY "'", p_hm1.GetKey(i), "' - '", p_hm1.GetValue(i), "'"
  END FOR

  DISPLAY "HashMap 2 data:"
  FOR i = 1 TO p_hm2.GetLength()
    DISPLAY "'", p_hm2.GetKey(i), "' - '", p_hm2.GetValue(i), "'"
  END FOR

END FUNCTION
##################################################################################
# END FUNCTION debug(p_hm1, p_hm2)
##################################################################################


##################################################################################
# FUNCTION input_HashPair(p_hkey, p_arr_hvalue)
#
#
##################################################################################
FUNCTION input_HashPair(p_hkey, p_arr_hvalue)
	DEFINE p_hkey VARIANT
	DEFINE p_arr_hvalue OF dt_arr_HASH_VALUE
	
	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_array" ATTRIBUTE(BORDER,style="center")

	DIALOG ATTRIBUTE(UNBUFFERED)
		INPUT p_hkey WITHOUT DEFAULTS FROM key
		END INPUT
		INPUT ARRAY p_arr_hvalue WITHOUT DEFAULTS FROM sc_arr_hvalue.* 
		END INPUT
		ON ACTION ("ACCEPT","CANCEL")
			EXIT DIALOG
		
	END DIALOG 

	CLOSE WINDOW w_hashPair
	RETURN p_hkey,p_arr_hvalue
END FUNCTION
##################################################################################
# END FUNCTION input_HashPair(p_hkey, p_arr_hvalue)
##################################################################################


##################################################################################
# FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
#
#
##################################################################################
FUNCTION display_HashPair(p_hkey,p_arr_hvalue,p_title,p_description)
	DEFINE p_hkey VARIANT
	DEFINE p_arr_hvalue OF dt_arr_HASH_VALUE
	DEFINE p_title STRING
	DEFINE p_description STRING

	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_array" ATTRIBUTE(BORDER,style="center")
	
	DISPLAY p_title TO lb_title
	DISPLAY p_description TO lb_description
	DISPLAY p_hkey TO key
	DISPLAY ARRAY p_arr_hvalue TO sc_hashArray.*
		BEFORE DISPLAY
			 CALL dialog.setActionHidden("ACCEPT",TRUE)	
		ON ACTION "CANCEL"
			LET int_flag = FALSE
			EXIT DISPLAY
	END DISPLAY
	 
	CLOSE WINDOW w_hashPair
END FUNCTION
##################################################################################
# END FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
##################################################################################


##################################################################################
# FUNCTION edit_json_string(p_json_string)
#
#
##################################################################################
FUNCTION edit_json_string(p_json_string)
	DEFINE p_json_string STRING
	
	IF p_json_string IS NULL THEN
		LET p_json_string = 

			'{
				"hMap_imp_key_key_9":[787.72,791.45,795.18,798.91,802.64,806.37,810.10,813.83,817.56,821.29,825.02,828.75,832.48,836.21,839.94,843.67,847.40,851.13,854.86,858.59,862.32,866.05,869.78,873.51,877.24,880.97,884.70,888.43,892.16,895.89,899.62,903.35,907.08,910.81,914.54,918.27,922.00,925.73,929.46,933.19,936.92,940.65,944.38,948.11,951.84,955.57,959.30,963.03,966.76,970.49],
				"hMap_imp_key_key_1":[90.84,94.57,98.30,102.03,105.76,109.49,113.22,116.95,120.68,124.41,128.14,131.87,135.60,139.33,143.06,146.79,150.52,154.25,157.98,161.71,165.44,169.17,172.90,176.63,180.36,184.09,187.82,191.55,195.28,199.01,202.74,206.47,210.20,213.93,217.66,221.39,225.12,228.85,232.58,236.31,240.04,243.77,247.50,251.23,254.96,258.69,262.42,266.15,269.88,273.61],
				"hMap_imp_key_key_2":[177.95,181.68,185.41,189.14,192.87,196.60,200.33,204.06,207.79,211.52,215.25,218.98,222.71,226.44,230.17,233.90,237.63,241.36,245.09,248.82,252.55,256.28,260.01,263.74,267.47,271.20,274.93,278.66,282.39,286.12,289.85,293.58,297.31,301.04,304.77,308.50,312.23,315.96,319.69,323.42,327.15,330.88,334.61,338.34,342.07,345.80,349.53,353.26,356.99,360.72],
				"hMap_imp_key_key_3":[265.06,268.79,272.52,276.25,279.98,283.71,287.44,291.17,294.90,298.63,302.36,306.09,309.82,313.55,317.28,321.01,324.74,328.47,332.20,335.93,339.66,343.39,347.12,350.85,354.58,358.31,362.04,365.77,369.50,373.23,376.96,380.69,384.42,388.15,391.88,395.61,399.34,403.07,406.80,410.53,414.26,417.99,421.72,425.45,429.18,432.91,436.64,440.37,444.10,447.83],
				"hMap_imp_key_key_4":[352.17,355.90,359.63,363.36,367.09,370.82,374.55,378.28,382.01,385.74,389.47,393.20,396.93,400.66,404.39,408.12,411.85,415.58,419.31,423.04,426.77,430.50,434.23,437.96,441.69,445.42,449.15,452.88,456.61,460.34,464.07,467.80,471.53,475.26,478.99,482.72,486.45,490.18,493.91,497.64,501.37,505.10,508.83,512.56,516.29,520.02,523.75,527.48,531.21,534.94],
				"hMap_imp_key_key_5":[439.28,443.01,446.74,450.47,454.20,457.93,461.66,465.39,469.12,472.85,476.58,480.31,484.04,487.77,491.50,495.23,498.96,502.69,506.42,510.15,513.88,517.61,521.34,525.07,528.80,532.53,536.26,539.99,543.72,547.45,551.18,554.91,558.64,562.37,566.10,569.83,573.56,577.29,581.02,584.75,588.48,592.21,595.94,599.67,603.40,607.13,610.86,614.59,618.32,622.05],
				"hMap_imp_key_key_6":[526.39,530.12,533.85,537.58,541.31,545.04,548.77,552.50,556.23,559.96,563.69,567.42,571.15,574.88,578.61,582.34,586.07,589.80,593.53,597.26,600.99,604.72,608.45,612.18,615.91,619.64,623.37,627.10,630.83,634.56,638.29,642.02,645.75,649.48,653.21,656.94,660.67,664.40,668.13,671.86,675.59,679.32,683.05,686.78,690.51,694.24,697.97,701.70,705.43,709.16],
				"hMap_imp_key_key_7":[613.50,617.23,620.96,624.69,628.42,632.15,635.88,639.61,643.34,647.07,650.80,654.53,658.26,661.99,665.72,669.45,673.18,676.91,680.64,684.37,688.10,691.83,695.56,699.29,703.02,706.75,710.48,714.21,717.94,721.67,725.40,729.13,732.86,736.59,740.32,744.05,747.78,751.51,755.24,758.97,762.70,766.43,770.16,773.89,777.62,781.35,785.08,788.81,792.54,796.27],
				"hMap_imp_key_key_8":[700.61,704.34,708.07,711.80,715.53,719.26,722.99,726.72,730.45,734.18,737.91,741.64,745.37,749.10,752.83,756.56,760.29,764.02,767.75,771.48,775.21,778.94,782.67,786.40,790.13,793.86,797.59,801.32,805.05,808.78,812.51,816.24,819.97,823.70,827.43,831.16,834.89,838.62,842.35,846.08,849.81,853.54,857.27,861.00,864.73,868.46,872.19,875.92,879.65,883.38],
				"hMap_imp_key_key_10":[874.83,878.56,882.29,886.02,889.75,893.48,897.21,900.94,904.67,908.40,912.13,915.86,919.59,923.32,927.05,930.78,934.51,938.24,941.97,945.70,949.43,953.16,956.89,960.62,964.35,968.08,971.81,975.54,979.27,983.00,986.73,990.46,994.19,997.92,1001.65,1005.38,1009.11,1012.84,1016.57,1020.30,1024.03,1027.76,1031.49,1035.22,1038.95,1042.68,1046.41,1050.14,1053.87,1057.60]
			}'
			
	END IF
	
	OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console"
	
	INPUT p_json_string WITHOUT DEFAULTS FROM console ATTRIBUTE(UNBUFFERED)
	END INPUT
	
	CLOSE WINDOW cm_hashmap_console
	RETURN p_json_string
END FUNCTION
##################################################################################
# END FUNCTION edit_json_string(p_json_string)
##################################################################################