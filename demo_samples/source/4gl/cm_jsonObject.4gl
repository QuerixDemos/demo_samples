#####################################################################################
# GLOBAL SCOPE VARIABLES
#####################################################################################
DEFINE dt_rec_jsonObj TYPE AS RECORD
	id STRING,
	fName STRING,
	lName STRING,
	dob DATE,
	country STRING,
	partner BOOLEAN,
	loyalty SMALLINT,
	myNullInt INT
	#transaction DYNAMIC ARRAY OF MONEY
END RECORD


DEFINE dt_rec_json_struct TYPE AS RECORD
	key_id STRING,
	id STRING,
	key_fname STRING,
	fName STRING,
	key_lName STRING,
	lName STRING,
	key_dob STRING,
	dob DATE,
	key_country STRING,
	country STRING,
	key_partner STRING,
	partner BOOLEAN,
	key_loyalty STRING,
	loyalty SMALLINT,
	key_myNullInt STRING,
	myNullInt INT

END RECORD

GLOBALS
	DEFINE glob_arr_rec_pair DYNAMIC ARRAY OF RECORD 
		jKey STRING, 
		jValue VARIANT 
	END RECORD
END GLOBALS

#####################################################################################
# MAIN
#
#
#####################################################################################
MAIN
	DEFINE l_json_obj util.JSONObject
#	DEFINE json_obj_tmp util.JSONObject
	DEFINE l_rec_data OF dt_rec_jsonObj
	DEFINE l_arr_rec_data DYNAMIC ARRAY OF RECORD
			j_key STRING,
			j_value STRING
		END RECORD		
	DEFINE l_json_obj_data util.JSONObject
	DEFINE l_rec_json_data OF dt_rec_json_struct
#	DEFINE l_ref_rec REFERENCE
	DEFINE i SMALLINT
	DEFINE l_js STRING	#JSON string
	DEFINE l_json_key VARIANT	#json key
	DEFINE l_msg STRING #for messages
	DEFINE l_index SMALLINT #json index
	DEFINE l_index_start SMALLINT #json index start
	DEFINE l_index_end SMALLINT #json index end
	DEFINE l_key VARIANT
	DEFINE l_value VARIANT
	DEFINE l_arr_curr SMALLINT
	DEFINE l_choice CHAR
	DEFINE l_arr_j_key DYNAMIC ARRAY OF VARIANT #used to demonstrate the method .getKeys() for hash keys	
	
	DEFER INTERRUPT
	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	
	CALL ui_init()
	
	OPEN WINDOW cm_jsonObject WITH FORM "form/cm_jsonObject"
	
	IF l_json_obj IS NOT NULL THEN
		DISPLAY "Failed to DEFINE the object" 
	END IF

	#util.JSONObject.create() #Create JSONObject instance -- NULL return is error
	LET l_json_obj = util.JSONObject.CREATE()
	IF l_json_obj IS NULL THEN 
		CALL fgl_winmessage("JSONObject could not be created","Failed to CREATE the JSONobject!\n","error")
	END IF
	
	CALL init_record() RETURNING l_rec_data.*


	#util.JSONObject.fromFGL() # will overwrite any existing JSONObj data
	LET l_json_obj = util.JSONObject.fromFGL(l_rec_data)


#	LET l_js='{ 
#						"fName":"Alexander", 
#						"lName":"Williams",
#						"dob":"2012-03-19T07:22Z", 
#						"country":"Spain",  
#						"partner":true,
#						"myNullInt":null
#					}'
#						"transaction":[234,3456,24656,34561] 




	#BEFORE DIALOG
	CALL display_json_object_value_list(l_json_obj)	
	CALL get_rec_data(l_json_obj) RETURNING l_rec_json_data.*

	DIALOG ATTRIBUTE(UNBUFFERED)

		#DISPLAY ARRAY --------------------------------------------------------------------------
		DISPLAY ARRAY glob_arr_rec_pair TO sc_json.* 
			BEFORE DISPLAY
				DISPLAY l_json_obj.getLength() TO json_getLength
				
			BEFORE ROW
				LET l_arr_curr = arr_curr()
	-------------------
	
		END DISPLAY
		#END DISPLAY ---------------------------------------

		#INPUT Record (extracted from the jsonObj.
		INPUT l_rec_json_data.* WITHOUT DEFAULTS FROM sc_rec_data.*
		END INPUT

		BEFORE DIALOG
			CALL get_rec_data(l_json_obj) RETURNING l_rec_json_data.*

			
		ON ACTION "Get JSON Data"
			CALL get_rec_data(l_json_obj) RETURNING l_rec_json_data.*
			CALL display_json_object_value_list(l_json_obj)
				
		ON ACTION "Save Record"
			CALL l_json_obj.put(l_rec_json_data.key_id,l_rec_json_data.id)
			CALL l_json_obj.put(l_rec_json_data.key_fName,l_rec_json_data.fName)
			CALL l_json_obj.put(l_rec_json_data.key_lName,l_rec_json_data.lName)
			CALL l_json_obj.put(l_rec_json_data.key_dob,l_rec_json_data.dob)
			CALL l_json_obj.put(l_rec_json_data.key_country,l_rec_json_data.country)
			CALL l_json_obj.put(l_rec_json_data.key_partner,l_rec_json_data.partner)
			CALL l_json_obj.put(l_rec_json_data.key_loyalty,l_rec_json_data.loyalty)			
			CALL l_json_obj.put(l_rec_json_data.key_myNullInt,l_rec_json_data.myNullInt)
			CALL display_json_object_value_list(l_json_obj)
				
		ON ACTION "CANCEL"
			EXIT DIALOG

			#---------------------------------------------------------------------------------------
			# util.JSON
			#---------------------------------------------------------------------------------------
			
			#util.JSON.parse(<jason string>,<4GL variable>)  util.JSON.parse() parses a JSON string into separate values and fills the corresponding program variable with them.
			ON ACTION "util.JSON.parse()"
				LET l_js = 
					'{
						"id":"HHOELZL",
						"fname":"Hubert",
						"lname":"Hölzl",
						"dob":"2012-03-19T07:22Z",
						"country":"Germany",
						"partner":true,
						"loyalty":5,
						"mynullint":null
					}'

				LET l_choice = fgl_winbutton(
					"Choose target variable dataType",
					"To what variable type do you want to parse this json string to?",
					"Record",
					"Record|Array|JSONObject|Cancel",
					"question")
				CASE l_choice 
					WHEN "R"
						CALL util.JSON.parse(l_js, l_rec_data)
						CALL display_4gl_rec_data(l_rec_data.*)
					WHEN "A"
						CALL util.JSON.parse(l_js, l_arr_rec_data)
						CALL display_4gl_arr_rec_data(l_arr_rec_data)
					WHEN "J"
						CALL util.JSON.parse(l_js, l_json_obj_data)	  
						CALL getTypeLoop(l_json_obj_data)  	
					OTHERWISE
						LET int_flag = TRUE 
						      	#CALL l_json_obj.clear()
				END CASE
				
				
				IF int_flag THEN
					MESSAGE "Operation aborted on user request"
					LET int_flag = FALSE
				END IF
	
			#Another util.JSONObject.parse() Example
			ON ACTION ".parse()...more"
				LET l_js=
					'{ 
						"cust_num":2735, 
						"cust_name":"McCarlson",
						"sport":"Football" 
					}'
				LET l_json_obj = util.JSONObject.parse(l_js)
				CALL display_json_object_value_list(l_json_obj)
	
			#util.JSON.format() formats and indents data as a JSON string.
			ON ACTION "util.JSON.format()"
				LET l_js = util.JSON.stringify( l_rec_data )
				#DISPLAY l_js
				#DISPLAY "----------------------------------" 
				#DISPLAY util.JSON.format( l_js ) 
				LET l_msg = 
					trim(l_js), "\n\n",
					"----------------------------------\n",
					"util.JSON.format( l_js )", "\n",
					"----------------------------------\n",
					util.JSON.format( l_js )
				CALL fgl_winmessage(".format() method",l_msg,"info")
					
			#util.JSON.stringify() turns any 4gl variable into a flat JSON string.
			ON ACTION "util.JSON.stringify()" 
				LET l_js = util.JSON.stringify( l_rec_data )

				LET l_msg = "LET l_js = util.JSON.stringify( l_rec_data )", "\n\n", l_js
				CALL fgl_winmessage(".stringify() method",l_msg,"info")
					
			#util.JSON.proposeType() suggests a record structure that can hold the given JSON string.
			ON ACTION "util.JSON.proposeType()"
				LET l_js = util.JSON.stringify( l_rec_data )
				
				LET l_msg = "util.JSON.proposeType(l_js)", "\n\n", util.JSON.proposeType(l_js)
				CALL fgl_winmessage(".stringify() method",l_msg,"info")				
				#CALL display_json_object_value_list(l_json_obj)	
#########################################
	
			#util.JSONObject.parse(<jason string>) parses a JSON string into a new JSON object.
			ON ACTION "util.JSONObject.parse()"
				LET l_js ='{ "cust_num":273, "cust_name":"McCarlson",
          "orderids":[234,3456,24656,34561],
          "js_obj" : { "js_cust_num":1111, "js_cust_name":"js_McCarlson",
          "js_orderids":[123, 2345, 34567, 45678] } }'
          			
				LET l_json_obj = util.JSONObject.parse(l_js)
				CALL display_json_object_value_list(l_json_obj)					


			#--------------------------------------------------------------
			# Actions for general demo/test case usage
			#--------------------------------------------------------------

			ON ACTION "init 4gl record"
				CALL init_record() RETURNING l_rec_data.*
				CALL display_4gl_rec_data(l_rec_data.*)
				CALL display_json_object_value_list(l_json_obj)


			#--------------------------------------------------------------
			# NULL Initialize / Clear entire json
			#--------------------------------------------------------------
					
			#Initialze jsonObject to NULL sing initialize to NULL
			ON ACTION "INITIALIZE jsonObj TO NULL"
				INITIALIZE l_json_obj TO NULL		
				CALL display_json_object_value_list(l_json_obj)	
	
			#Initialze jsonObject to NULL 
			ON ACTION "LET jsonObj = NULL"
				LET l_json_obj = NULL
				CALL display_json_object_value_list(l_json_obj)
					CALL l_json_obj.clear()


			#Clear/Delete/Initialize all elements of a JSON container			
			ON ACTION "Clear()"
				LET l_choice = fgl_winbutton("Delete ALL JSON elements?","Do you want to delete all JSON elements/data?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	CALL l_json_obj.clear()
	      END IF	

				DISPLAY l_json_obj.toString()
				CALL display_json_object_value_list(l_json_obj)
									

			#util.JSONObject.create() #Create JSONObject instance -- NULL return is error
			ON ACTION ".create()"
				LET l_choice = fgl_winbutton("Create new JSON object?","Do you want to create a new JSON object (current JSON object with all JSON elements/data will be destroyed?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	LET l_json_obj = util.JSONObject.CREATE()
	      END IF	
				
				IF l_json_obj IS NULL THEN 
					CALL fgl_winmessage("JSONObject could not be created","Failed to CREATE the JSONobject!\n","error")
				ELSE
					CALL fgl_winmessage("JSONObject was created","A JSONobject was created !\n","info")
				END IF
				DISPLAY l_json_obj.toString()
				CALL display_json_object_value_list(l_json_obj)

			#--------------------------------------------------------------
			# DISPLAY/DEBUG
			#--------------------------------------------------------------
								
			ON ACTION "DISPLAY to console"
				OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_json_obj TO console 

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hash_console
				CALL display_json_object_value_list(l_json_obj)
	
			ON ACTION "Debug Console"
				DISPLAY "*********************************"
				DISPLAY "DISPLAY l_json_obj"
				DISPLAY "*********************************"
				DISPLAY l_json_obj.toString()
				CALL display_json_object_value_list(l_json_obj)
					
			ON ACTION "DISPLAY key element"
				LET l_key = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",trim(l_json_obj.name(l_arr_curr)),30,0)

				IF l_json_obj.has(l_key) THEN	
					#OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
					#DISPLAY l_json_obj.get(l_key) TO console
					LET l_msg = "JSON element with the key ", trim(l_key), "\nhas got the value ", trim(l_json_obj.get(l_key)) 
					CALL fgl_winmessage("JSON Element",l_msg,"info")
					#MENU
					#	BEFORE MENU
					#		 CALL dialog.setActionHidden("ACCEPT",TRUE)
					#	ON ACTION "CANCEL"
					#		EXIT MENU
					#END MENU
					#CLOSE WINDOW cm_hash_console
				ELSE
					LET l_msg = "JSON Key ", l_key ," does not exist"
					CALL fgl_winmessage("Unknown JSON Key",l_msg,"info")
					ERROR l_msg
				END IF
				CALL display_json_object_value_list(l_json_obj)	
	
	
			# util.JSONObject.toString() produces a JSON string from the values included in the JSON object.
			ON ACTION "util.JSONObject.toString()"
				LET l_js = l_json_obj.toString()
				
				OPEN WINDOW cm_json_console WITH FORM "form/cm_json_console" ATTRIBUTE(BORDER, style="center")
					INPUT l_js WITHOUT DEFAULTS FROM console	
						BEFORE INPUT
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
					END INPUT
				CLOSE WINDOW cm_json_console
				CALL display_json_object_value_list(l_json_obj)	

	
			#util.JSONObject.getLength() returns the number of the name:value pairs included in the JSON object.			
			ON ACTION ".getLength()"
				LET l_msg = "JSON Object has got ", trim(l_json_obj.getLength(), " pairs of data") 	
				DISPLAY l_json_obj.getLength() TO json_getLength
				CALL fgl_winmessage("l_json_obj.getLength()",l_msg,"info")
				CALL display_json_object_value_list(l_json_obj)
					
			#util.JSONObject.get(<key>) util.JSONObject.get() returns a value that corresponds to the specified name.
			ON ACTION ".get()"
				LET l_json_key = fgl_winprompt(0, 0, "Enter the JSON Key name", NULL, 30, 0)
				LET l_msg = "l_json_obj.get(", trim(l_json_key), ") returned: ->", trim(l_json_obj.get(l_json_key)), "<-"
				CALL fgl_winmessage("JSON jsonObject.get()",l_msg,"info")
				CALL display_json_object_value_list(l_json_obj)

#		ON ACTION ".getValue()"
#			DISPLAY l_json_obj.getValue()

					
			ON ACTION ".getKey(INT index)"
				LET l_json_key = fgl_winprompt(0, 0, "Enter the JSON Key name", NULL, 30, 0)
				LET l_msg = "l_json_obj.getKey(", trim(l_json_key), ") returned: ->", trim(l_json_obj.getKey(l_json_key)), "<-"
				CALL fgl_winmessage("JSON jsonObject.getKey()",l_msg,"info")
				CALL display_json_object_value_list(l_json_obj)
							
			#util.JSONObject.getType()
			ON ACTION ".getType()"
				LET l_json_key = fgl_winprompt(0, 0, "Enter the JSON Key name", NULL, 30, 0)
				LET l_msg = "l_json_obj.getType(", trim(l_json_key), ") returned: ->", trim(l_json_obj.getType(l_json_key)), "<-"
				CALL fgl_winmessage("JSON jsonObject.getType()",l_msg,"info")
				CALL display_json_object_value_list(l_json_obj)
				
			ON ACTION ".get() and .getType()"
				CALL display_get_getType(l_json_obj)
				CALL display_json_object_value_list(l_json_obj)
								
			ON ACTION "display_get_getType"
				CALL display_console_get_getType(l_json_obj)
				CALL display_json_object_value_list(l_json_obj)
				
			ON ACTION "getType Loop"
				CALL getTypeLoop(l_json_obj)
				CALL display_json_object_value_list(l_json_obj)

			# .getKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
			ON ACTION "GetKeys()" 
				CALL l_json_obj.getKeys() RETURNING l_arr_j_key
				CALL show_key_list(l_arr_j_key)
	
			# util.JSONObject.has() checks whether the JSON object includes the element with the specified name.
			ON ACTION ".has(<key>)"
				LET l_json_key = fgl_winprompt(0, 0, "Enter the JSON Key name", NULL, 30, 0)
				LET l_msg = "l_json_obj.has(", trim(l_json_key), ") returned: ->", trim(l_json_obj.has(l_json_key)), "<-"
				IF l_json_obj.has(l_json_key) THEN 
					LET l_msg = l_msg, "\nJSON Object HAS got this key"
					CALL fgl_winmessage("JSON jsonObject.has()",l_msg,"info")
				ELSE
					LET l_msg = l_msg, "\nJSON Object HAS got this key"
					CALL fgl_winmessage("JSON jsonObject.has()",l_msg,"info")
				END IF	
				CALL display_json_object_value_list(l_json_obj)
				
		ON ACTION ".keyExists(key)"
				LET l_json_key = fgl_winprompt(0, 0, "Enter the JSON Key name", NULL, 30, 0)
				LET l_msg = "l_json_obj.keyExists(", trim(l_json_key), ") returned: ->", trim(l_json_obj.keyExists(l_json_key)), "<-"
				IF l_json_obj.keyExists(l_json_key) THEN 
					LET l_msg = l_msg, "\nJSON Object HAS got this key"
					CALL fgl_winmessage("JSON jsonObject.keyExists()",l_msg,"info")
				ELSE
					LET l_msg = l_msg, "\nJSON Object HAS got this key"
					CALL fgl_winmessage("JSON jsonObject.keyExists()",l_msg,"info")
				END IF	
				CALL display_json_object_value_list(l_json_obj)							
	
			# util.JSONObject.name() returns the name of the name:value pair by its index.
			ON ACTION ".name()" 
				LET l_index = l_json_obj.getLength() #length with be the max. index value
				IF l_index > 0 THEN
					CALL index_input(l_index) RETURNING l_index
					LET l_msg = "JSON Object index ", trim(l_index), " keeps the key ", trim(l_json_obj.name(l_index))
					CALL fgl_winmessage("util.JSONObject.name()",l_msg,"info")
				ELSE
					CALL fgl_winmessage("JSON Object is empty","JSON Object is empty","error")
				END IF
				CALL display_json_object_value_list(l_json_obj)
					
			# util.JSONObject.put() adds an element to the JSON object (= a name:value pair).
			ON ACTION ".put()"
				LET l_key = NULL
				LET l_value = NULL
				CALL input_pair(l_key,l_value) RETURNING l_key, l_value
				IF NOT int_flag THEN
					CALL l_json_obj.put(l_key,l_value)
				ELSE
					LET int_flag = FALSE
				END IF
				CALL display_json_object_value_list(l_json_obj)
					
			# util.JSONObject.remove(<key>) removes the specified element from the JSON object.			
			ON ACTION ".remove()" 
					LET l_key = fgl_winprompt(1,1,"Enter Key to be deleted",trim(l_json_obj.name(l_arr_curr)),30,0)
		  		IF l_key IS NOT NULL THEN
		  			DISPLAY "remove response: ", l_json_obj.remove(l_key)
		  		END IF
				CALL display_json_object_value_list(l_json_obj)
				
			ON ACTION "Delete(INT index)" 
				IF (l_arr_curr > 0) AND (l_arr_curr <= l_json_obj.getSize()) THEN
					CALL l_json_obj.delete(l_arr_curr)
					LET l_msg = "JSON element with index ", trim(l_arr_curr), " was removed"
					MESSAGE l_msg
				ELSE
					CALL fgl_winmessage("JSON is empty","JSON object is empty!\nCan not delete any elements","error")
				END IF
				CALL display_json_object_value_list(l_json_obj)
				
			ON ACTION "Delete(Idx first, Idx last)"				
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_index_start,l_index_end WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_index_start <= 0) OR (l_index_end <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_index_start > l_json_obj.getlength()) OR (l_index_end > l_json_obj.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_index_start > l_index_end) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_json_obj.delete(l_index_start,l_index_end)
	  			LET l_msg = "l_json_obj.delete(",trim(l_index_start), ",", trim(l_index_end), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range
				CALL display_json_object_value_list(l_json_obj)


			#util.JSONObject.fromFGL() # will overwrite any existing JSONObj data
			ON ACTION ".fromFGL(<4glRecord>)"
				LET l_json_obj = util.JSONObject.fromFGL(l_rec_data)
	
				DISPLAY l_json_obj.toString()
				CALL display_json_object_value_list(l_json_obj)

			# util.JSONObject.toFGL() fills a RECORD with the elements of the JSON object.
			ON ACTION ".toFgl()" 
				INITIALIZE l_rec_data TO NULL
				CALL l_json_obj.toFgl(l_rec_data)
				DISPLAY l_rec_data.*
				CALL display_4gl_rec_data(l_rec_data.*)
				CALL display_json_object_value_list(l_json_obj)				
#		ON ACTION "range - .name()" #util.JSONObject.name() returns the name of the name:value pair by its index.
#			CALL index_range_input(l_index_start, l_index_end) RETURNING l_index_start, l_index_end
#			
#			FOR i = l_index 
#
#		l_json_obj.name(i)			
#			
								
			#LET
			ON ACTION "LET" #currently .put()
				OPEN WINDOW cm_form_pair WITH FORM "form/cm_form_pair" ATTRIBUTE(BORDER,style="center")
				INPUT l_key WITHOUT DEFAULTS FROM j_key
				IF l_json_obj.has(l_key) THEN
					LET l_value = l_json_obj.get(l_key) 
					INPUT l_value WITHOUT DEFAULTS FROM j_value
					IF NOT int_flag THEN
						CALL l_json_obj.put(l_key,l_value)
					END IF
				ELSE
					LET l_msg = "Could not find the key ", trim(l_key), " in the JSON container"
					CALL fgl_winmessage("Not found",l_msg,"error")
				END IF
				CLOSE WINDOW cm_form_pair
				CALL display_json_object_value_list(l_json_obj)
				
	    # .insert(key,value) - Inserts a new hashMap pair / bucket 
	    ON ACTION ".insert(<key>,<value>)"
				OPEN WINDOW cm_form_pair WITH FORM "form/cm_form_pair" ATTRIBUTE(BORDER,style="center")
				INPUT l_key,l_value WITHOUT DEFAULTS FROM j_key, j_value
				
				IF int_flag THEN
					LET int_flag = FALSE
				ELSE
					IF l_json_obj.has(l_key) THEN
						CALL l_json_obj.insert(l_key,l_value)
						LET l_msg = "Element with the key ", trim(l_key), " already exists!, Update it's value to ", trim(l_value)
						MESSAGE l_msg
					ELSE
						CALL l_json_obj.insert(l_key,l_value) 
						LET l_msg = "New element with key=", trim(l_key), " and value ", trim(l_value), " was inserted."
						CALL fgl_winmessage(".insert()",l_msg,"error")
					END IF
				END IF
				CLOSE WINDOW cm_form_pair
				CALL display_json_object_value_list(l_json_obj)
#		does not exist			
#		ON ACTION ".contains()"
#			CALL l_json_obj.contains("dob")

#			ON ACTION ".CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/dictionary
#				LET l_key = fgl_winprompt(1,1,"Search",l_key,30,0)
#				IF l_json_obj.Contains(l_key) THEN				
#					CALL display_HashPair(l_key,l_hm1,"l_hm1.contains() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")
#			  ELSE
#			  	LET l_msg = "HashMap does not contain ", trim( l_key)
#			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
#				END IF

	
		#--------------------------------------------------------------
		# Miscellaneous
		#--------------------------------------------------------------

		ON ACTION "REFRESH"
			CALL display_json_object_value_list(l_json_obj)		

    ON ACTION "HELP"
    	CALL onlineHelp("JSON_Object",NULL)

	END DIALOG
	#-----------------------------------------------------
END MAIN
#####################################################################################
# END MAIN
#####################################################################################


#####################################################################################
# FUNCTION init_record()
#
#
#####################################################################################
FUNCTION init_record()
	DEFINE l_rec_data OF dt_rec_jsonObj
#	DEFINE i SMALLINT 
	LET l_rec_data.id = "HHOELZL"
	LET l_rec_data.fName = "Hubert"
	LET l_rec_data.lName = "Hölzl"
	LET l_rec_data.dob = "10/12/1986"
	LET l_rec_data.country = "Germany"
	LET l_rec_data.partner = TRUE
	LET l_rec_data.loyalty = 5
	LET l_rec_data.myNullInt = NULL
	
#	LET l_rec_data.transaction[1]= 110
#	FOR i = 2 TO 20
#		LET l_rec_data.transaction[i]= l_rec_data.transaction[i-1] + 110
#	END FOR
	
	RETURN l_rec_data.*
END FUNCTION
#####################################################################################
# END FUNCTION init_record()
#####################################################################################


#####################################################################################
# FUNCTION display_json_object_value_list(p_json_obj)
#
#
#####################################################################################
FUNCTION display_json_object_value_list(p_json_obj)
	DEFINE p_json_obj util.JSONObject
	DEFINE i SMALLINT
	
	CALL glob_arr_rec_pair.clear()
	
	FOR i = 1 TO p_json_obj.getLength()
		LET glob_arr_rec_pair[i].jKey = p_json_obj.name(i)
		LET glob_arr_rec_pair[i].jValue = p_json_obj.get(p_json_obj.name(i))
		#DEBUG
		#DISPLAY p_json_obj.name(i)
		#DISPLAY p_json_obj.get(p_json_obj.name(i))
	END FOR
 
	DISPLAY ARRAY glob_arr_rec_pair TO sc_json.* WITHOUT SCROLL
	END DISPLAY
	
END FUNCTION
#####################################################################################
# END FUNCTION display_json_object_value_list(p_json_obj)
#####################################################################################


#####################################################################################
# FUNCTION display_4gl_rec_data(l_rec_data)
#
#
#####################################################################################
FUNCTION display_4gl_rec_data(l_rec_data)
	DEFINE l_rec_data OF dt_rec_jsonObj

	OPEN WINDOW cm_json_record WITH FORM "form/cm_json_record" ATTRIBUTE(BORDER, STYLE="center")
		DISPLAY l_rec_data.* TO sc_rec_data.*

		MENU "confirm"
			BEFORE MENU
				CALL dialog.setActionHidden("CANCEL",TRUE)
			ON ACTION "ACCEPT"
				EXIT MENU
		END MENU
	CLOSE WINDOW cm_json_record
END FUNCTION
#####################################################################################
# END FUNCTION display_4gl_rec_data(l_rec_data)
#####################################################################################


#####################################################################################
# FUNCTION display_4gl_arr_rec_data(l_rec_data)
#
#
#####################################################################################
FUNCTION display_4gl_arr_rec_data(l_arr_rec_data)
	DEFINE l_arr_rec_data DYNAMIC ARRAY OF RECORD
			j_key STRING,
			j_value STRING
		END RECORD		

	OPEN WINDOW cm_json_pair_list WITH FORM "form/cm_json_pair_list" ATTRIBUTE(BORDER, STYLE="center")
		DISPLAY ARRAY l_arr_rec_data TO sc_arr_rec_data.*

#		MENU "confirm"
#			BEFORE MENU
#				CALL dialog.setActionHidden("CANCEL",TRUE)
#			ON ACTION "ACCEPT"
#				EXIT MENU
#		END MENU
	CLOSE WINDOW cm_json_pair_list
END FUNCTION
#####################################################################################
# END FUNCTION display_4gl_arr_rec_data(l_rec_data)
#####################################################################################


#####################################################################################
# FUNCTION display_get_getType(p_json_obj)
#
#
#####################################################################################
FUNCTION display_get_getType(p_json_obj)
	DEFINE p_json_obj util.JSONObject

	IF p_json_obj IS NOT NULL THEN
		CALL fgl_winmessage("NULL JSON object","JSON object is NULL","error")
		RETURN 
	END IF


	OPEN WINDOW cm_json_key_get_gettype WITH FORM "form/cm_json_key_get_gettype" ATTRIBUTE(BORDER, STYLE="center")

	DISPLAY p_json_obj.getType("id") TO type_id 	
	DISPLAY p_json_obj.getType("fname") TO type_fName 	
	DISPLAY p_json_obj.getType("lname") TO type_lName
	DISPLAY p_json_obj.getType("dob") TO type_dob
	DISPLAY p_json_obj.getType("country") TO type_country
	DISPLAY p_json_obj.getType("partner") TO type_partner
	DISPLAY p_json_obj.getType("loyalty") TO type_loyalty
	DISPLAY p_json_obj.getType("mynullint") TO type_myNullInt

	#util.JSONObject.get()
	DISPLAY p_json_obj.get("id") 	TO id
	DISPLAY p_json_obj.get("fname") 	 TO fName
	DISPLAY p_json_obj.get("lname") TO lName
	DISPLAY p_json_obj.get("dob") TO dob
	DISPLAY p_json_obj.get("country") TO country
	DISPLAY p_json_obj.get("partner") TO partner
	DISPLAY p_json_obj.get("loyalty") TO loyalty
	DISPLAY p_json_obj.get("mynullint") TO myNullInt
	
	MENU "confirm"
		BEFORE MENU
			CALL dialog.setActionHidden("CANCEL",TRUE)
		ON ACTION "ACCEPT"
			EXIT MENU
	END MENU
	
	
	CLOSE WINDOW cm_json_key_get_gettype
END FUNCTION
#####################################################################################
# END FUNCTION display_get_getType(p_json_obj)
#####################################################################################


#####################################################################################
# FUNCTION display_console_get_getType(p_json_obj)
#
#
#####################################################################################
FUNCTION display_console_get_getType(p_json_obj)
	DEFINE p_json_obj util.JSONObject
	DEFINE l_str_console STRING
	OPEN WINDOW cm_json_console WITH FORM "form/cm_json_console" ATTRIBUTE(BORDER, STYLE="center")

	LET l_str_console = l_str_console, "\n", "id=: ", 			p_json_obj.get("id"), " type: ", p_json_obj.getType("id") 	
	LET l_str_console = l_str_console, "\n", "fName=",			p_json_obj.get("id"), " type: ", p_json_obj.getType("fName") 	
	LET l_str_console = l_str_console, "\n", "lName=: ", 		p_json_obj.get("id"), " type: ", p_json_obj.getType("lName") 
	LET l_str_console = l_str_console, "\n", "dob=: ", 			p_json_obj.get("id"), " type: ", p_json_obj.getType("dob")
	LET l_str_console = l_str_console, "\n", "country=: ",	p_json_obj.get("id"), " type: ", p_json_obj.getType("country")
	LET l_str_console = l_str_console, "\n", "partner=: ",	p_json_obj.get("id"), " type: ", p_json_obj.getType("partner")
	LET l_str_console = l_str_console, "\n", "loyalty=: ",	p_json_obj.get("id"), " type: ", p_json_obj.getType("loyalty")
	LET l_str_console = l_str_console, "\n", "myNullInt=: ",p_json_obj.get("id"), " type: ", p_json_obj.getType("myNullInt")

	INPUT l_str_console WITHOUT DEFAULTS FROM console	
	
	CLOSE WINDOW cm_json_console
END FUNCTION
#####################################################################################
# END FUNCTION display_console_get_getType(p_json_obj)
#
#####################################################################################


#####################################################################################
# FUNCTION index_input(p_index)
#
#
#####################################################################################
FUNCTION index_input(p_index)
	DEFINE p_index SMALLINT
	DEFINE l_index_max SMALLINT	
	LET l_index_max = p_index
	
	OPEN WINDOW cm_json_index WITH FORM "form/cm_json_index" ATTRIBUTE(BORDER,style="center")
	INPUT p_index WITHOUT DEFAULTS FROM json_index
		AFTER INPUT
			IF p_index <= 0 OR p_index > l_index_max THEN
				CONTINUE INPUT
			END IF
		END INPUT
			 	
	IF int_flag THEN
		MESSAGE "User abborted input"
	END IF
		
	CLOSE WINDOW cm_json_index
	RETURN p_index	
END FUNCTION
#####################################################################################
# END FUNCTION index_input(p_index)
#####################################################################################


#####################################################################################
# FUNCTION index_range_input(p_index_start, p_index_end)
#
#
#####################################################################################
FUNCTION index_range_input(p_index_start, p_index_end)
	DEFINE p_index_start SMALLINT
	DEFINE p_index_end SMALLINT
	DEFINE l_index_max SMALLINT	
	LET l_index_max = p_index_end
	
	OPEN WINDOW cm_json_range_index WITH FORM "form/cm_json_range_index"
	INPUT p_index_start, p_index_end WITHOUT DEFAULTS FROM index_start, index_end
		AFTER INPUT
			IF p_index_start <= 0 
			OR p_index_start > l_index_max
			OR p_index_end > l_index_max
			OR p_index_end <= 0 THEN
			CONTINUE INPUT
		END IF
	END INPUT
			 	
	IF int_flag THEN
		MESSAGE "User abborted input"
	END IF
		
	CLOSE WINDOW cm_json_range_index
	RETURN p_index_start, p_index_end	
END FUNCTION
#####################################################################################
# END FUNCTION index_range_input(p_index_start, p_index_end)
#####################################################################################


#####################################################################################
# FUNCTION input_pair(p_key,p_value)
#
#
#####################################################################################
FUNCTION input_pair(p_key,p_value)
	DEFINE p_key VARIANT
	DEFINE p_value VARIANT
	
	OPEN WINDOW cm_form_pair WITH FORM "form/cm_form_pair" ATTRIBUTE(BORDER,style="center")
	INPUT p_key,p_value WITHOUT DEFAULTS FROM j_key,j_value
	CLOSE WINDOW cm_form_pair
	
	RETURN p_key,p_value
END FUNCTION
#####################################################################################
# END FUNCTION input_pair(p_key,p_value)
#####################################################################################


#####################################################################################
# FUNCTION get_rec_data(p_jsonObj)
#
#
#####################################################################################
FUNCTION get_rec_data(p_json_obj)
	DEFINE p_json_obj util.JSONObject
	DEFINE l_rec_json_data OF dt_rec_json_struct			

	LET l_rec_json_data.key_id = "id"
	LET l_rec_json_data.key_fName = "fname"
	LET l_rec_json_data.key_lName = "lname"
	LET l_rec_json_data.key_dob = "dob"
	LET l_rec_json_data.key_country = "country"
	LET l_rec_json_data.key_partner = "partner"
	LET l_rec_json_data.key_loyalty = "loyalty"
	LET l_rec_json_data.key_myNullInt = "mynullint"

	LET l_rec_json_data.id = p_json_obj.get(l_rec_json_data.key_id )
	LET l_rec_json_data.fName = p_json_obj.get(l_rec_json_data.key_fName)
	LET l_rec_json_data.lName = p_json_obj.get(l_rec_json_data.key_lName)
	LET l_rec_json_data.dob = p_json_obj.get(l_rec_json_data.key_dob)
	LET l_rec_json_data.country = p_json_obj.get(l_rec_json_data.key_country)
	LET l_rec_json_data.partner = p_json_obj.get(l_rec_json_data.key_partner)
	LET l_rec_json_data.loyalty = p_json_obj.get(l_rec_json_data.key_loyalty)
	LET l_rec_json_data.myNullInt = p_json_obj.get(l_rec_json_data.key_myNullInt)
	
	RETURN l_rec_json_data.*
END FUNCTION 
#####################################################################################
# END FUNCTION get_rec_data(p_jsonObj)
#####################################################################################


#####################################################################################
# FUNCTION getTypeLoop(p_json_obj)
#
#
#####################################################################################
FUNCTION getTypeLoop(p_json_obj)
	DEFINE p_json_obj util.JSONObject
	DEFINE l_str STRING
	DEFINE l_cnt_string SMALLINT
	DEFINE l_cnt_number SMALLINT
	DEFINE l_cnt_boolean SMALLINT
	DEFINE l_cnt_object SMALLINT
	DEFINE l_cnt_array SMALLINT
	DEFINE l_cnt_null SMALLINT
	DEFINE l_cnt_other SMALLINT
	DEFINE i SMALLINT
	
	OPEN WINDOW cm_json_console WITH FORM "form/cm_json_console" ATTRIBUTE(BORDER, style="center")
					
	FOR i = 1 TO p_json_obj.getLength()
		LET l_str = l_str, "type=", p_json_obj.getType(p_json_obj.name(i)), " (", trim(p_json_obj.name(i)), "=", trim(p_json_obj.get(p_json_obj.name(i))),")\n"
		
		#Just some statistic on how many dataType are used of each type in this JSON
		CASE p_json_obj.getType(p_json_obj.name(i))
			WHEN "string" 
				LET l_cnt_string = l_cnt_string + 1
			WHEN "boolean" 
				LET l_cnt_boolean = l_cnt_boolean + 1
			WHEN "number" 
				LET l_cnt_number = l_cnt_number + 1
			WHEN "object" 
				LET l_cnt_object = l_cnt_object + 1
			WHEN "array" 
				LET l_cnt_array = l_cnt_array + 1
			WHEN "null" 
				LET l_cnt_null = l_cnt_null + 1
			OTHERWISE 
				LET l_cnt_other = l_cnt_other + 1
 				 		
		END CASE	
	END FOR

	LET l_str = l_str, 
		"\n", "STRING:",	l_cnt_string,
		"\n", "NUMBER:",	l_cnt_number,
		"\n", "BOOLEAN:",	l_cnt_boolean,
		"\n", "NULL:",		l_cnt_null,
		"\n", "OBJECT:",	l_cnt_object,
		"\n", "ARRAY:",		l_cnt_array,
		"\n", "Other:",		l_cnt_other
	
	DISPLAY l_str TO console
	
	MENU "confirm"
		BEFORE MENU
			CALL dialog.setActionHidden("CANCEL",TRUE)
		ON ACTION "ACCEPT"
			EXIT MENU
	END MENU
	CLOSE WINDOW cm_json_console
END FUNCTION
#####################################################################################
# END FUNCTION getTypeLoop(p_json_obj)
#####################################################################################