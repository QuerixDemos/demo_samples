DEFINE dt_rec_HASH_VALUE TYPE AS RECORD
	fName STRING,
	lName STRING,
	dob DATE,
	country STRING
END RECORD
DEFINE dt_rec_HASH_ELEMENT TYPE AS RECORD
	hKey REFERENCE,
	hValue dt_rec_HASH_VALUE
END RECORD

    
##################################################################################
# MAIN
#
#
##################################################################################     
MAIN
  DEFINE l_ht_tmp HASHMAP OF dt_rec_HASH_VALUE	#hashMap for temp operations

  DEFINE l_hm1 HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 1
  DEFINE l_hm1_row_hValue OF dt_rec_HASH_VALUE
	DEFINE l_hm1_row_hkey VARIANT
  DEFINE l_hm1_idx SMALLINT			#array index for l_hm1

  DEFINE l_hm2 HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 2
  DEFINE l_hm2_row_hValue OF dt_rec_HASH_VALUE
	DEFINE l_hm2_row_hkey VARIANT
  DEFINE l_hm2_idx SMALLINT			#array index for l_hm2
  
  DEFINE i INT
	DEFINE l_hkey VARIANT #temp key for operations like delete...
	DEFINE l_hvalue VARIANT
  DEFINE l_rec_hvalue dt_rec_HASH_VALUE

  DEFINE l_idx1 SMALLINT
  DEFINE l_idx2 SMALLINT
	DEFINE l_msg STRING
	DEFINE l_search STRING	#??variant ? key ?
	DEFINE l_arr_hkey DYNAMIC ARRAY OF VARIANT #used to demonstrate the method .getKeys() for hash keys
	DEFINE l_target_hash_id STRING
	
  
  DEFINE l_jsonObj util.JSONObject
	DEFINE l_json_string STRING
	DEFINE l_choice CHAR(1)

	DEFER INTERRUPT
	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	
	CALL ui_init()
	
	OPEN WINDOW w_cm_hash_record WITH FORM "form/cm_hash_record"
	CALL ui.Interface.setText("HashMap Record")
  	  
  CALL init(l_hm1, l_hm2) 
  
  CALL update_display(l_hm1, l_hm2)

  ##############################
	# DIALOG START
	###############################
  DIALOG ATTRIBUTE(UNBUFFERED)
		#------------------------------------------------------------------
  	# l_hm1 HashMap 1 
		#------------------------------------------------------------------

	  INPUT ARRAY l_hm1 WITHOUT DEFAULTS FROM sc_hm1_inp.* 
	  	BEFORE ROW
	  		LET l_hm1_idx = DIALOG.getCurrentRow("sc_hm1_inp")	
	  			
	  		IF l_hm1_idx > 0 THEN
	  			LET l_hm1_row_hkey = l_hm1.GetKey(l_hm1_idx)
					LET l_hm1_row_hValue.fname =   l_hm1[l_hm1_row_hkey].fname
					LET l_hm1_row_hValue.lname =   l_hm1[l_hm1_row_hkey].lname
					LET l_hm1_row_hValue.dob =     l_hm1[l_hm1_row_hkey].dob
					LET l_hm1_row_hValue.country = l_hm1[l_hm1_row_hkey].country
				END IF

				DISPLAY l_hm1_row_hkey TO hm1_row_hKey
				DISPLAY l_hm1_row_hValue.* TO sc_hm1_value.* 
	  		DISPLAY l_hm1_idx TO hm1_bucketArrIndex
				  
	  	#############################################################	
			#dataType Methods
			#############################################################
			# .getSize() - returns count of elements in hash map	  		
	  	ON ACTION "getSize()"
	  		LET l_msg = "The size of l_hm1 -> l_hm1.getSize() = ", trim(l_hm1.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			# .delete(INT index) - Deletes element with index
			On ACTION "Delete(INT index)" 
				IF l_hm1_idx > 0 THEN
	  			CALL l_hm1.delete(l_hm1_idx)
	  			LET l_msg = "l_hm1.delete(",trim(l_hm1_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF
	  		
	  	# .delete(INT first, INT last) - Deletes elements/range from first to the last	
			ON ACTION "Delete(INT first, INT last)"				
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_hm1.getlength()) OR (l_idx2 > l_hm1.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_hm1.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_hm1.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range

			# .remove(VARIANT key) - Deletes element with key 'key'
			ON ACTION "Remove(VARIANT key)" 				
				LET l_hkey = fgl_winprompt(1,1,"Enter Key to be deleted for the selected table",l_hm1_row_hkey,30,0)
	  		IF l_hkey IS NOT NULL THEN
	  			CALL l_hm1.remove(l_hkey)
	  			DISPLAY "Removed HashMap element with key: ", trim(l_hkey)
	  		END IF

			# .clear() - Removes all elements from hash map
	    ON ACTION "Clear()"
				LET l_choice = fgl_winbutton("Delete ALL hashMap buckets?","Do you want to delete all HashMap buckets/data?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	CALL l_hm1.clear()
	      END IF	      


			# .keyExists(key variant) - Returns true/false if the key exists
			ON ACTION "keyExists(key)"
#				CALL fgl_winmessage(".keyExists(key) Needs implementing","This Method .keyExists(key) needs implementing","ERROR")
	  		IF l_hm1_idx > 0 THEN
					LET l_hkey = fgl_winprompt(1,1,"Enter Key to be checked for existence (for the selected table)",l_hm1_row_hkey,30,0)	  		
	  			#LET l_hm1_row_hkey = l_hm1.GetKey(l_hkey)
					IF l_hm1.keyExists(l_hkey) THEN
						LET l_msg = "Key ", trim(l_hkey), " does exist (TRUE)"
						CALL fgl_winmessage(".keyExists()",l_msg,"INFO")
					ELSE
						LET l_msg = "Key ", trim(l_hkey), " does NOT exist (FALSE)"
						CALL fgl_winmessage(".keyExists()",l_msg,"WARNING")
					END IF					
				END IF
			# .valueExists(value) - Returns true/false if this value exists in the hashMap
			ON ACTION "valueExists(value)"
#				CALL fgl_winmessage(".valueExists(value) Needs implementing","This Method .valueExists(value) needs implementing","ERROR")
	  		IF l_hm1_idx > 0 THEN
					LET l_hvalue = fgl_winprompt(1,1,"Enter the value to be checked for existence (for the selected table)",NULL,30,0)	  		
	  			IF l_hvalue IS NOT NULL THEN
						IF l_hm1.valueExists(l_hvalue) THEN
							LET l_msg = "Value ", trim(l_hvalue), " does exist (TRUE)"
							CALL fgl_winmessage(".valueExists()",l_msg,"INFO")
						ELSE
							LET l_msg = "Value ", trim(l_hvalue), " does NOT exist (FALSE)"
							CALL fgl_winmessage(".valueExists()",l_msg,"ERROR")
						END IF
					END IF			
				END IF

			# .getKey(INT index) - Returns element's value by index
			ON ACTION "GetKey(INT index)" 
				MESSAGE "l_hm1_idx=", trim(l_hm1_idx)
				LET l_msg ="getKey(", trim(l_hm1_idx),  ")\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			# .getValue(INT index) - Returns element's VALUE by index
			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_hm1_idx),  ")", "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 1)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")

			# .contains(key) - checks if an element with the given key exists in the hashMap
			ON ACTION "CONTAINS(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm1_row_hkey,30,0)
				IF l_hm1.Contains(l_hkey) THEN				
					CALL display_HashPair(l_hkey, l_hm1[l_hkey].*,"l_hm1.contains() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

				
			# .find(key) - checks if the key exists, if true, return value otherwise return NULL
			ON ACTION "Find(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm1_row_hkey,30,0)
				
				IF l_hm1.find(l_hkey) THEN
					CALL l_hm1.find(l_hkey) RETURNING l_rec_hvalue.*  				
					CALL display_HashPair(l_hkey, l_rec_hvalue.*,"l_hm1.find() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .getKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
				CALL l_hm1.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			# KeySort(INT direction) - !!! BY VALUE !!!
			# sorts HashMap by key. Any further operations, like Insert, Delete and etc., 
			# will be much slower, just because any such operation will trigger 
			# sorting by key. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			# 'direction’ is an optional parameter, it’s 1 by default.

			# .keySort(1) Ascending - sorts HashMap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) ASC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm1.keySort(1)
				MESSAGE "l_hm1: Sorted by key in ascending order"

			# .keySort(0) Descending- sorts HashMap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) DESC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm1.keySort(0)
				MESSAGE "l_hm1: Sorted by key in ascending order"

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts HashMap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			# .valueSort(1) Ascending - sorts HashMap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) ASC" #sorts HashMap by value. Any further operations, like Insert will perform slower
				CALL l_hm1.ValueSort(1)
				MESSAGE "l_hm1: Sorted by value in ascending order"

			# .valueSort(0) Descending - sorts HashMap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) DESC" #sorts HashMap by value. Any further operations, like Insert will perform slower
				CALL l_hm1.ValueSort(0)
				MESSAGE "l_hm1: Sorted by value in ascending order"


			#--------------------------------------------------------------------------
			# .customSort(funcName) - 
			# sorts HashMap using defined function. The function is a binary function 
			# that accepts two elements of the HashMap, like in the below example.
			#--------------------------------------------------------------------------

			# .customSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
				MESSAGE "Sorting by DOB (date of birth) in Ascending Order"
				CALL l_hm1.CustomSort("CompareAsc")

			# .customSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareDESC" 
				MESSAGE "Sorting by DOB (date of birth) in Descending Order"
				CALL l_hm1.CustomSort("CompareDESC")

			# .resetSorting() - resets value sorts and back HashMap to the initial sorting.
			ON ACTION "ResetSorting()" 
				CALL l_hm1.resetSorting()

			# .copyTo(HASHMAP dst) - Copies all elements of the HashMap into another HashMap.
			ON ACTION "CopyTo(hashMap)" 
				
				LET l_target_hash_id = get_target("hm1","hm2","Specify the COPY Target hashMap")
				MESSAGE "COPY ", trim(l_target_hash_id), " to l_hm1"
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.CopyTo(l_hm1)
						WHEN "hm2"
							CALL l_hm1.CopyTo(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF

#########################################################

			# LET and  .insert() work identical except, insert() can only insert new pairs in haschBuckets
			# but LET can also be used to change values
			# To change a key, you need to insert a new hashMapBucket and delete the old one

			ON ACTION "LET"
				CALL input_HashPair(l_hm1_row_hkey, l_hm1_row_hValue.*) RETURNING l_hm1_row_hkey, l_hm1_row_hValue.*
				IF NOT int_flag THEN
					LET l_hm1[l_hm1_row_hkey].* = l_hm1_row_hValue.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF

	    # .insert(key,value) - Inserts a new hashMap pair / bucket  
	    ON ACTION "INSERT"
	    	LET l_hkey = l_hm1_row_hkey
	    	CALL input_HashPair(l_hkey, l_hm1_row_hValue.*) RETURNING l_hkey, l_hm1_row_hValue.*

	    	IF l_hkey = l_hm1_row_hkey THEN
	    		MESSAGE "insert ignores this operation because this key already exists"
	    		CALL l_hm1.Insert(l_hkey, l_hm1_row_hValue)
	    	ELSE
	      	CALL l_hm1.Insert(l_hkey, l_hm1_row_hValue)
	      END IF

			# .join() - Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' HashMap
			ON ACTION "Join(hashMap)" 
				
				LET l_target_hash_id = get_target("hm1","hm2","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_hm1 with ", trim(l_target_hash_id)
				
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.Join(l_hm1)
						WHEN "hm2"
							CALL l_hm1.Join(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			#DISPLAY l_hm1 #to console in json format for debugging purpose
			ON ACTION "DISPLAY HashMap TO console"
				OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_hm1 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hash_console

			# DISPLAY the value of a hashMap bucket
			# NOTE: if the key does not exist, it will insert a new bucket with this key
			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_hm1_row_hkey,30,0)

				IF l_hm1.Contains(l_hkey) THEN	
					OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_hm1[l_hkey] TO console 
	
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hash_console
				ELSE
					LET l_msg = "Hash Key ", l_hkey ," does not exist"
					CALL fgl_winmessage("Unknown HashKey",l_msg,"info")
					ERROR l_msg
				END IF

			# util.JSONObject.toFGL() / .util.JSON.parse()
			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #Append
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_hm1.Join(l_ht_tmp)
						
						WHEN "O" #Overwrite
							CALL util.JSON.parse(l_json_string, l_hm1)
							
						WHEN "C" #Cancel
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
				
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_hm1) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hash_console
	  END INPUT

		#INPUT & Update current HM1 Hash Row Record
		INPUT l_hm1_row_hkey,l_hm1_row_hValue.* FROM sc_hm1_rec_row.*
			ON ACTION "act_save_hm1_row_rec"
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_row_hValue) #INSERT() needs the record (not the expanded .* record values)
		END INPUT

		#------------------------------------------------------------------
  	# l_hm2 HashMap 2 
		#------------------------------------------------------------------

	  INPUT ARRAY l_hm2 WITHOUT DEFAULTS FROM sc_hm2_inp.* 
	  	BEFORE ROW
	  		LET l_hm2_idx = DIALOG.getCurrentRow("sc_hm2_inp")	
	  			
	  		IF l_hm2_idx > 0 THEN
	  			LET l_hm2_row_hkey = l_hm2.GetKey(l_hm2_idx)
					LET l_hm2_row_hValue.fname =   l_hm2[l_hm2_row_hkey].fname
					LET l_hm2_row_hValue.lname =   l_hm2[l_hm2_row_hkey].lname
					LET l_hm2_row_hValue.dob =     l_hm2[l_hm2_row_hkey].dob
					LET l_hm2_row_hValue.country = l_hm2[l_hm2_row_hkey].country
				END IF

				DISPLAY l_hm2_row_hkey TO hm2_row_hKey
				DISPLAY l_hm2_row_hValue.* TO sc_hm2_value.* 
	  		DISPLAY l_hm2_idx TO hm2_bucketArrIndex
				  
	  	#############################################################	
			#dataType Methods
			#############################################################
			# .getSize() - returns count of elements in hash map	  		
	  	ON ACTION "getSize()"
	  		LET l_msg = "The size of l_hm2 -> l_hm2.getSize() = ", trim(l_hm2.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			# .delete(INT index) - Deletes element with index
			On ACTION "Delete(INT index)" 
				IF l_hm2_idx > 0 THEN
	  			DISPLAY l_hm2.delete(l_hm2_idx)
	  			LET l_msg = "l_hm2.delete(",trim(l_hm2_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF
	  		
	  	# .delete(INT first, INT last) - Deletes elements/range from first to the last	
			ON ACTION "Delete(INT first, INT last)"				
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_hm2.getlength()) OR (l_idx2 > l_hm2.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_hm2.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_hm2.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range

			# .remove(VARIANT key) - Deletes element with key 'key'
			ON ACTION "Remove(VARIANT key)" 				
				LET l_hkey = fgl_winprompt(1,1,"Enter Key to be deleted for the selected table",l_hm2_row_hkey,30,0)
	  		IF l_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_hm2.remove(l_hkey)
	  		END IF

			# .clear() - Removes all elements from hash map
	    ON ACTION "Clear()"
				LET l_choice = fgl_winbutton("Delete ALL hashMap buckets?","Do you want to delete all HashMap buckets/data?","No","Yes|No","question")
				IF l_choice = "Y"	THEN    	
	      	CALL l_hm2.clear()
	      END IF	      


			# .keyExists(key variant) - Returns true/false if the key exists
			ON ACTION "keyExists(key)"
				CALL fgl_winmessage(".keyExists(key) Needs implementing","This Method .keyExists(key) needs implementing","ERROR")

#				IF l_hm2.keyExists() THEN
#					CALL fgl_winmessage(".keyExists()","Key does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".keyExists()","Key does NOT exist (TRUE)","ERROR")
#				END IF					

			# .valueExists(value) - Returns true/false if this value exists in the hashMap
			ON ACTION "valueExists(value)"
				CALL fgl_winmessage(".valueExists(value) Needs implementing","This Method .valueExists(value) needs implementing","ERROR")

#				IF l_hm2.valueExists() THEN
#					CALL fgl_winmessage(".valueExists()","Value does exist (TRUE)","INFO")
#				ELSE
#					CALL fgl_winmessage(".valueExists()","Value does NOT exist (TRUE)","ERROR")
#				END IF			


			# .getKey(INT index) - Returns element's value by index
			ON ACTION "GetKey(INT index)" 
				MESSAGE "l_hm2_idx=", trim(l_hm2_idx)
				LET l_msg ="getKey(", trim(l_hm2_idx),  ")\nKey = ", trim(l_hm2.GetKey(l_hm2_idx)), "\nValue=",trim(l_hm2.getValue(l_hm2_idx)),"\nArray index=", trim(l_hm2_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			# .getValue(INT index) - Returns element's VALUE by index
			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_hm2_idx),  ")", "\nValue=",trim(l_hm2.getValue(l_hm2_idx)),"\nKey = ", trim(l_hm2.GetKey(l_hm2_idx)), "\nArray index=", trim(l_hm2_idx) , "\n(current row in HashMap 1)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")

			# .contains(key) - checks if an element with the given key exists in the hashMap
			ON ACTION "CONTAINS(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm2_row_hkey,30,0)
				IF l_hm2.Contains(l_hkey) THEN				
					CALL display_HashPair(l_hkey, l_hm2[l_hkey].*,"l_hm2.contains() returned true / (l_hm2 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

				
			# .find(key) - checks if the key exists, if true, return value otherwise return NULL
			ON ACTION "Find(<key>VARIANT)" 
				LET l_hkey = fgl_winprompt(1,1,"Search",l_hm2_row_hkey,30,0)
				
				IF l_hm2.find(l_hkey) THEN
					CALL l_hm2.find(l_hkey) RETURNING l_rec_hvalue.*  				
					CALL display_HashPair(l_hkey, l_rec_hvalue.*,"l_hm2.find() returned true / (l_hm2 contains key)",".contains(key) method returns true if the key exists")				
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			# .getKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the HashMap.
				CALL l_hm2.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			# KeySort(INT direction) - !!! BY VALUE !!!
			# sorts HashMap by key. Any further operations, like Insert, Delete and etc., 
			# will be much slower, just because any such operation will trigger 
			# sorting by key. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			# 'direction’ is an optional parameter, it’s 1 by default.

			# .keySort(1) Ascending - sorts HashMap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) ASC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm2.keySort(1)
				MESSAGE "l_hm2: Sorted by key in ascending order"

			# .keySort(0) Descending- sorts HashMap by key. Any further operations, like Insert will perform slower
			ON ACTION "KeySort(INT direction) DESC" 
				#Function needs implementing
				CALL fgl_winmessage(".KeySort()","Method .keySort() needs implementing","error")
#				CALL l_hm2.keySort(0)
				MESSAGE "l_hm2: Sorted by key in ascending order"

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts HashMap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			# .valueSort(1) Ascending - sorts HashMap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) ASC" #sorts HashMap by value. Any further operations, like Insert will perform slower
				CALL l_hm2.ValueSort(1)
				MESSAGE "l_hm2: Sorted by value in ascending order"

			# .valueSort(0) Descending - sorts HashMap by value. Any further operations, like Insert will perform slower
			ON ACTION "ValueSort(INT direction) DESC" #sorts HashMap by value. Any further operations, like Insert will perform slower
				CALL l_hm2.ValueSort(0)
				MESSAGE "l_hm2: Sorted by value in ascending order"


			#--------------------------------------------------------------------------
			# .customSort(funcName) - 
			# sorts HashMap using defined function. The function is a binary function 
			# that accepts two elements of the HashMap, like in the below example.
			#--------------------------------------------------------------------------

			# .customSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
				MESSAGE "Sorting by DOB (date of birth) in Ascending Order"
				CALL l_hm2.CustomSort("CompareAsc")

			# .customSort(STRING funcName) - sorts HashMap using defined function. The function is a binary function that accepts two elements of the HashMap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareDESC" 
				MESSAGE "Sorting by DOB (date of birth) in Descending Order"
				CALL l_hm2.CustomSort("CompareDESC")

			# .resetSorting() - resets value sorts and back HashMap to the initial sorting.
			ON ACTION "ResetSorting()" 
				CALL l_hm2.resetSorting()

			# .copyTo(HASHMAP dst) - Copies all elements of the HashMap into another HashMap.
			ON ACTION "CopyTo(hashMap)" 
				
				LET l_target_hash_id = get_target("hm2","hm2","Specify the COPY Target hashMap")
				MESSAGE "COPY ", trim(l_target_hash_id), " to l_hm2"
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm2"
							CALL l_hm2.CopyTo(l_hm2)
						WHEN "hm2"
							CALL l_hm2.CopyTo(l_hm2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF

#########################################################

			# LET and  .insert() work identical except, insert() can only insert new pairs in haschBuckets
			# but LET can also be used to change values
			# To change a key, you need to insert a new hashMapBucket and delete the old one

			ON ACTION "LET"
				CALL input_HashPair(l_hm2_row_hkey, l_hm2_row_hValue.*) RETURNING l_hm2_row_hkey, l_hm2_row_hValue.*
				IF NOT int_flag THEN
					LET l_hm2[l_hm2_row_hkey].* = l_hm2_row_hValue.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF

	    # .insert(key,value) - Inserts a new hashMap pair / bucket  
	    ON ACTION "INSERT"
	    	LET l_hkey = l_hm2_row_hkey
	    	CALL input_HashPair(l_hkey, l_hm2_row_hValue.*) RETURNING l_hkey, l_hm2_row_hValue.*

	    	IF l_hkey = l_hm2_row_hkey THEN
	    		MESSAGE "insert ignores this operation because this key already exists"
	    		CALL l_hm2.Insert(l_hm2_row_hkey, l_hm2_row_hValue)
	    	ELSE
	      	CALL l_hm2.Insert(l_hm2_row_hkey, l_hm2_row_hValue)
	      END IF

			# .join() - Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' HashMap
			ON ACTION "Join(hashMap)" 
				
				LET l_target_hash_id = get_target("hm2","hm2","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_hm2 with ", trim(l_target_hash_id)
				
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm2"
							CALL l_hm2.Join(l_hm2)
						WHEN "hm2"
							CALL l_hm2.Join(l_hm1)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			#DISPLAY l_hm2 #to console in json format for debugging purpose
			ON ACTION "DISPLAY HashMap TO console"
				OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_hm2 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hash_console

			# DISPLAY the value of a hashMap bucket
			# NOTE: if the key does not exist, it will insert a new bucket with this key
			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_hm2_row_hkey,30,0)

				IF l_hm2.Contains(l_hkey) THEN	
					OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_hm2[l_hkey] TO console 
	
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hash_console
				ELSE
					LET l_msg = "Hash Key ", l_hkey ," does not exist"
					CALL fgl_winmessage("Unknown HashKey",l_msg,"info")
					ERROR l_msg
				END IF

			# util.JSONObject.toFGL() / .util.JSON.parse()
			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #Append
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_hm2.Join(l_ht_tmp)
						
						WHEN "O" #Overwrite
							CALL util.JSON.parse(l_json_string, l_hm2)
							
						WHEN "C" #Cancel
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
				
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_hm2) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hash_console
	  END INPUT

		#INPUT & Update current hm2 Hash Row Record
		INPUT l_hm2_row_hkey,l_hm2_row_hValue.* FROM sc_hm2_rec_row.*
			ON ACTION "act_save_hm2_row_rec"
				CALL l_hm2.Insert(l_hm2_row_hkey, l_hm2_row_hValue) #INSERT() needs the record (not the expanded .* record values)
		END INPUT

		
		#DIALOG Actions
		#------------------------------------------------

		ON ACTION "act_clear_hm1_row_rec"
			INITIALIZE l_hm1_row_hValue.* TO NULL

		ON ACTION "act_clear_hm2_row_rec"
			INITIALIZE l_hm1_row_hValue.* TO NULL

		ON ACTION "REFRESH" #"DISPLAY UPDATE"
			CALL update_display(l_hm1,l_hm2)		

		ON ACTION "EXIT"
			EXIT DIALOG

	END DIALOG 
	#END DIALOG --------------------------------------------------------------------

END MAIN
##################################################################################
# END MAIN
##################################################################################     



##################################################################################
# FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
#
#
##################################################################################  
FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
	DEFINE p_rec_dict1 OF dt_rec_HASH_ELEMENT 
	DEFINE p_rec_dict2 OF dt_rec_HASH_ELEMENT 
	
  RETURN p_rec_dict1.hValue.dob < p_rec_dict2.hValue.dob
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
##################################################################################  


##################################################################################
# FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
#
#
##################################################################################  
FUNCTION CompareDesc(p_rec_dict1, p_rec_dict2)
	DEFINE p_rec_dict1 OF dt_rec_HASH_ELEMENT 
	DEFINE p_rec_dict2 OF dt_rec_HASH_ELEMENT 

	RETURN p_rec_dict1.hValue.dob >= p_rec_dict2.hValue.dob
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
##################################################################################  


##################################################################################
# FUNCTION update_display(p_hm1,p_hm2)
#
#
##################################################################################     
FUNCTION update_display(p_hm1,p_hm2)
	DEFINE p_hm1 HASHMAP OF dt_rec_HASH_VALUE
	DEFINE p_hm2 HASHMAP OF dt_rec_HASH_VALUE
	 
	DISPLAY ARRAY p_hm1 TO sc_hm1_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
	DISPLAY ARRAY p_hm2 TO sc_hm2_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
			
END FUNCTION
##################################################################################
# END FUNCTION update_display(p_hm1,p_hm2)
##################################################################################     


##################################################################################
# FUNCTION init()
#
#
##################################################################################     
FUNCTION init(p_hm1, p_hm2)
	DEFINE l_hkey STRING
	DEFINE l_rec_hvalue dt_rec_HASH_VALUE
  DEFINE p_hm1 HASHMAP OF dt_rec_HASH_VALUE
  DEFINE p_hm2 HASHMAP OF dt_rec_HASH_VALUE
	DEFINE l_arr_size SMALLINT
  DEFINE i SMALLINT
  DEFINE l_date DATE

  LET l_arr_size = 10
  
  FOR i = 1 TO l_arr_size
  	LET l_hkey = "hm1_key_", trim(i)

  	LET l_rec_hvalue.fName = "hm1_value_fname_", trim(i)
  	LET p_hm1[l_hkey].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "hm1_value_lName_", trim(i)
  	LET p_hm1[l_hkey].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 150*i
  	LET l_rec_hvalue.dob = l_date #"hm1_value_dob_", trim(i)
  	LET p_hm1[l_hkey].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "hm1_value_country_", trim(i)
  	LET p_hm1[l_hkey].country = l_rec_hvalue.country

  END FOR

  FOR i = 1 TO l_arr_size
  	LET l_hkey = "hm2_key_", trim(i)

  	LET l_rec_hvalue.fName = "hm2_value_fname_", trim(i)
  	LET p_hm2[l_hkey].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "hm2_value_lName_", trim(i)
  	LET p_hm2[l_hkey].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 200*i
  	LET l_rec_hvalue.dob = l_date #"hm2_value_dob_", trim(i)
  	LET p_hm2[l_hkey].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "hm2_value_country_", trim(i)
  	LET p_hm2[l_hkey].country = l_rec_hvalue.country

  END FOR    

END FUNCTION
##################################################################################
# END FUNCTION init()
##################################################################################     


##################################################################################
# FUNCTION debug(p_hm1, p_hm2)
#
#
##################################################################################     
FUNCTION debug(p_hm1, p_hm2)
  DEFINE p_hm1  HASHMAP OF STRING
  DEFINE p_hm2  HASHMAP OF STRING
 
	DEFINE i SMALLINT
	
  DISPLAY "HashMap 1 data:"
  FOR i = 1 TO p_hm1.GetLength()
    DISPLAY "'", p_hm1.GetKey(i), "' - '", p_hm1.GetValue(i), "'"
  END FOR

  DISPLAY "HashMap 2 data:"
  FOR i = 1 TO p_hm2.GetLength()
    DISPLAY "'", p_hm2.GetKey(i), "' - '", p_hm2.GetValue(i), "'"
  END FOR

END FUNCTION
##################################################################################
# END FUNCTION debug(p_hm1, p_hm2)
##################################################################################



##################################################################################
# FUNCTION input_HashPair(p_hkey, p_rec_hvalue)
#
#
##################################################################################
FUNCTION input_HashPair(p_hkey, p_rec_hvalue)
	DEFINE p_hkey VARIANT
	DEFINE p_rec_hvalue OF dt_rec_HASH_VALUE
	
	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_record" ATTRIBUTE(BORDER,style="center")

	INPUT p_hkey,p_rec_hvalue.* WITHOUT DEFAULTS FROM sc_hashPair.* ATTRIBUTE(UNBUFFERED) 
	END INPUT
	
	CLOSE WINDOW w_hashPair
	RETURN p_hkey,p_rec_hvalue.*
END FUNCTION
##################################################################################
# END FUNCTION input_HashPair(p_rec_hash_pair)
##################################################################################


##################################################################################
# FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
#
#
##################################################################################
FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
	DEFINE p_hkey VARIANT
	DEFINE p_rec_hvalue OF dt_rec_HASH_VALUE
	DEFINE p_title STRING
	DEFINE p_description STRING

	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_record" ATTRIBUTE(BORDER,style="center")
	
	DISPLAY p_title TO lb_title
	DISPLAY p_description TO lb_description
	DISPLAY p_hkey,p_rec_hvalue.* TO sc_hashPair.*

	MENU
		BEFORE MENU
			 CALL dialog.setActionHidden("ACCEPT",TRUE)	
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
	 
	CLOSE WINDOW w_hashPair
END FUNCTION
##################################################################################
# END FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
##################################################################################

##################################################################################
# FUNCTION edit_json_string(p_json_string)
#
#
##################################################################################
FUNCTION edit_json_string(p_json_string)
	DEFINE p_json_string STRING
	
	IF p_json_string IS NULL THEN
		LET p_json_string = 
			'{
				"hm1_key_1":{
					"fname":"hm1_value_fname_1",
					"lname":"hm1_value_lName_1",
					"dob":"11.05.1981",
					"country":"hm1_value_country_1"},
				"hm1_key_2":{
					"fname":"hm1_value_fname_2",
					"lname":"hm1_value_lName_2",
					"dob":"08.10.1981",
					"country":"hm1_value_country_2"},
				"hm1_key_3":{
					"fname":"hm1_value_fname_3",
					"lname":"hm1_value_lName_3",
					"dob":"07.03.1982",
					"country":"hm1_value_country_3"},
				"hm1_key_4":{
					"fname":"hm1_value_fname_4",
					"lname":"hm1_value_lName_4",
					"dob":"04.08.1982",
					"country":"hm1_value_country_4"},
				"hm1_key_5":{
					"fname":"hm1_value_fname_5",
					"lname":"hm1_value_lName_5",
					"dob":"01.01.1983",
					"country":"hm1_value_country_5"},
				"hm1_key_6":{
					"fname":"hm1_value_fname_6",
					"lname":"hm1_value_lName_6",
					"dob":"31.05.1983",
					"country":"hm1_value_country_6"},
				"hm1_key_7":{
					"fname":"hm1_value_fname_7",
					"lname":"hm1_value_lName_7",
					"dob":"28.10.1983",
					"country":"hm1_value_country_7"},
				"hm1_key_8":{
					"fname":"hm1_value_fname_8",
					"lname":"hm1_value_lName_8",
					"dob":"26.03.1984",
					"country":"hm1_value_country_8"},
				"hm1_key_9":{
					"fname":"hm1_value_fname_9",
					"lname":"hm1_value_lName_9",
					"dob":"23.08.1984",
					"country":"hm1_value_country_9"
				},
				"hm1_key_10":{
					"fname":"hm1_value_fname_10",
					"lname":"hm1_value_lName_10",
					"dob":"20.01.1985",
					"country":"hm1_value_country_10"
				}
			}
			'
	END IF
	
	OPEN WINDOW cm_hash_console WITH FORM "form/cm_hash_console" ATTRIBUTE(BORDER,style="centered")
	
	INPUT p_json_string WITHOUT DEFAULTS FROM console ATTRIBUTE(UNBUFFERED)
	END INPUT
	
	CLOSE WINDOW cm_hash_console
	RETURN p_json_string
END FUNCTION
##################################################################################
# END FUNCTION edit_json_string(p_json_string)
##################################################################################