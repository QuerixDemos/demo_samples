
DEFINE dt_rec_jsonObj TYPE AS RECORD
	fName STRING,
	lName STRING,
	dob DATE,
	country STRING,
	transaction DYNAMIC ARRAY OF MONEY
END RECORD


MAIN
	DEFINE json_obj util.JSONObject
	#DEFINE json_obj_tmp util.JSONObject
	DEFINE l_rec_data OF dt_rec_jsonObj
	#DEFINE l_rec_data REFERENCE
	DEFINE i SMALLINT
	DEFINE js STRING

	DEFER INTERRUPT
	 
	OPEN WINDOW cm_jsonObject WITH FORM "form/cm_jsonObject"
	
	IF json_obj IS NOT NULL THEN
		DISPLAY "Failed to DEFINE the object" 
	END IF

	LET json_obj = util.JSONObject.CREATE()

	IF json_obj IS NULL THEN DISPLAY "Failed to CREATE the object" ELSE 
  	DISPLAY "PASSED"
	END IF

	LET l_rec_data = l_rec_data
	
	DISPLAY "-------------------------------------"
	CALL init_record(l_rec_data) 
	DISPLAY "After INIT()"
	DISPLAY l_rec_data
	DISPLAY "-------------------------------------"

#????!!!????	LET l_rec_data = l_rec_data


	LET json_obj = util.JSONObject.fromFGL(l_rec_data)
	DISPLAY "AFTER LET json_obj_tmp = util.JSONObject.fromFGL(l_rec_data)"
	DISPLAY json_obj.toString()
	DISPLAY "-------------------------------------"

	DISPLAY "json_obj.getLength() = ", json_obj.getLength()
	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"
	DISPLAY "json_obj.getType"
	DISPLAY "fName=", json_obj.getType("fName") 	
	DISPLAY "lName=", json_obj.getType("lName") 
	DISPLAY "dob=", json_obj.getType("dob")
	DISPLAY "country=", json_obj.getType("country")
	DISPLAY "transaction=", json_obj.getType("transaction")

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"
	DISPLAY "json_obj.get()"
	DISPLAY "fName=", json_obj.get("fName") 	
	DISPLAY "lName=", json_obj.get("lName") 
	DISPLAY "dob=", json_obj.get("dob")
	DISPLAY "country=", json_obj.get("country")
	DISPLAY "transaction=", json_obj.get("transaction")
	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"

	DISPLAY "json_obj.put(\"id\", 19680206)"
	CALL json_obj.put("id", 19680206)

	DISPLAY "json_obj.get()"
	DISPLAY "id=", json_obj.get("id")
	DISPLAY "id type=", json_obj.getType("id")
	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"

	DISPLAY "json_obj.has(\"id\")"
	DISPLAY json_obj.has("id")

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"
	DISPLAY "FOR i = 1 TO json_obj.getLength()"
	DISPLAY "DISPLAY json_obj.name(i)"
	FOR i = 1 TO json_obj.getLength()
 		DISPLAY json_obj.name(i)
 	END FOR

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"			

	LET js='{ "cust_num":2735, "cust_name":"McCarlson",
          "orderids":[234,3456,24656,34561],
          "js_obj" : { "js_cust_num":1111, "js_cust_name":"js_McCarlson",
          "js_orderids":[123,2345,34567,45678] } }'
	LET json_obj = util.JSONObject.parse(js)
	DISPLAY "json_obj="
	DISPLAY json_obj
	
	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"		

	DISPLAY "json_obj.put(\"fruit\", \"Orange\")"
	CALL json_obj.put("fruit", "Orange")

	DISPLAY "DISPLAY json_obj.get(\"fruit\")"
	DISPLAY json_obj.get("fruit")
	
	DISPLAY "CALL json_obj.remove(\"fruit\")"
	CALL json_obj.remove("fruit")

	DISPLAY "DISPLAY json_obj.has(\"fruit\")"
	DISPLAY json_obj.has("fruit")



	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"			
	DISPLAY "LET js =  json_obj.toString()"
	LET js =  json_obj.toString()
	DISPLAY "JSON String 0 "
	DISPLAY js 

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"			

	DISPLAY "LET js = util.JSON.stringify( l_rec_data )"
	LET js = util.JSON.stringify( l_rec_data )
	DISPLAY util.JSON.format( js )

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------" 			


	LET js='{ 
						"fName":"Alexander", 
						"lName":"Williams",
						"dob":"7/7/1917", 
						"country":"Spain",  
						"transaction":[234,3456,24656,34561] 
					}'
	CALL util.JSON.parse(js, l_rec_data)
	DISPLAY l_rec_data

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"			

	DISPLAY util.JSON.proposeType(js)

	CALL display_json_object_value_list(json_obj)
	DISPLAY "-------------------------------------"
	DISPLAY "-------------------------------------"
	
	MENU
		on action "cancel"
		exit menu
	end menu


END MAIN


FUNCTION init_record(p_ref_rec)
	DEFINE p_ref_rec REFERENCE
	DEFINE l_rec_data OF dt_rec_jsonObj
	DEFINE i SMALLINT 
	LET l_rec_data = p_ref_rec
	
	LET l_rec_data.fName = "Hubert"
	LET l_rec_data.lName = "Hölzl"
	LET l_rec_data.dob = "10/12/1986"
	LET l_rec_data.country = "Germany"
	
	LET l_rec_data.transaction[1]= 110
	FOR i = 2 TO 20
		LET l_rec_data.transaction[i]= l_rec_data.transaction[i-1] + 110
	END FOR
	
	LET p_ref_rec = l_rec_data
END FUNCTION


FUNCTION display_json_object_value_list(p_json_obj)
	DEFINE p_json_obj util.JSONObject
	DEFINE l_arr_rec_pair DYNAMIC ARRAY OF RECORD 
		jKey STRING, 
		jValue STRING 
	END RECORD
	DEFINE i SMALLINT
	
	FOR i = 1 TO p_json_obj.getLength()
		LET l_arr_rec_pair[i].jKey = p_json_obj.name(i)
		#LET l_arr_rec_pair[i].jValue = p_json_obj.get(p_json_obj.name(i))
	END FOR
	
	DISPLAY ARRAY l_arr_rec_pair TO sc_json.* #WITHOUT SCROLL
	END DISPLAY
	
	IF int_flag THEN
		LET int_flag = FALSE
	END IF
END FUNCTION

