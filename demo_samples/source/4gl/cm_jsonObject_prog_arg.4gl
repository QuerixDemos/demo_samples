#####################################################################################
# MAIN
#
# Program is launching a child program and passes an jsonObject String
#####################################################################################

MAIN
  DEFINE l_json util.JSONObject
  DEFINE l_child_prog_name STRING
  DEFINE l_child_prog_argument STRING
  DEFINE l_run STRING
  DEFINE l_msg STRING
  DEFINE l_stb base.StringBuffer
  
  DISPLAY "---------------------------------------------"
  DISPLAY "cm_jsonObject_prog_arg"
  DISPLAY "---------------------------------------------"
  
  LET l_json = util.JSONObject.create()
  CALL l_json.insert("id", "HHOELZL")
  CALL l_json.insert("fname", "Hubert")
  CALL l_json.insert("lname", "Hölzl")
  CALL l_json.insert("dob", "10.12.1986")
  CALL l_json.insert("country", "Germany")
  CALL l_json.insert("partner", TRUE)
  CALL l_json.insert("loyalty", 5)
  CALL l_json.insert("mynullint", NULL)	
		
  LET l_stb = base.StringBuffer.create()
  
  CALL l_stb.append(l_json)
  CALL l_stb.replace("\"", "\\\"", 0)
  
  LET l_child_prog_name = "cm_jsonObject_prog_arg_read"
  LET l_child_prog_argument = l_stb.ToString()

	IF fgl_arch() = "nt" THEN   
		LET l_child_prog_name = trim(l_child_prog_name),".exe"
	END IF
	
  IF os.Path.executable(l_child_prog_name) THEN
	  LET l_run = trim(l_child_prog_name), " \"", trim(l_child_prog_argument), "\""
	  DISPLAY l_run
	  RUN l_run
	ELSE
		LET l_msg = "Attempt to run ", trim(l_child_prog_name), " failed!" 
		CALL fgl_winmessage(l_msg,"Child/Run Program is not deployed or has no executable permission","error") 
	END IF
	
END MAIN
#####################################################################################
# END MAIN
#####################################################################################