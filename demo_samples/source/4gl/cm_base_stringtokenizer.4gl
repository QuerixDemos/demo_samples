#####################################################################################
# MAIN
#
#
#####################################################################################
MAIN
	DEFINE l_source_string STRING
	DEFINE l_arr_token DYNAMIC ARRAY OF STRING
	DEFINE l_arr_rec_pair DYNAMIC ARRAY OF RECORD
		left_prefix_str STRING,
		right_suffix_str STRING
	END RECORD
	DEFINE l_var_rec RECORD
		var1,var2,var3 STRING #Just 3 sample variables for this demo
	END RECORD

	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	DEFER INTERRUPT
	
	CALL ui_init()
	CALL initData() RETURNING l_source_string		#init master array with data  (also creates an array duplicate/casting just for demo purpose)
	
	OPEN WINDOW cm_base_tokenizer WITH FORM "form/cm_base_stringtokenizer"
	CALL ui.Interface.setText("base.StringTokenizer")
	
	DIALOG ATTRIBUTE(UNBUFFERED)	
		INPUT l_source_string WITHOUT DEFAULTS FROM source_string 
			ON ACTION "ACCEPT"
				CALL string_tokenizer(l_source_string) RETURNING l_arr_token
				CALL regex_pattern_tokenizer(l_arr_token) RETURNING l_arr_rec_pair
				CALL assign_values_to_variables(l_arr_rec_pair) RETURNING l_var_rec.*
				CONTINUE DIALOG #CONTINUE INPUT
		END INPUT

		DISPLAY ARRAY l_arr_token TO scr_arr_token.* #token
		END DISPLAY

		DISPLAY ARRAY l_arr_rec_pair TO scr_arr_rec_pair.*
		END DISPLAY
		
		INPUT BY NAME l_var_rec.* WITHOUT DEFAULTS
		END INPUT
		
		ON ACTION "CANCEL"
			EXIT DIALOG
	END DIALOG
		
	CLOSE WINDOW cm_base_tokenizer 
END MAIN
#####################################################################################
# END MAIN
#####################################################################################


#####################################################################################
# FUNCTION string_tokenizer(p_arg_str)
#
#
#####################################################################################
FUNCTION string_tokenizer(p_arg_str)
	DEFINE p_arg_str STRING
	DEFINE tok base.StringTokenizer
	DEFINE l_arr_token DYNAMIC ARRAY OF STRING
	DEFINE l_idx SMALLINT
		
	LET tok = base.StringTokenizer.Create(p_arg_str, ";")

	LET l_idx = 0
  WHILE tok.hasMoreTokens()
  	LET l_idx = l_idx + 1
		LET l_arr_token[l_idx] = tok.nextToken()
  END WHILE
  
	RETURN l_arr_token		
END FUNCTION
#####################################################################################
# END FUNCTION string_tokenizer(p_arg_str)
#####################################################################################


#####################################################################################
# FUNCTION regex_pattern_tokenizer(p_arr_token)
#
#
#####################################################################################
FUNCTION regex_pattern_tokenizer(p_arr_token)
	DEFINE p_arr_token DYNAMIC ARRAY OF STRING
	DEFINE l_arr_rec_pair DYNAMIC ARRAY OF RECORD
		left_prefix_str STRING,
		right_suffix_str STRING
	END RECORD
	DEFINE l_idx SMALLINT	
	DEFINE l_regex util.REGEX #util.REGEX - to hold the regular expression:
	DEFINE l_match_results util.MATCH_RESULTS

	CALL l_regex.setPattern("=") #you can also use this: LET l_regex = /=/
	CALL l_regex.setFlags(NULL)		#You can also provide regex  modifier flags
	
	FOR l_idx = 1 TO p_arr_token.getSize()
		CALL util.REGEX.search(p_arr_token[l_idx], l_regex) RETURNING l_match_results
			
		IF l_match_results.matched(0) THEN
			LET l_arr_rec_pair[l_idx].left_prefix_str =	l_match_results.prefix()
			LET l_arr_rec_pair[l_idx].right_suffix_str =	l_match_results.suffix()
		END IF
	END FOR
	
	RETURN l_arr_rec_pair
END FUNCTION
#####################################################################################
# END FUNCTION regex_pattern_tokenizer(p_arr_token)
#####################################################################################


#####################################################################################
# FUNCTION assign_values_to_variables(p_arr_rec_token)
#
#
#####################################################################################
FUNCTION assign_values_to_variables(p_arr_rec_token)
	DEFINE p_arr_rec_token,l_arr_rec_token DYNAMIC ARRAY OF RECORD
		var STRING,
		val STRING
	END RECORD

	DEFINE l_var_rec RECORD
		var1,var2,var3 STRING #Just 3 sample module scope variables for this demo
	END RECORD

	LET l_arr_rec_token = p_arr_rec_token
		
	WHILE l_arr_rec_token.getSize()
		CASE l_arr_rec_token[1].var.toLowerCase()
			WHEN "var1"
				LET l_var_rec.var1 = l_arr_rec_token[1].val
			
			WHEN "var2"
				LET l_var_rec.var2 = l_arr_rec_token[1].val
			
			WHEN "var3"
				LET l_var_rec.var3 = l_arr_rec_token[1].val
		END CASE				
		CALL l_arr_rec_token.delete(1)
	END WHILE

	RETURN l_var_rec.*
END FUNCTION
#####################################################################################
# END FUNCTION assign_values_to_variables(p_arr_rec_token)
#####################################################################################


#####################################################################################
# FUNCTION initData()
#
# RETURN l_ret_source_string STRING
#####################################################################################
FUNCTION initData()
	DEFINE l_ret_source_string STRING		#init master array with data  (also creates an array duplicate/casting just for demo purpose)
		
	LET l_ret_source_string = "var1=111;Var2=222;VAR3=333"
	
	RETURN l_ret_source_string		#init master array with data  (also creates an array duplicate/casting just for demo purpose)
END FUNCTION
#####################################################################################
# END FUNCTION initData()
#####################################################################################