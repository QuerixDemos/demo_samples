#####################################################################################
# MAIN
#
# Program is launched by another program which passes an jsonObject String
#####################################################################################
  MAIN
    DEFINE l_arg STRING
    DEFINE l_json util.JSONObject
    DEFINE l_msg STRING
    
    CALL fgl_winmessage("Client Application launched","Program cm_jsonObject_prog_arg_read","info")
    
    DISPLAY "---------------------------------------------"
    DISPLAY "Program: cm_jsonObject_prog_arg_read"
    DISPLAY "---------------------------------------------"
    
    
    LET l_arg = arg_val(1)
    DISPLAY "Argument=", l_arg
    
    LET l_json = util.JSONObject.create()
    
    LET l_json=util.JSONObject.parse(l_arg) #Parse JSON string
    
    #GET and DISPLAY the different jsonObject members    
    DISPLAY "l_json=", l_json
    DISPLAY "---------------------------------------------"
		DISPLAY "id=", l_json.get("id")
		DISPLAY "fname=", l_json.get("fname")
		DISPLAY "lname=", l_json.get("lname")
		DISPLAY "dob=", l_json.get("dob")
		DISPLAY "country=", l_json.get("country")
		DISPLAY "partner=", l_json.get("partner")
		DISPLAY "loyalty=", l_json.get("loyalty")
		DISPLAY "mynullint=", l_json.get("mynullint")
    DISPLAY "---------------------------------------------"
  
    LET l_msg = "Program cm_jsonObject_prog_arg_read\n", trim(l_json)
    CALL fgl_winmessage("Client Application is exiting",l_msg,"info")
END MAIN
#####################################################################################
# END MAIN
#####################################################################################