##########################################################################
# New_features_in_Querix_4gl Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##########################################################################
# MAIN
#
#
##########################################################################
MAIN
	DEFINE l_rec RECORD
		f1,f2,f3 STRING
	END RECORD

	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	DEFER INTERRUPT
	
	CALL ui_init()
	
	OPEN WINDOW act_hidden_and_active WITH FORM "form/act_hidden_and_active" #attribute(BORDER, style="center") 
	CALL ui.Interface.setText("Action Hide/Active")
	
	INPUT BY NAME l_rec.* WITHOUT DEFAULTS
		BEFORE INPUT
			CALL dialog.setActionHidden("ACCEPT",TRUE)
			
	 	ON ACTION "EDIT_ActActive_On"
	 		CALL dialog.setActionActive("EDIT",TRUE)
	
	 	ON ACTION "EDIT_ActActive_Off"
	 		CALL dialog.setActionActive("EDIT",FALSE)

	 	ON ACTION "EDIT_ActHidden_On"
	 		CALL dialog.setActionHidden("EDIT",TRUE)
	
	 	ON ACTION "EDIT_ActHidden_Off"
	 		CALL dialog.setActionHidden("EDIT",FALSE)
	 			
		ON ACTION "EDIT"
			MESSAGE "EDIT"

			
	 	ON ACTION "DELETE_ActActive_On"
	 		CALL dialog.setActionActive("DELETE",TRUE)
	
	 	ON ACTION "DELETE_ActActive_Off"
	 		CALL dialog.setActionActive("DELETE",FALSE)
	 			
	 	ON ACTION "DELETE_ActHidden_On"
	 		CALL dialog.setActionHidden("DELETE",TRUE)
	
	 	ON ACTION "DELETE_ActHidden_Off"
	 		CALL dialog.setActionHidden("DELETE",FALSE)
	
		ON ACTION "DELETE"
			MESSAGE "DELETE"
	
	 	ON ACTION "INSERT_ActActive_On"
	 		CALL dialog.setActionActive("INSERT",TRUE)
	
	 	ON ACTION "INSERT_ActActive_Off"
	 		CALL dialog.setActionActive("INSERT",FALSE)
	 			
	 	ON ACTION "INSERT_ActHidden_On"
	 		CALL dialog.setActionHidden("INSERT",TRUE)
	
	 	ON ACTION "INSERT_ActHidden_Off"
	 		CALL dialog.setActionHidden("INSERT",FALSE)
	 			
		ON ACTION "INSERT"
			MESSAGE "INSERT"

	
	 	ON ACTION "SEARCH_ActActive_On"
	 		CALL dialog.setActionActive("SEARCH",TRUE)
	
	 	ON ACTION "SEARCH_ActActive_Off"
	 		CALL dialog.setActionActive("SEARCH",FALSE)
	 			
	 	ON ACTION "SEARCH_ActHidden_On"
	 		CALL dialog.setActionHidden("SEARCH",TRUE)
	
	 	ON ACTION "SEARCH_ActHidden_Off"
	 		CALL dialog.setActionHidden("SEARCH",FALSE)
	
		ON ACTION "SEARCH"
			MESSAGE "SEARCH"

		ON ACTION "HELP"
			CALL onlineHelp("dialog.action",NULL)
			
	END INPUT 

END MAIN 
##########################################################################
# END MAIN
##########################################################################

