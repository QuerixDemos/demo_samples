MAIN
  DEFINE context ZMQ.Context
  DEFINE responder ZMQ.Socket
  DEFINE request, reply STRING

# Socket to talk to clients
	LET responder = context.Socket("ZMQ.REP")
	CALL responder.bind("tcp://*:5556")

	WHILE TRUE
	  # Wait for next request from the client
	  LET request = responder.recv()
	  DISPLAY "Received Hello"  
	  # Do some 'work'
	  SLEEP 1
	   
	  # Send reply back to client
	  LET reply = "World reply!"
	  CALL responder.send(reply)
  END WHILE
  
	CALL responder.close()
	CALL context.term()
END MAIN