MAIN
DEFINE context ZMQ.Context
DEFINE requester ZMQ.Socket
DEFINE request, reply STRING
DEFINE requestNbr INT

# Socket to talk to server
DISPLAY "Connecting to hello world server..."

LET requester = context.Socket("ZMQ.REQ")
CALL requester.connect("tcp://localhost:5556")

FOR requestNbr = 0 TO 10
  LET request = "Friendly Hello, from client"
  DISPLAY "Sending Hello: ", requestNbr
  CALL requester.send(request)
  LET reply = requester.recv()
  DISPLAY "Received: ", reply CLIPPED, " ", requestNbr
END FOR

CALL requester.close()
CALL context.term()
END MAIN