
DEFINE modu_log STRING

MAIN
	DEFINE l_base_stringBuffer base.StringBuffer
	DEFINE l_position INTEGER
	DEFINE l_searchString STRING
	DEFINE l_line_no SMALLINT #display print line
	DEFINE l_index SMALLINT
	DEFINE l_index_end SMALLINT
	DEFINE l_length SMALLINT
	DEFINE l_char CHAR
	DEFINE l_string STRING		
	DEFINE l_msg STRING

	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	DEFER INTERRUPT
	
	CALL ui_init()
		
	LET l_line_no = 5

	OPEN WINDOW cm_base_stringbuffer WITH FORM "form/cm_base_stringbuffer"
	CALL ui.Interface.setText("base.StringBuffer")
	   
	LET l_base_stringBuffer = base.StringBuffer.create()
	CALL l_base_stringBuffer.append("123Hubert likes to play with 32Hubert's demo test cases to show, that 73Hubert understands their usage and 111Hubertus")
	
	DISPLAY l_base_stringBuffer.toString() TO stringBuffer
	
	LET l_position = 1
	LET l_searchString = "Hubert"

	MENU

			
		ON ACTION "sbAppend"
			CALL l_base_stringBuffer.append(fgl_winprompt(5, 5, "", "Your input", 25, 0))
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sb_Clear"
			CALL l_base_stringBuffer.clear()
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbEquals"
			IF l_base_stringBuffer.equals(fgl_winprompt(5, 5, "", "Your input", 25, 0)) THEN
				CALL fgl_winmessage("stringBuffer.equals()","StringBuffer equals your entered string","info")
			ELSE
				CALL fgl_winmessage("stringBuffer.equals()","StringBuffer DOES NOT equal your entered string","info")
			END IF

		ON ACTION "sbEqualsIgnoreCase"
			IF l_base_stringBuffer.equalsIgnoreCase(fgl_winprompt(5, 5, "", "Your input", 25, 0)) THEN
				CALL fgl_winmessage("stringBuffer.equalsIgnoreCase()","StringBuffer equals your entered string","info")
			ELSE
				CALL fgl_winmessage("stringBuffer.equalsIgnoreCase()","StringBuffer DOES NOT equal your entered string","info")
			END IF

		ON ACTION "sbGetCharAt"
			LET l_position = fgl_winprompt(5, 5, "Enter Char Index (int)", "1", 25, 1)
			IF l_position > 0 THEN
				LET l_char = l_base_stringBuffer.getCharAt(l_position)
				 
				LET l_msg = "The character at position ", trim(l_position), " = ", l_char
				CALL fgl_winmessage("stringBuffer.getCharAt()",l_msg,"info")
			END IF

		ON ACTION "sbGetIndexOf"

	   LET l_msg = "Searching for: ", l_searchString
	   CALL append_modu_log(l_msg)
   
		LET l_searchString = fgl_winprompt(5, 5, "Enter the string you want to search for", "Hubert", 25, 0)
		LET l_position = fgl_winprompt(5, 5, "Enter the index position where the search should start from (int)", "1", 4, 1)   	
   	
	   WHILE TRUE
	      LET l_position = l_base_stringBuffer.getIndexOf(l_searchString,l_position)
	      
	      IF l_position == 0 THEN
	         EXIT WHILE
	      END IF
	      
	      LET l_msg = "Position: ", trim(l_position) 
	      CALL append_modu_log(l_msg)
	      LET l_position = l_position + l_searchString.getLength()
	   END WHILE


		ON ACTION "sbGetLength"
			LET l_length = l_base_stringBuffer.getLength()

			LET l_msg = "The length of the string is: ", trim(l_length)
			CALL fgl_winmessage("Info",l_msg,"info")

		ON ACTION "sbInsertAt"
			LET l_string = fgl_winprompt(5, 5, "Enter the string you want to insert", "My String", 25, 0)
			LET l_position = fgl_winprompt(5, 5, "Enter the location of the insert operation", "1", 25, 1)
			
			CALL l_base_stringBuffer.insertAt(l_position,l_string)
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbReplace"
			LET l_searchString = fgl_winprompt(5, 5, "Search String (to be replaced)", "Hubert", 25, 0)
			LET l_string = fgl_winprompt(5, 5, "Replacement String", "My String", 25, 0)
			
			CALL l_base_stringBuffer.replace(l_searchString,l_string,0)
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbReplaceAt"
			LET l_index = fgl_winprompt(5, 5, "Enter the location of the replace operation", "1", 25, 1)
			LET l_length = fgl_winprompt(5, 5, "Enter the length of the to replace string", "5", 25, 1)
			LET l_string = fgl_winprompt(5, 5, "Replacement String", "My String", 25, 0)
			
			CALL l_base_stringBuffer.replaceAt(l_index,l_length,l_string)
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbSubString"
			LET l_index = fgl_winprompt(5, 5, "Enter the Start Index of the SubString", "1", 25, 1)
			LET l_index_end = fgl_winprompt(5, 5, "Enter the End Index of the SubString", "5", 25, 1)
			
			LET l_string = l_base_stringBuffer.subString(l_index,l_index_end)
			
			LET l_msg = "SubString located in [", trim(l_index), ",", trim(l_index_end), "] = ", trim(l_string) 
			CALL fgl_winmessage("SubString value", l_msg, "info")

		ON ACTION "sbToLowerCase"
			CALL l_base_stringBuffer.ToLowerCase()
			LET l_string = l_base_stringBuffer.toString()
			
			LET l_msg = "SringBuffer in lower case:\n", trim(l_string) 
			CALL fgl_winmessage("StringBuffer .toLowerCase()\n", l_msg, "info")
			
			DISPLAY l_base_stringBuffer.toString() TO stringBuffer


		ON ACTION "sbToString"
			
			LET l_string = l_base_stringBuffer.toString()
			
			LET l_msg = "SringBuffer:\n", trim(l_string) 
			CALL fgl_winmessage("StringBuffer.toString()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbToUpperCase"
			CALL l_base_stringBuffer.ToUpperCase()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer in upper case:\n", trim(l_string) 
			CALL fgl_winmessage("StringBuffer .ToUpperCase()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbTrim"
			CALL l_base_stringBuffer.trim()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trim():\n", trim(l_string) 
			CALL fgl_winmessage("StringBuffer.trim()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer
{						
		ON ACTION "sbTrimWhiteSpace"
			CALL l_base_stringBuffer.trimWhiteSpace()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trimWhiteSpace():\n", trimWhiteSpace(l_string) 
			CALL fgl_winmessage("StringBuffer.trimWhiteSpace()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer
						
		ON ACTION "sbTrimLeft"
			CALL l_base_stringBuffer.trimLeft()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trimLeft():\n", trimLeft(l_string) 
			CALL fgl_winmessage("StringBuffer.trimLeft()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbTrimRight"
			CALL l_base_stringBuffer.trimRight()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trimRight():\n", trimRight(l_string) 
			CALL fgl_winmessage("StringBuffer.trimRight()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer


		ON ACTION "sbTrimLeftWhiteSpace"
			CALL l_base_stringBuffer.trimLeftWhiteSpace()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trimLeftWhiteSpace():\n", trimLeftWhiteSpace(l_string) 
			CALL fgl_winmessage("StringBuffer.trimLeftWhiteSpace()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer

		ON ACTION "sbTrimRightWhiteSpace"
			CALL l_base_stringBuffer.trimRightWhiteSpace()
			LET l_string = l_base_stringBuffer.toString()

			LET l_msg = "SringBuffer.trimRightWhiteSpace():\n", trimRightWhiteSpace(l_string) 
			CALL fgl_winmessage("StringBuffer.trimRightWhiteSpace()\n", l_msg, "info")

			DISPLAY l_base_stringBuffer.toString() TO stringBuffer
}			
		ON ACTION "HELP"
			CALL onlineHelp("StringBuffer",NULL)
		    	
		ON ACTION CANCEL
			EXIT MENU
	END MENU   

	CLOSE WINDOW cm_base_stringbuffer
END MAIN
 



FUNCTION display_modu_log()
	DISPLAY modu_log TO log_area
END FUNCTION

FUNCTION append_modu_log(p_string)
	DEFINE p_string STRING
	LET modu_log = modu_log, "\n", p_string
	CALL display_modu_log()
END FUNCTION