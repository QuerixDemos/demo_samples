DEFINE dt_rec_HASH_VALUE TYPE AS RECORD
	fName STRING,
	lName STRING,
	dob DATE,
	country STRING,
	transaction DYNAMIC ARRAY OF MONEY
END RECORD

DEFINE dt_rec_HASH_VALUE_SUB TYPE AS RECORD
	fName STRING,
	lName STRING,
	dob DATE,
	country STRING
END RECORD
DEFINE dt_arr_HASH_VALUE_SUB TYPE AS DYNAMIC ARRAY OF MONEY

DEFINE dt_rec_HASH_ELEMENT TYPE AS RECORD
	hKey REFERENCE,
	hValue dt_rec_HASH_VALUE
END RECORD

    
##################################################################################
# MAIN
#
#
##################################################################################     
MAIN
  DEFINE l_ht_tmp HASHMAP OF dt_rec_HASH_VALUE	#hashMap for temp operations

  DEFINE l_hm1 HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 1
  DEFINE l_hm1_value_rec_row OF dt_rec_HASH_VALUE
  DEFINE l_arr_hm1_value_array_SUB OF dt_arr_HASH_VALUE_SUB
  DEFINE l_rec_hm1_value_rec_SUB OF dt_rec_HASH_VALUE_SUB
  
	DEFINE l_hm1_row_hkey VARIANT

  DEFINE l_ht2 HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 2
  DEFINE l_ht2_rec_row OF dt_rec_HASH_VALUE
	DEFINE l_ht2_row_hkey VARIANT

  DEFINE l_dct1 DICTIONARY OF dt_rec_HASH_VALUE	#dictionary example 1
  DEFINE l_dct1_rec_row OF dt_rec_HASH_VALUE
	DEFINE l_dct1_row_hkey VARIANT

  DEFINE l_dct2 DICTIONARY OF dt_rec_HASH_VALUE	#dictionary example 2
  DEFINE l_dct2_rec_row OF dt_rec_HASH_VALUE
	DEFINE l_dct2_row_hkey VARIANT
  
  DEFINE i INT

  DEFINE l_hm1_idx SMALLINT			#array index for l_hm1
  DEFINE l_ht2_idx SMALLINT			#array index for l_ht2
  DEFINE l_dct1_idx SMALLINT		#array index for l_dct1
  DEFINE l_dct2_idx SMALLINT		#array index for l_dct2
  DEFINE l_idx1 SMALLINT
  DEFINE l_idx2 SMALLINT
	DEFINE l_msg STRING
	DEFINE l_search STRING	#??variant ? key ?
	DEFINE l_arr_hkey DYNAMIC ARRAY OF VARIANT #used to demonstrate the method .getKeys() for hash keys
	DEFINE l_target_hash_id STRING
	
  
  DEFINE l_jsonObj util.JSONObject
	DEFINE l_json_string STRING
	DEFINE l_choice CHAR(1)

	DEFER INTERRUPT
	OPTIONS INPUT WRAP
	OPTIONS ON CLOSE APPLICATION CALL handle_dirty_exit
	
	CALL ui_init()
	
	OPEN WINDOW w_cm_hash_mixed WITH FORM "form/cm_hash_mixed"
	CALL ui.Interface.setText("HashMap")
  	  
  CALL init(l_hm1, l_ht2,l_dct1, l_dct2) RETURNING 
  	l_hm1_row_hkey,
  	l_ht2_row_hkey,
  	l_dct1_row_hkey,
  	l_dct2_row_hkey

  CALL update_display(
  	l_hm1_row_hkey,l_hm1,
  	l_ht2_row_hkey,l_ht2,
  	l_dct1_row_hkey,l_dct1,
  	l_dct2_row_hkey,l_dct2)

{
	  INPUT ARRAY l_hm1 WITHOUT DEFAULTS FROM sc_hm1_inp.* 
	  	BEFORE ROW
	  		LET l_hm1_idx = DIALOG.getCurrentRow("sc_hm1_inp")	
	  			
	  		IF l_hm1_idx > 0 THEN
	  			LET l_hm1_row_hkey = l_hm1.GetKey(l_hm1_idx)
					#Nested Record
					LET l_hm1_value_rec_row.fname =   l_hm1[l_hm1_row_hkey].fname
					LET l_hm1_value_rec_row.lname =   l_hm1[l_hm1_row_hkey].lname
					LET l_hm1_value_rec_row.dob =     l_hm1[l_hm1_row_hkey].dob
					LET l_hm1_value_rec_row.country = l_hm1[l_hm1_row_hkey].country
					#4GL record (sub-part of hashMap)
					LET l_rec_hm1_value_rec_SUB.fname =   l_hm1[l_hm1_row_hkey].fname
					LET l_rec_hm1_value_rec_SUB.lname =   l_hm1[l_hm1_row_hkey].lname
					LET l_rec_hm1_value_rec_SUB.dob =     l_hm1[l_hm1_row_hkey].dob
					LET l_rec_hm1_value_rec_SUB.country = l_hm1[l_hm1_row_hkey].country
					
					FOR i = 1 TO l_hm1[l_hm1_row_hkey].transaction.getSize()
						LET l_arr_hm1_value_array_SUB[i] = l_hm1[l_hm1_row_hkey].transaction[i]
					END FOR

				END IF

				DISPLAY l_hm1_row_hkey, l_rec_hm1_value_rec_SUB.* TO sc_hm1_rec_row.*
				DISPLAY ARRAY l_arr_hm1_value_array_SUB TO sc_hm1_val_arr.* WITHOUT SCROLL 
	  		DISPLAY l_hm1_idx TO hm1_bucketArrIndex

	  END INPUT
}	  
{
		#works
		#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
		INPUT l_hm1_row_hkey,l_rec_hm1_value_rec_SUB.* WITHOUT DEFAULTS FROM sc_hm1_rec_row.*
			ON ACTION "act_save_hm1_row_rec"
				LET l_hm1_value_rec_row.fname = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.lname = l_rec_hm1_value_rec_SUB.lname
				LET l_hm1_value_rec_row.dob = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.country = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.transaction = l_arr_hm1_value_array_SUB
				
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT
}
{
		#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
		INPUT l_hm1_row_hkey,l_hm1[l_hm1_row_hkey].fname, l_hm1[l_hm1_row_hkey].lname,l_hm1[l_hm1_row_hkey].dob,l_hm1[l_hm1_row_hkey].country WITHOUT DEFAULTS FROM sc_hm1_rec_row.*
		END INPUT
}
{
		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY l_arr_hm1_value_array_SUB WITHOUT DEFAULTS FROM sc_hm1_val_arr.*
			ON ACTION "act_save_hm1_row_rec"
				LET l_hm1_value_rec_row.fname = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.lname = l_rec_hm1_value_rec_SUB.lname
				LET l_hm1_value_rec_row.dob = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.country = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.transaction = l_arr_hm1_value_array_SUB
				
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT

		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY l_hm1_value_rec_row.transaction WITHOUT DEFAULTS FROM sc_hm1_val_arr.*
			ON ACTION "act_save_hm1_row_rec"
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT
}
{
		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY l_hm1[l_hm1_row_hkey].transaction WITHOUT DEFAULTS FROM sc_hm1_val_arr.*
			ON ACTION "act_save_hm1_row_rec"
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT
}

  ##############################
	# DIALOG START
	###############################
  DIALOG ATTRIBUTE(UNBUFFERED)
		#------------------------------------------------------------------
  	# l_hm1 HashMap 1 
		#------------------------------------------------------------------

	  INPUT ARRAY l_hm1 WITHOUT DEFAULTS FROM sc_hm1_inp.* 
	  	BEFORE ROW
	  		LET l_hm1_idx = DIALOG.getCurrentRow("sc_hm1_inp")	
	  			
	  		IF l_hm1_idx > 0 THEN
	  			LET l_hm1_row_hkey = l_hm1.GetKey(l_hm1_idx)
					#Nested Record
					LET l_hm1_value_rec_row.fname =   l_hm1[l_hm1_row_hkey].fname
					LET l_hm1_value_rec_row.lname =   l_hm1[l_hm1_row_hkey].lname
					LET l_hm1_value_rec_row.dob =     l_hm1[l_hm1_row_hkey].dob
					LET l_hm1_value_rec_row.country = l_hm1[l_hm1_row_hkey].country
					#4GL record (sub-part of hashMap)
					LET l_rec_hm1_value_rec_SUB.fname =   l_hm1[l_hm1_row_hkey].fname
					LET l_rec_hm1_value_rec_SUB.lname =   l_hm1[l_hm1_row_hkey].lname
					LET l_rec_hm1_value_rec_SUB.dob =     l_hm1[l_hm1_row_hkey].dob
					LET l_rec_hm1_value_rec_SUB.country = l_hm1[l_hm1_row_hkey].country
					
					FOR i = 1 TO l_hm1[l_hm1_row_hkey].transaction.getSize()
						LET l_arr_hm1_value_array_SUB[i] = l_hm1[l_hm1_row_hkey].transaction[i]
					END FOR

				END IF

				DISPLAY l_hm1_row_hkey, l_rec_hm1_value_rec_SUB.* TO sc_hm1_rec_row.*
				DISPLAY ARRAY l_arr_hm1_value_array_SUB TO sc_hm1_val_arr.* WITHOUT SCROLL 
	  		DISPLAY l_hm1_idx TO hm1_bucketArrIndex
	  		
	  		--------------------------

				  
			#dataType Methods	  		
	  	ON ACTION "getSize()" #GetSize() - returns count of elements in hash map
	  		LET l_msg = "The size of l_hm1 -> l_hm1.getSize() = ", trim(l_hm1.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			On ACTION "Delete(INT index)" #Delete(INT index) - Deletes element with index
				IF l_hm1_idx > 0 THEN
	  			DISPLAY l_hm1.delete(l_hm1_idx)
	  			LET l_msg = "l_hm1.delete(",trim(l_hm1_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF
	  		
			ON ACTION "Delete(INT first, INT last)"				# Delete(INT first, INT last) - Deletes elements from first till last
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_hm1.getlength()) OR (l_idx2 > l_hm1.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_hm1.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_hm1.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range


			ON ACTION "Remove(VARIANT key)" #Remove(VARIANT key) - Deletes element with key 'key'				
				LET l_hm1_row_hkey = fgl_winprompt(1,1,"Enter Key to be deleted",l_hm1_row_hkey,30,0)
	  		IF l_hm1_row_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_hm1.remove(l_hm1_row_hkey)
	  		END IF

	    ON ACTION "Clear()" #Clear() - Removes all elements from hash map
				IF fgl_winbutton("Delete All?","Do you want to delete all buckets of this HashMap?","Yes","Yes|No","question") = "Yes" THEN
		      CALL l_hm1.clear()
				END IF				

			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' hash map.
				
				LET l_target_hash_id = get_target("hm1","ht2","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_hm1 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.Join(l_hm1)
						WHEN "ht2"
							CALL l_hm1.Join(l_ht2)
						WHEN "dct1"
							CALL l_hm1.Join(l_dct1)
						WHEN "dct2"
							CALL l_hm1.Join(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF		

			ON ACTION "GetKey(INT index)" #GetKey(INT index) - Returns element's value by index
				LET l_msg ="getKey(", trim(l_hm1_idx),  ")\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 1)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_hm1_idx),  ")", "\nValue=",trim(l_hm1.getValue(l_hm1_idx)),"\nKey = ", trim(l_hm1.GetKey(l_hm1_idx)), "\nArray index=", trim(l_hm1_idx) , "\n(current row in HashMap 1)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			ON ACTION "ValueSort(INT direction) ASC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_hm1.ValueSort(1)
				MESSAGE "l_hm1: Sorted by value in ascending order"

			ON ACTION "ValueSort(INT direction) DESC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_hm1.ValueSort(0)
				MESSAGE "l_hm1: Sorted by value in ascending order"

			# CustomSort(STRING funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_hm1.CustomSort("CompareAsc")

			ON ACTION "CustomSort(STRING funcName) CompareDESC" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_hm1.CustomSort("CompareDESC")

			ON ACTION "ResetSorting()" #ResetSorting() - resets value sorts and back hashmap to the initial sorting.
				CALL l_hm1.resetSorting()

			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the hashmap.
				CALL l_hm1.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			ON ACTION "CopyTo(HashMap)" # CopyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
				
				LET l_target_hash_id = get_target("hm1","ht2","Specify the COPY Target HashMap/Dictionary")
				MESSAGE "COPY ", trim(l_target_hash_id), " to l_hm1"
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_hm1.CopyTo(l_hm1)
						WHEN "ht2"
							CALL l_hm1.CopyTo(l_ht2)
						WHEN "dct1"
							CALL l_hm1.CopyTo(l_dct1)
						WHEN "dct2"
							CALL l_hm1.CopyTo(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF

			ON ACTION "LET"
#				CALL input_HashPairValue(l_hm1_row_hkey, l_hm1_value_rec_row.*) 
				CALL input_HashPairValue(l_hm1_row_hkey, l_hm1[l_hm1_row_hkey].*)				
					RETURNING l_hm1_row_hkey, l_hm1_value_rec_row.*

#FUNCTION input_HashPairValue(p_hkey, p_rec_hvalue)
#	DEFINE p_hkey VARIANT
#	DEFINE p_rec_hvalue OF dt_rec_HASH_VALUE

#LOL
#Function was called with a wrong number of parameters.
#Function input_hashpairvalue 
#was called with 6 parameters 
#but this function has 105 parameters. 
#(error code: -109272)



#This is, how the hashMap is defind as

#  DEFINE l_hm1 HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 1
#
#DEFINE dt_rec_HASH_VALUE TYPE AS RECORD
#	fName STRING,
#	lName STRING,
#	dob DATE,
#	country STRING,
#	transaction DYNAMIC ARRAY OF MONEY
#END RECORD
				
				IF NOT int_flag THEN
					LET l_hm1[l_hm1_row_hkey].* = l_hm1_value_rec_row.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF
  
	    ON ACTION "INSERT"
	    	CALL input_HashPairValue(l_hm1_row_hkey, l_hm1_value_rec_row.*) RETURNING l_hm1_row_hkey, l_hm1_value_rec_row.*
	      CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row)

			ON ACTION "LET-REFERENCE"
				CALL input_HashMapBucket(l_hm1_row_hkey, l_hm1)

			ON ACTION "DISPLAY HashMap TO console"
				#DISPLAY l_hm1 #to console in json format for debugging purpose
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_hm1 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

				#util.JSONObject.create() #Create JSONObject instance -- NULL return is error
				
			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/dictionary
				LET l_hm1_row_hkey = fgl_winprompt(1,1,"Search",l_hm1_row_hkey,30,0)
				IF l_hm1.Contains(l_hm1_row_hkey) THEN				
					CALL display_HashPair(l_hm1_row_hkey,l_hm1,"l_hm1.contains() returned true / (l_hm1 contains key)",".contains(key) method returns true if the key exists")
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_hm1_row_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #Append
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_hm1.Join(l_ht_tmp)
						
						WHEN "O" #Overwrite
							CALL util.JSON.parse(l_json_string, l_hm1)
							
						WHEN "C" #Cancel
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
				
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_hm1) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console
	  END INPUT

		# RECORD INPUT -----------------------------------------------------------
		#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
		INPUT l_hm1_row_hkey,l_hm1[l_hm1_row_hkey].fname, l_hm1[l_hm1_row_hkey].lname,l_hm1[l_hm1_row_hkey].dob,l_hm1[l_hm1_row_hkey].country WITHOUT DEFAULTS FROM sc_hm1_rec_row.*
		END INPUT

		# RECORD.ARRAY INPUT -----------------------------------------------------------
		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY l_hm1[l_hm1_row_hkey].transaction WITHOUT DEFAULTS FROM sc_hm1_val_arr.*
			ON ACTION "act_save_hm1_row_rec"
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT


{
		#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
		INPUT l_hm1_row_hkey,l_rec_hm1_value_rec_SUB.* WITHOUT DEFAULTS FROM sc_hm1_rec_row.*
			ON ACTION "act_save_hm1_row_rec"
				LET l_hm1_value_rec_row.fname = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.lname = l_rec_hm1_value_rec_SUB.lname
				LET l_hm1_value_rec_row.dob = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.country = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.transaction = l_arr_hm1_value_array_SUB
				
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT

		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY l_arr_hm1_value_array_SUB WITHOUT DEFAULTS FROM sc_hm1_val_arr.*
			ON ACTION "act_save_hm1_row_rec"
				LET l_hm1_value_rec_row.fname = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.lname = l_rec_hm1_value_rec_SUB.lname
				LET l_hm1_value_rec_row.dob = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.country = l_rec_hm1_value_rec_SUB.fname
				LET l_hm1_value_rec_row.transaction = l_arr_hm1_value_array_SUB
				
				CALL l_hm1.Insert(l_hm1_row_hkey, l_hm1_value_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT
}




{
		#------------------------------------------------------------------
  	# HM2 HashMap 2 (l_ht2)
		#------------------------------------------------------------------
	  INPUT ARRAY l_ht2 WITHOUT DEFAULTS FROM sc_ht2_inp.* 
	  	BEFORE ROW
	  		LET l_ht2_idx = DIALOG.getCurrentRow("sc_ht2_inp")
				DISPLAY l_ht2_idx TO ht2_bucketArrIndex
	
	  		IF l_ht2_idx > 0 THEN
	  			LET l_ht2_row_hkey = l_ht2.GetKey(l_ht2_idx)
					LET l_ht2_rec_row.fname =   l_ht2[l_ht2_row_hkey].fname
					LET l_ht2_rec_row.lname =   l_ht2[l_ht2_row_hkey].lname
					LET l_ht2_rec_row.dob =     l_ht2[l_ht2_row_hkey].dob
					LET l_ht2_rec_row.country = l_ht2[l_ht2_row_hkey].country
				END IF
				DISPLAY l_ht2_row_hkey, l_ht2_rec_row.* TO sc_ht2_rec_row.* 

			#dataType Methods	  		
	  	ON ACTION "getSize()" #GetSize() - returns count of elements in hash map
	  		LET l_msg = "The size of l_ht2 -> l_ht2.getSize() = ", trim(l_ht2.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			On ACTION "Delete(INT index)" #Delete(INT index) - Deletes element with index
				IF l_ht2_idx > 0 THEN
	  			DISPLAY l_ht2.delete(l_ht2_idx)
	  			LET l_msg = "l_ht2.delete(",trim(l_ht2_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF

			ON ACTION "Delete(INT first, INT last)"				# Delete(INT first, INT last) - Deletes elements from first till last
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_ht2.getlength()) OR (l_idx2 > l_ht2.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_ht2.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_ht2.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range

				
			ON ACTION "Remove(VARIANT key)" #Remove(VARIANT key) - Deletes element with key 'key'				
				LET l_ht2_row_hkey = fgl_winprompt(1,1,"Enter Key to be deleted",l_ht2_row_hkey,30,0)
	  		IF l_ht2_row_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_ht2.remove(l_ht2_row_hkey)
	  		END IF

	    ON ACTION "INSERT"
	    	CALL input_HashPairValue(l_ht2_row_hkey, l_ht2_rec_row.*) RETURNING l_ht2_row_hkey, l_ht2_rec_row.*
	      CALL l_ht2.Insert(l_ht2_row_hkey, l_ht2_rec_row)
	      
	    ON ACTION "Clear()" #Clear() - Removes all elements from hash map
				IF fgl_winbutton("Delete All?","Do you want to delete all buckets of this HashMap?","Yes","Yes|No","question") = "Yes" THEN
		      CALL l_ht2.clear()
				END IF

			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' hash map or dct dictionary.
				
				LET l_target_hash_id = get_target("ht2","hm1","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_ht2 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_ht2.Join(l_hm1)
						WHEN "ht2"
							CALL l_ht2.Join(l_ht2)
						WHEN "dct1"
							CALL l_ht2.Join(l_dct1)
						WHEN "dct2"
							CALL l_ht2.Join(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			ON ACTION "GetKey(INT index)" #GetKey(INT index) - Returns element's value by index
				LET l_msg = "Key = ", trim(l_ht2.GetKey(l_ht2_idx)), "\nArray index=", trim(l_ht2_idx) ,"\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_ht2_idx),  ")", "\nValue=",trim(l_ht2.getValue(l_ht2_idx)),"\nKey = ", trim(l_ht2.GetKey(l_ht2_idx)), "\nArray index=", trim(l_ht2_idx) , "\n(current row in HashMap 2)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")

			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			ON ACTION "ValueSort(INT direction) ASC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_ht2.ValueSort(1)
				MESSAGE "l_ht2: Sorted by value in ascending order"

			ON ACTION "ValueSort(INT direction) DESC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_ht2.ValueSort(0)
				MESSAGE "l_ht2: Sorted by value in ascending order"

			# CustomSort(STRING funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_ht2.CustomSort("CompareAsc")

			ON ACTION "CustomSort(STRING funcName) CompareDESC" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_ht2.CustomSort("CompareDESC")

			ON ACTION "ResetSorting()" #ResetSorting() - resets value sorts and back hashmap to the initial sorting.
				CALL l_ht2.resetSorting()

			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the hashmap.
				CALL l_ht2.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)
	

			ON ACTION "CopyTo(HashMap)" # CopyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
				LET l_target_hash_id = get_target("ht2","hm1","Specify the COPY Target HashMap/Dictionary")
				MESSAGE "COPY l_ht2 object TO ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_ht2.CopyTo(l_hm1)
						WHEN "ht2"
							CALL l_ht2.CopyTo(l_ht2)
						WHEN "dct1"
							CALL l_ht2.CopyTo(l_dct1)
						WHEN "dct2"
							CALL l_ht2.CopyTo(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
	      				
			ON ACTION "LET"
				CALL input_HashPairValue(l_ht2_row_hkey, l_ht2_rec_row.*) RETURNING l_ht2_row_hkey, l_ht2_rec_row.*
				IF NOT int_flag THEN
					LET l_ht2[l_ht2_row_hkey].* = l_ht2_rec_row.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF

			ON ACTION "DISPLAY HashMap TO console"
				#DISPLAY l_ht2 #to console in json format for debugging purpose
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_ht2 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_ht2_row_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_ht2_row_hkey,30,0)

				#DISPLAY l_ht2 #to console in json format for debugging purpose
				IF l_ht2.Contains(l_ht2_row_hkey) THEN	
					OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_ht2[l_ht2_row_hkey] TO console 
	
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hashmap_console
				ELSE
					LET l_msg = "Hash Key ", l_ht2_row_hkey ," does not exist"
					ERROR l_msg
				END IF
				
			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/dictionary
				LET l_ht2_row_hkey = fgl_winprompt(1,1,"Search",l_ht2_row_hkey,30,0)
				IF l_ht2.Contains(l_ht2_row_hkey) THEN				
					CALL display_HashPair(l_ht2_row_hkey,l_ht2,"l_ht2.contains() returned true / (l_ht2 contains key)",".contains(key) method returns true if the key exists")
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_ht2_row_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #"Append"
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_ht2.Join(l_ht_tmp)
						
						WHEN "O" #"Overwrite"
							CALL util.JSON.parse(l_json_string, l_ht2)
							
						WHEN "C" #"Cancel"
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_ht2) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console
				
			ON ACTION "REFRESH" #"DISPLAY UPDATE"
				CALL update_display(l_hm1,l_ht2,l_dct1,l_dct2)		
	  		
	  END INPUT

		#INPUT & Update current HM2 Hash Row Record
		INPUT l_ht2_row_hkey,l_ht2_rec_row.* FROM sc_ht2_rec_row.*
			ON ACTION "act_save_ht2_row_rec"
				CALL l_ht2.Insert(l_ht2_row_hkey, l_ht2_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT



  	#------------------------------------------------------------------
  	# DCT1 Dictionary 1 (l_dct1)
		#------------------------------------------------------------------
	  INPUT ARRAY l_dct1 WITHOUT DEFAULTS FROM sc_dct1_inp.*
	  	BEFORE ROW
	  		LET l_dct1_idx = DIALOG.getCurrentRow("sc_dct1_inp")
	  		DISPLAY l_dct1_idx TO dct1_bucketArrIndex
	  			
	  		IF l_dct1_idx > 0 THEN
	  			LET l_dct1_row_hkey = l_dct1.GetKey(l_dct1_idx)
					LET l_dct1_rec_row.fname =   l_dct1[l_dct1_row_hkey].fname
					LET l_dct1_rec_row.lname =   l_dct1[l_dct1_row_hkey].lname
					LET l_dct1_rec_row.dob =     l_dct1[l_dct1_row_hkey].dob
					LET l_dct1_rec_row.country = l_dct1[l_dct1_row_hkey].country
				END IF
				DISPLAY l_dct1_row_hkey, l_dct1_rec_row.* TO sc_dct1_rec_row.* 


			#dataType Methods	  		
	  	ON ACTION "getSize()" #GetSize() - returns count of elements in hash map
	  		LET l_msg = "The size of l_dct1 -> l_dct1.getSize() = ", trim(l_dct1.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			On ACTION "Delete(INT index)" #Delete(INT index) - Deletes element with index
				IF l_dct1_idx > 0 THEN
	  			DISPLAY l_dct1.delete(l_dct1_idx)
	  			LET l_msg = "l_dct1.delete(",trim(l_dct1_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF

			ON ACTION "Delete(INT first, INT last)"				# Delete(INT first, INT last) - Deletes elements from first till last
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_dct1.getlength()) OR (l_idx2 > l_dct1.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_dct1.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_dct1.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range


			ON ACTION "Remove(VARIANT key)" #Remove(VARIANT key) - Deletes element with key 'key'				
				LET l_dct1_row_hkey = fgl_winprompt(1,1,"Enter Key to be deleted",l_dct1_row_hkey,30,0)
	  		IF l_dct1_row_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_dct1.remove(l_dct1_row_hkey)
	  		END IF

	    ON ACTION "INSERT"
	    	CALL input_HashPairValue(l_dct1_row_hkey, l_dct1_rec_row.*) RETURNING l_dct1_row_hkey, l_dct1_rec_row.*
	      CALL l_dct1.Insert(l_dct1_row_hkey, l_dct1_rec_row)
	      
	    ON ACTION "Clear()" #Clear() - Removes all elements from hash map
				IF fgl_winbutton("Delete All?","Do you want to delete all buckets of this HashMap?","Yes","Yes|No","question") = "Yes" THEN
		      CALL l_dct1.clear()
				END IF

			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' hash map or dct dictionary.
				LET l_target_hash_id = get_target("dct1","dct2","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_dct1 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_dct1.Join(l_hm1)
						WHEN "ht2"
							CALL l_dct1.Join(l_ht2)
						WHEN "dct1"
							CALL l_dct1.Join(l_dct1)
						WHEN "dct2"
							CALL l_dct1.Join(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF
				
			ON ACTION "GetKey(INT index)" #GetKey(INT index) - Returns element's value by index
				LET l_msg = "Key = ", trim(l_dct1.GetKey(l_dct1_idx)), "\nArray index=", trim(l_dct1_idx) ,"\n(current row in Dictionary 1)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_dct1_idx),  ")", "\nValue=",trim(l_dct1.getValue(l_dct1_idx)),"\nKey = ", trim(l_dct1.GetKey(l_dct1_idx)), "\nArray index=", trim(l_dct1_idx) , "\n(current row in Dictionary 1)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")
					      
			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			ON ACTION "ValueSort(INT direction) ASC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_dct1.ValueSort(1)
				MESSAGE "l_dct1: Sorted by value in ascending order"

			ON ACTION "ValueSort(INT direction) DESC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_dct1.ValueSort(0)
				MESSAGE "l_dct1: Sorted by value in ascending order"

			# CustomSort(STRING funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_dct1.CustomSort("CompareAsc")

			ON ACTION "CustomSort(STRING funcName) CompareDESC" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_dct1.CustomSort("CompareDESC")

			ON ACTION "ResetSorting()" #ResetSorting() - resets value sorts and back hashmap to the initial sorting.
				CALL l_dct1.resetSorting()

			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the hashmap.
				CALL l_dct1.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			ON ACTION "CopyTo(HashMap)" # CopyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
				LET l_target_hash_id = get_target("dct1","dct2","Specify the COPY Target HashMap/Dictionary")
				MESSAGE "COPY l_dct1 object TO ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_dct1.CopyTo(l_hm1)
						WHEN "ht2"
							CALL l_dct1.CopyTo(l_ht2)
						WHEN "dct1"
							CALL l_dct1.CopyTo(l_dct1)
						WHEN "dct2"
							CALL l_dct1.CopyTo(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF

			ON ACTION "LET"
				CALL input_HashPairValue(l_dct1_row_hkey, l_dct1_rec_row.*) RETURNING l_dct1_row_hkey, l_dct1_rec_row.*
				IF NOT int_flag THEN
					LET l_dct1[l_dct1_row_hkey].* = l_dct1_rec_row.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF

			ON ACTION "DISPLAY HashMap TO console"
				#DISPLAY l_dct1 #to console in json format for debugging purpose
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_dct1 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_dct1_row_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_dct1_row_hkey,30,0)

				#DISPLAY l_dct1 #to console in json format for debugging purpose
				IF l_dct1.Contains(l_dct1_row_hkey) THEN	
					OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_dct1[l_dct1_row_hkey] TO console 
	
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hashmap_console
				ELSE
					LET l_msg = "Hash Key ", l_dct1_row_hkey ," does not exist"
					ERROR l_msg
				END IF

			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/dictionary
				LET l_dct1_row_hkey = fgl_winprompt(1,1,"Search",l_dct1_row_hkey,30,0)
				IF l_dct1.Contains(l_dct1_row_hkey) THEN				
					CALL display_HashPair(l_dct1_row_hkey,l_dct1,"l_dct1.contains() returned true / (l_dct1 contains key)",".contains(key) method returns true if the key exists")
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim( l_dct1_row_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #"Append"
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_dct1.Join(l_ht_tmp)
						
						WHEN "O" #"Overwrite"
							CALL util.JSON.parse(l_json_string, l_dct1)
							
						WHEN "C" #"Cancel"
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_dct1) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console
		
	  		
	  END INPUT

		#INPUT & Update current DCT1 Hash Row Record
		INPUT l_dct1_row_hkey,l_dct1_rec_row.* FROM sc_dct1_rec_row.*
			ON ACTION "act_save_dct1_row_rec"
				CALL l_dct1.Insert(l_dct1_row_hkey, l_dct1_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT


  	#------------------------------------------------------------------
  	# DCT2 Dictionary 2
		#------------------------------------------------------------------
	  INPUT ARRAY l_dct2 WITHOUT DEFAULTS FROM sc_dct2_inp.* 
	  	BEFORE ROW
	  		LET l_dct2_idx = DIALOG.getCurrentRow("sc_dct2_inp")	
	  		DISPLAY l_dct2_idx TO dct2_bucketArrIndex
	  		
	  		IF l_dct2_idx > 0 THEN
	  			LET l_dct2_row_hkey = l_dct2.GetKey(l_dct2_idx)
					LET l_dct2_rec_row.fname =   l_dct2[l_dct2_row_hkey].fname
					LET l_dct2_rec_row.lname =   l_dct2[l_dct2_row_hkey].lname
					LET l_dct2_rec_row.dob =     l_dct2[l_dct2_row_hkey].dob
					LET l_dct2_rec_row.country = l_dct2[l_dct2_row_hkey].country
				END IF

				DISPLAY l_dct2_row_hkey, l_dct2_rec_row.* TO sc_dct2_rec_row.* 



			#dataType Methods	  		
	  	ON ACTION "getSize()" #GetSize() - returns count of elements in hash map
	  		LET l_msg = "The size of l_dct2 -> l_dct2.getSize() = ", trim(l_dct2.getSize()) 
	  		CALL fgl_winmessage("GestSize()",l_msg,"info")

			On ACTION "Delete(INT index)" #Delete(INT index) - Deletes element with index
				IF l_dct1_idx > 0 THEN
	  			DISPLAY l_dct2.delete(l_dct1_idx)
	  			LET l_msg = "l_dct2.delete(",trim(l_dct1_idx), ") processed. \nDataSet (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("elements deleted",l_msg,"info")
	  		END IF

			ON ACTION "Delete(INT first, INT last)"				# Delete(INT first, INT last) - Deletes elements from first till last
				OPEN WINDOW w_range WITH FORM "form/tools_range" ATTRIBUTE(BORDER,style="center")

				DISPLAY "Delete Range Delete(INT first, INT last)" TO lb_title
				DISPLAY "Enter start and end range index" TO lb_description

				INPUT l_idx1,l_idx2 WITHOUT DEFAULTS FROM idx1, idx2
					AFTER INPUT
					IF (l_idx1 <= 0) OR (l_idx2 <= 0) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be 0 or negative","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_dct2.getlength()) OR (l_idx2 > l_dct2.getlength()) THEN
						CALL fgl_winmessage("Invalid Range entered","Range index can not be greater than the count of buckets (array index)","error")
						CONTINUE INPUT
					END IF					
					IF (l_idx1 > l_idx2) THEN
						CALL fgl_winmessage("Invalid Range entered","Range START index can not be greater than Range END index","error")
						CONTINUE INPUT
					END IF					
					
				END INPUT

				IF int_flag THEN
					LET int_flag = FALSE
					MESSAGE "No elements deleted - User abbort"
				ELSE
					CALL l_dct2.delete(l_idx1,l_idx2)
	  			LET l_msg = "l_dct2.delete(",trim(l_idx1), ",", trim(l_idx2), ") processed. \nDataSet Range (key with corresponding values) has been deleted."
	  			CALL fgl_winmessage("Elements in range deleted",l_msg,"info")
				END IF
				CLOSE WINDOW w_range


			ON ACTION "Remove(VARIANT key)" #Remove(VARIANT key) - Deletes element with key 'key'				
				LET l_dct2_row_hkey = fgl_winprompt(1,1,"Enter Key to be deleted",l_dct2_row_hkey,30,0)
	  		IF l_dct2_row_hkey IS NOT NULL THEN
	  			DISPLAY "remove response: ", l_dct2.remove(l_dct2_row_hkey)
	  		END IF

	    ON ACTION "INSERT"
	    	CALL input_HashPairValue(l_dct2_row_hkey, l_dct2_rec_row.*) RETURNING l_dct2_row_hkey, l_dct2_rec_row.*
	      CALL l_hm1.Insert(l_dct2_row_hkey, l_dct2_rec_row)
     
	    ON ACTION "Clear()" #Clear() - Removes all elements from hash map
				IF fgl_winbutton("Delete All?","Do you want to delete all buckets of this HashMap?","Yes","Yes|No","question") = "Yes" THEN
		      CALL l_dct2.clear()
				END IF
	
			ON ACTION "Join(HashMap)" #Join(HASHMAP l_ht_tmp) - Append element from 'l_ht_tmp' hash map or dct dictionary.
				
				LET l_target_hash_id = get_target("dct2","dct1","Specify the Join Source HashMap/Dictionary")
				MESSAGE "Add (Join) l_dct2 with ", trim(l_target_hash_id)
				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_dct2.Join(l_hm1)
						WHEN "ht2"
							CALL l_dct2.Join(l_ht2)
						WHEN "dct1"
							CALL l_dct2.Join(l_dct1)
						WHEN "dct2"
							CALL l_dct2.Join(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
					
				END IF	
	
			ON ACTION "GetKey(INT index)" #GetKey(INT index) - Returns element's value by index
				LET l_msg = "Key = ", trim(l_dct2.GetKey(l_dct2_idx)), "\nArray index=", trim(l_dct2_idx) ,"\n(current row in Dictionary 2)" 				
				CALL fgl_winmessage("getKey(INT index)",l_msg,"info")

			ON ACTION "GetValue(INT index)" #GetValue(INT index) - Returns element's key by index
				LET l_msg ="getValue(", trim(l_dct2_idx),  ")", "\nValue=",trim(l_dct2.getValue(l_dct2_idx)),"\nKey = ", trim(l_dct2.GetKey(l_dct2_idx)), "\nArray index=", trim(l_dct2_idx) , "\n(current row in Dictionary 2)" 				
				CALL fgl_winmessage("getValue(INT index)",l_msg,"info")
					      
					      
			# ValueSort(INT direction) - !!! BY VALUE !!!
			# sorts hashmap by value. Any further operations, like Insert, Delete and etc., 
			#will be much slower, just because any such operation will trigger 
			#sorting by value. ‘direction' >=1 ASC/ASCENDING Order  <1 =DESC/DESCENDING order 
			#'direction’ is an optional parameter, it’s 1 by default.

			ON ACTION "ValueSort(INT direction) ASC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_dct2.ValueSort(1)
				MESSAGE "l_dct2: Sorted by value in ascending order"

			ON ACTION "ValueSort(INT direction) DESC" #sorts hashmap by value. Any further operations, like Insert will perform slower
				CALL l_dct2.ValueSort(0)
				MESSAGE "l_dct2: Sorted by value in ascending order"

			# CustomSort(STRING funcName) - 
			# sorts hashmap using defined function. The function is a binary function 
			# that accepts two elements of the hashmap, like in the below example.
			ON ACTION "CustomSort(STRING funcName) CompareAsc" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_dct2.CustomSort("CompareAsc")

			ON ACTION "CustomSort(STRING funcName) CompareDESC" #CustomSort(STRING funcName) - sorts hashmap using defined function. The function is a binary function that accepts two elements of the hashmap, like in the below example.
				CALL l_dct2.CustomSort("CompareDESC")
				
			ON ACTION "ResetSorting()" #ResetSorting() - resets value sorts and back hashmap to the initial sorting.
				CALL l_dct2.resetSorting()

			ON ACTION "GetKeys()" # GetKeys() - Returns a DYNAMIC ARRAY of all keys of the hashmap.
				CALL l_dct2.GetKeys() RETURNING l_arr_hkey
				CALL show_hash_keys_list(l_arr_hkey)

			ON ACTION "CopyTo(HashMap)" # CopyTo(HASHMAP dst) - Copies all elements of the hashmap into another hashmap.
				LET l_target_hash_id = get_target("dct2","dct1","Specify the COPY Target HashMap/Dictionary")
				MESSAGE "COPY l_dct2 object TO ", trim(l_target_hash_id)

				IF l_target_hash_id IS NOT NULL THEN
					CASE l_target_hash_id
						WHEN "hm1"
							CALL l_dct2.CopyTo(l_hm1)
						WHEN "ht2"
							CALL l_dct2.CopyTo(l_ht2)
						WHEN "dct1"
							CALL l_dct2.CopyTo(l_dct1)
						WHEN "dct2"
							CALL l_dct2.CopyTo(l_dct2)
									
						OTHERWISE 
							CALL fgl_winmessage("ERROR","Invalid source defined","ERROR")
					END CASE 
				END IF			

			ON ACTION "LET"
				CALL input_HashPairValue(l_dct2_row_hkey, l_dct2_rec_row.*) RETURNING l_dct2_row_hkey, l_dct2_rec_row.*
				IF NOT int_flag THEN
					LET l_dct2[l_dct2_row_hkey].* = l_dct2_rec_row.*
				ELSE
					LET int_flag = FALSE
					MESSAGE "Input abborted by user"
				END IF

			ON ACTION "DISPLAY HashMap TO console"
				#DISPLAY l_dct2 #to console in json format for debugging purpose
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				DISPLAY l_dct2 TO console 
				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				CLOSE WINDOW cm_hashmap_console

			ON ACTION "DISPLAY HashMap Bucket TO console"
				LET l_dct2_row_hkey = fgl_winprompt(1,1,"Enter the key to be displayed (none existing will be inserted)",l_dct2_row_hkey,30,0)

				#DISPLAY l_dct2 #to console in json format for debugging purpose
				IF l_dct2.Contains(l_dct2_row_hkey) THEN	
					OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
					DISPLAY l_dct2[l_dct2_row_hkey] TO console 
	
					MENU
						BEFORE MENU
							 CALL dialog.setActionHidden("ACCEPT",TRUE)
						ON ACTION "CANCEL"
							EXIT MENU
					END MENU
					CLOSE WINDOW cm_hashmap_console
				ELSE
					LET l_msg = "Hash Key ", l_dct2_row_hkey ," does not exist"
					ERROR l_msg
				END IF

			ON ACTION "CONTAINS(VARIANT key)" #.contains(key) checks if an element with the given key exists in the HashMap/dictionary
				LET l_dct2_row_hkey = fgl_winprompt(1,1,"Search",l_dct2_row_hkey,30,0)
				IF l_dct2.Contains(l_dct2_row_hkey) THEN				
					CALL display_HashPair(l_dct2_row_hkey,l_dct2,"l_dct2.contains() returned true / (l_dct1 contains key)",".contains(key) method returns true if the key exists")					
			  ELSE
			  	LET l_msg = "HashMap does not contain ", trim(l_dct2_row_hkey)
			  	CALL fgl_winmessage("Search NOT Found",l_msg,"WARNING")
				END IF

			ON ACTION "util.JSONObject.toFGL(hash)"
				LET l_json_string = edit_json_string(NULL)
				IF l_json_string IS NOT NULL THEN
					LET l_choice = fgl_winbutton("Replace or Copy?","Do you want to overwrite or append the HashMap with the parsed JSON data?","Append","Append|Overwrite|Cancel","question")
					
					CASE l_choice
						WHEN "A" #"Append"
							CALL util.JSON.parse(l_json_string, l_ht_tmp)
							CALL l_dct2.Join(l_ht_tmp)
						
						WHEN "O" #"Overwrite"
							CALL util.JSON.parse(l_json_string, l_dct2)
							
						WHEN "C" #"Cancel"
							MESSAGE "Abborted"
						
						OTHERWISE
							CALL fgl_winmessage("invalid choice","invalid choice","error")
					
					END CASE
				END IF			
	
			ON ACTION "util.JSONObject.fromFGL(hash)"
				OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
				
				DISPLAY NULL TO console
		  	LET l_jsonObj = util.JSONObject.fromFGL(l_dct2) #populate json object with data from dictonary
		  	DISPLAY l_jsonObj.toString()	TO console #display data from json objectobj

				MENU
					BEFORE MENU
						 CALL dialog.setActionHidden("ACCEPT",TRUE)
					ON ACTION "CANCEL"
						EXIT MENU
				END MENU
				
				CLOSE WINDOW cm_hashmap_console

	  END INPUT

		#INPUT & Update current dct2 Hash Row Record
		INPUT l_dct2_row_hkey,l_dct2_rec_row.* FROM sc_dct2_rec_row.*
			ON ACTION "act_save_dct2_row_rec"
				CALL l_dct2.Insert(l_dct2_row_hkey, l_dct2_rec_row) #INSERT() needs the record (not the expanded .* record values)
		END INPUT

	}	
		#DIALOG Actions
		#------------------------------------------------

		ON ACTION "act_clear_hm1_row_rec"
			INITIALIZE l_hm1_value_rec_row.* TO NULL

		ON ACTION "act_clear_ht2_row_rec"
			INITIALIZE l_ht2_rec_row.* TO NULL

		ON ACTION "act_clear_dct1_row_rec"
			INITIALIZE l_dct1_rec_row.* TO NULL

		ON ACTION "act_clear_dct2_row_rec"
			INITIALIZE l_dct2_rec_row.* TO NULL

		ON ACTION "REFRESH" #"DISPLAY UPDATE"
			CALL update_display(l_hm1,l_ht2,l_dct1, l_dct2)		

		ON ACTION "EXIT"
			EXIT DIALOG

	END DIALOG 
	#END DIALOG --------------------------------------------------------------------

END MAIN
##################################################################################
# END MAIN
##################################################################################     



##################################################################################
# FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
#
#
##################################################################################  
FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
	DEFINE p_rec_dict1 OF dt_rec_HASH_ELEMENT 
	DEFINE p_rec_dict2 OF dt_rec_HASH_ELEMENT 
	
  RETURN p_rec_dict1.hValue.lname < p_rec_dict2.hValue.lname
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
##################################################################################  


##################################################################################
# FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
#
#
##################################################################################  
FUNCTION CompareDesc(p_rec_dict1, p_rec_dict2)
	DEFINE p_rec_dict1 OF dt_rec_HASH_ELEMENT 
	DEFINE p_rec_dict2 OF dt_rec_HASH_ELEMENT 

	RETURN p_rec_dict1.hValue.lname >= p_rec_dict2.hValue.lname
END FUNCTION
##################################################################################
# END FUNCTION CompareAsc(p_rec_dict1, p_rec_dict2)
##################################################################################  


##################################################################################
# FUNCTION update_display(p_hm1,p_ht2,p_dct1,p_dct2)
#
#
##################################################################################     
FUNCTION update_display(p_hm1_key,p_hm1,p_ht2_key,p_ht2,p_dct1_key,p_dct1,p_dct2_key,p_dct2)
	DEFINE p_hm1_key VARIANT
	DEFINE p_hm1 HASHMAP OF dt_rec_HASH_VALUE
	DEFINE p_ht2_key VARIANT
	DEFINE p_ht2 HASHMAP OF dt_rec_HASH_VALUE
	DEFINE p_dct1_key VARIANT
	DEFINE p_dct1 DICTIONARY OF dt_rec_HASH_VALUE
	DEFINE p_dct2_key VARIANT
	DEFINE p_dct2 DICTIONARY OF dt_rec_HASH_VALUE

	# RECORD INPUT -----------------------------------------------------------
	#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
	IF p_hm1.getSize() > 0 THEN
		DISPLAY p_hm1_key TO hm1_row_hKey
		DISPLAY p_hm1[p_hm1_key].fname TO hm1_value_fname
		DISPLAY p_hm1[p_hm1_key].lname TO hm1_value_lname
		DISPLAY p_hm1[p_hm1_key].dob TO hm1_value_dob
		DISPLAY p_hm1[p_hm1_key].country TO hm1_value_country
		# RECORD.ARRAY INPUT -----------------------------------------------------------
		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		DISPLAY ARRAY p_hm1[p_hm1_key].transaction TO sc_hm1_val_arr.* WITHOUT SCROLL
		END DISPLAY
	END IF

	 
#	DISPLAY ARRAY p_hm1 TO sc_hm1_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
#	DISPLAY ARRAY p_ht2 TO sc_ht2_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
#	DISPLAY ARRAY p_dct1 TO sc_dct1_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
#	DISPLAY ARRAY p_dct2 TO sc_dct2_inp.* WITHOUT SCROLL #ATTRIBUTE(WITHOUT SCROLL)
			
END FUNCTION
##################################################################################
# END FUNCTION update_display(p_hm1,p_ht2,p_dct1,p_dct2)
##################################################################################     


##################################################################################
# FUNCTION init()
#
#
##################################################################################     
FUNCTION init(p_hm1, p_ht2, p_dct1, p_dct2)
	DEFINE l_rec_hvalue dt_rec_HASH_VALUE
  DEFINE p_hm1 HASHMAP OF dt_rec_HASH_VALUE
  DEFINE p_ht2 HASHMAP OF dt_rec_HASH_VALUE
  DEFINE p_dct1 DICTIONARY OF dt_rec_HASH_VALUE
  DEFINE p_dct2 DICTIONARY OF dt_rec_HASH_VALUE
	DEFINE l_hm1_key VARIANT
	DEFINE l_ht2_key VARIANT
	DEFINE l_dct1_key VARIANT
	DEFINE l_dct2_key VARIANT

	DEFINE l_arr_size SMALLINT
	DEFINE l_arr2_size SMALLINT	
  DEFINE i SMALLINT
  DEFINE i2 SMALLINT
  DEFINE l_date DATE

  LET l_arr_size = 10
  LET l_arr2_size = 50
  
  FOR i = 1 TO l_arr_size
  	LET l_hm1_key = "hm1_key_", trim(i)

  	LET l_rec_hvalue.fName = "hm1_value_fname_", trim(i)
  	LET p_hm1[l_hm1_key].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "hm1_value_lName_", trim(i)
  	LET p_hm1[l_hm1_key].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 150*i
  	LET l_rec_hvalue.dob = l_date #"hm1_value_dob_", trim(i)
  	LET p_hm1[l_hm1_key].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "hm1_value_country_", trim(i)
  	LET p_hm1[l_hm1_key].country = l_rec_hvalue.country

		FOR i2 = 1 TO l_arr2_size
	  	LET p_hm1[l_hm1_key].transaction[i2]= 0.33 + (i*87.11) + (i2*3.73)
	  END FOR
  END FOR

  FOR i = 1 TO l_arr_size
  	LET l_ht2_key = "ht2_key_", trim(i)

  	LET l_rec_hvalue.fName = "ht2_value_fname_", trim(i)
  	LET p_ht2[l_ht2_key].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "ht2_value_lName_", trim(i)
  	LET p_ht2[l_ht2_key].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 200*i
  	LET l_rec_hvalue.dob = l_date #"ht2_value_dob_", trim(i)
  	LET p_ht2[l_ht2_key].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "ht2_value_country_", trim(i)
  	LET p_ht2[l_ht2_key].country = l_rec_hvalue.country

		FOR i2 = 1 TO l_arr2_size
	  	LET p_ht2[l_ht2_key].transaction[i2]= 100.44 + (i*87.11) + (i2*3.73)
	  END FOR
	  
  END FOR    

  FOR i = 1 TO l_arr_size
  	LET l_dct1_key = "dct1_key_", trim(i)

  	LET l_rec_hvalue.fName = "dct1_value_fname_", trim(i)
  	LET p_dct1[l_dct1_key].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "dct1_value_lName_", trim(i)
  	LET p_dct1[l_dct1_key].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 250*i
  	LET l_rec_hvalue.dob = l_date #"dct1_value_dob_", trim(i)
  	LET p_dct1[l_dct1_key].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "dct1_value_country_", trim(i)
  	LET p_dct1[l_dct1_key].country = l_rec_hvalue.country

		FOR i2 = 1 TO l_arr2_size
	  	LET p_dct1[l_dct1_key].transaction[i2]= 200.55 + (i*87.11) + (i2*3.73)
	  END FOR
	  
  END FOR

  FOR i = 1 TO l_arr_size
  	LET l_dct2_key = "dct2_key_", trim(i)

  	LET l_rec_hvalue.fName = "dct2_value_fname_", trim(i)
  	LET p_dct2[l_dct2_key].fName = l_rec_hvalue.fName

  	LET l_rec_hvalue.lName = "dct2_value_lName_", trim(i)
  	LET p_dct2[l_dct2_key].lName = l_rec_hvalue.lName

		LET l_date = "12/12/1980" 
		LET l_date = l_date + 300*i
  	LET l_rec_hvalue.dob = l_date #"dct2_value_dob_", trim(i)
  	LET p_dct2[l_dct2_key].dob = l_rec_hvalue.dob

  	LET l_rec_hvalue.country = "dct2_value_country_", trim(i)
  	LET p_dct2[l_dct2_key].country = l_rec_hvalue.country

		FOR i2 = 1 TO l_arr2_size
	  	LET p_dct2[l_dct2_key].transaction[i2]= 300.66 + (i*87.11) + (i2*3.73)
	  END FOR
	  
  END FOR    

	#return first key #just for demo.. optional...
	LET l_hm1_key = p_hm1.getKey(1)
	LET l_ht2_key = p_ht2.getKey(1)
	LET l_dct1_key = p_dct1.getKey(1)
	LET l_dct2_key = p_dct2.getKey(1)
	
	RETURN l_hm1_key,l_ht2_key,l_dct1_key,l_dct2_key
END FUNCTION
##################################################################################
# END FUNCTION init()
##################################################################################     


##################################################################################
# FUNCTION debug(p_hm1, p_ht2, p_dct1, p_dct2)
#
#
##################################################################################     
FUNCTION debug(p_hm1, p_ht2, p_dct1, p_dct2)
  DEFINE p_hm1  HASHMAP OF STRING
  DEFINE p_ht2  HASHMAP OF STRING
  DEFINE p_dct1 DICTIONARY OF STRING
  DEFINE p_dct2 DICTIONARY OF STRING
  
	DEFINE i SMALLINT
	
  DISPLAY "HashMap 1 data:"
  FOR i = 1 TO p_hm1.GetLength()
    DISPLAY "'", p_hm1.GetKey(i), "' - '", p_hm1.GetValue(i), "'"
  END FOR

  DISPLAY "HashMap 2 data:"
  FOR i = 1 TO p_ht2.GetLength()
    DISPLAY "'", p_ht2.GetKey(i), "' - '", p_ht2.GetValue(i), "'"
  END FOR

  DISPLAY "Dictionary 1 data:"
  FOR i = 1 TO p_dct1.GetLength()
    DISPLAY "'", p_dct1.GetKey(i), "' - '", p_dct1.GetValue(i), "'"
  END FOR

  DISPLAY "Dictionary 2 data:"
  FOR i = 1 TO p_dct2.GetLength()
    DISPLAY "'", p_dct2.GetKey(i), "' - '", p_dct2.GetValue(i), "'"
  END FOR

END FUNCTION
##################################################################################
# END FUNCTION debug(p_hm1, p_ht2, p_dct1, p_dct2)
##################################################################################




##################################################################################
# FUNCTION input_HashMapBucket(p_hkey, p_hashMap)
#
#
##################################################################################
FUNCTION input_HashMapBucket(p_hkey, p_hashMap)
	DEFINE p_hkey VARIANT
	DEFINE p_hashMap HASHMAP OF dt_rec_HASH_VALUE		#hashMap example 1
	DEFINE l_hkey_backup VARIANT
	DEFINE l_HashMapBucket_backup OF dt_rec_HASH_VALUE	
	DEFINE l_hkey VARIANT
	DEFINE l_rec_hvalue OF dt_rec_HASH_VALUE

	#Backup original values
	LET l_hkey_backup = p_hkey
	LET l_HashMapBucket_backup.* = p_hashMap[p_hkey].* 

	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_record" ATTRIBUTE(BORDER,style="center")

	DIALOG ATTRIBUTE(UNBUFFERED)

		# RECORD INPUT -----------------------------------------------------------
		#INPUT & Update current hashMap Hash Row Record (sub element of this hashMap)
		INPUT 
			p_hkey,
			p_hashMap[p_hkey].fname, 
			p_hashMap[p_hkey].lname,
			p_hashMap[p_hkey].dob,
			p_hashMap[p_hkey].country WITHOUT DEFAULTS FROM sc_hashPair.*
		END INPUT

		# RECORD.ARRAY INPUT -----------------------------------------------------------
		#INPUT & Update current hashMap Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY p_hashMap[p_hkey].transaction WITHOUT DEFAULTS FROM sc_arr_hvalue.*
		END INPUT
		
		ON ACTION "CANCEL"
			EXIT DIALOG
						
		ON ACTION "ACCEPT"
			EXIT DIALOG
	END DIALOG
	
	IF int_flag THEN #CANCEL
		#RESTORE in case things have changed
		LET p_hashMap[l_hkey_backup].* = l_HashMapBucket_backup.*
	ELSE
		#check if key has changed
		IF p_hkey != l_hkey_backup THEN
			LET p_hashMap[p_hkey].* = p_hashMap[l_hkey_backup].*
			CALL p_hashMap.remove("l_hkey_backup")			
		END IF
		
		LET int_flag = FALSE
		
	END	IF  	
	
	CLOSE WINDOW w_hashPair

END FUNCTION
##################################################################################
# END FUNCTION input_HashMapBucket(p_hkey, p_hashMap)
##################################################################################


##################################################################################
# FUNCTION input_HashPairValue(p_hkey, p_rec_hvalue)
#
#
##################################################################################
FUNCTION input_HashPairValue(p_hkey, p_rec_hvalue)
	DEFINE p_hkey VARIANT
	DEFINE p_rec_hvalue OF dt_rec_HASH_VALUE
	DEFINE l_hkey VARIANT
	DEFINE l_rec_hvalue OF dt_rec_HASH_VALUE

	LET l_hkey = p_hkey
	LET l_rec_hvalue.* = p_rec_hvalue.* 		

	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_record" ATTRIBUTE(BORDER,style="center")

	DIALOG ATTRIBUTE(UNBUFFERED)

		# RECORD INPUT -----------------------------------------------------------
		#INPUT & Update current HM1 Hash Row Record (sub element of this hashMap)
		INPUT 
			p_hkey,
			p_rec_hvalue.fname, 
			p_rec_hvalue.lname,
			p_rec_hvalue.dob,
			p_rec_hvalue.country WITHOUT DEFAULTS FROM sc_hashPair.*
		END INPUT

		# RECORD.ARRAY INPUT -----------------------------------------------------------
		#INPUT & Update current HM1 Hash Row ARRAY (sub element of this hashMap)
		INPUT ARRAY p_rec_hvalue.transaction WITHOUT DEFAULTS FROM sc_arr_hvalue.*
		END INPUT
		
		ON ACTION "CANCEL"
			LET l_rec_hvalue.* = p_rec_hvalue.*
			LET int_flag = FALSE
			
		ON ACTION "ACCEPT"
			LET p_rec_hvalue.* = l_rec_hvalue.*
			EXIT DIALOG
	END DIALOG
	
	CLOSE WINDOW w_hashPair
	RETURN p_hkey,p_rec_hvalue.*
END FUNCTION
##################################################################################
# END FUNCTION input_HashPairValue(p_rec_hash_pair)
##################################################################################


##################################################################################
# FUNCTION display_HashPair(p_hkey,p_hashMap,p_title,p_description)
#
#
##################################################################################
FUNCTION display_HashPair(p_hkey,p_hashMap,p_title,p_description)
	DEFINE p_hkey VARIANT
	DEFINE p_hashMap HASHMAP OF dt_rec_HASH_VALUE	
	DEFINE p_title STRING
	DEFINE p_description STRING

	OPEN WINDOW w_hashPair WITH FORM "form/cm_hash_input_mixed" ATTRIBUTE(BORDER,style="center")
	
	DISPLAY p_title TO lb_title
	DISPLAY p_description TO lb_description

	DISPLAY p_hkey TO key
	DISPLAY p_hashMap[p_hkey].fname TO fName
	DISPLAY p_hashMap[p_hkey].lname TO lName
	DISPLAY p_hashMap[p_hkey].dob TO dob
	DISPLAY p_hashMap[p_hkey].country TO country
	
	DISPLAY ARRAY p_hashMap[p_hkey].transaction TO sc_arr_hvalue.* WITHOUT SCROLL
	END DISPLAY
	
	IF int_flag THEN LET int_flag = FALSE END IF
	
	CLOSE WINDOW w_hashPair
END FUNCTION
##################################################################################
# END FUNCTION display_HashPair(p_hkey,p_rec_hvalue,p_title,p_description)
##################################################################################

##################################################################################
# FUNCTION edit_json_string(p_json_string)
#
#
##################################################################################
FUNCTION edit_json_string(p_json_string)
	DEFINE p_json_string STRING
	
	IF p_json_string IS NULL THEN
		LET p_json_string = 
			'{
				"hm1_key_1":{
					"fname":"hm1_value_fname_1",
					"lname":"hm1_value_lName_1",
					"dob":"11.05.1981",
					"country":"hm1_value_country_1"},
				"hm1_key_2":{
					"fname":"hm1_value_fname_2",
					"lname":"hm1_value_lName_2",
					"dob":"08.10.1981",
					"country":"hm1_value_country_2"},
				"hm1_key_3":{
					"fname":"hm1_value_fname_3",
					"lname":"hm1_value_lName_3",
					"dob":"07.03.1982",
					"country":"hm1_value_country_3"},
				"hm1_key_4":{
					"fname":"hm1_value_fname_4",
					"lname":"hm1_value_lName_4",
					"dob":"04.08.1982",
					"country":"hm1_value_country_4"},
				"hm1_key_5":{
					"fname":"hm1_value_fname_5",
					"lname":"hm1_value_lName_5",
					"dob":"01.01.1983",
					"country":"hm1_value_country_5"},
				"hm1_key_6":{
					"fname":"hm1_value_fname_6",
					"lname":"hm1_value_lName_6",
					"dob":"31.05.1983",
					"country":"hm1_value_country_6"},
				"hm1_key_7":{
					"fname":"hm1_value_fname_7",
					"lname":"hm1_value_lName_7",
					"dob":"28.10.1983",
					"country":"hm1_value_country_7"},
				"hm1_key_8":{
					"fname":"hm1_value_fname_8",
					"lname":"hm1_value_lName_8",
					"dob":"26.03.1984",
					"country":"hm1_value_country_8"},
				"hm1_key_9":{
					"fname":"hm1_value_fname_9",
					"lname":"hm1_value_lName_9",
					"dob":"23.08.1984",
					"country":"hm1_value_country_9"
				},
				"hm1_key_10":{
					"fname":"hm1_value_fname_10",
					"lname":"hm1_value_lName_10",
					"dob":"20.01.1985",
					"country":"hm1_value_country_10"
				}
			}
			'
	END IF
	
	OPEN WINDOW cm_hashmap_console WITH FORM "form/cm_hashmap_console" ATTRIBUTE(BORDER,style="centered")
	
	INPUT p_json_string WITHOUT DEFAULTS FROM console ATTRIBUTE(UNBUFFERED)
	END INPUT
	
	CLOSE WINDOW cm_hashmap_console
	RETURN p_json_string
END FUNCTION
##################################################################################
# END FUNCTION edit_json_string(p_json_string)
##################################################################################