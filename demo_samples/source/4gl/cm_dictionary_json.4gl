################################################################################
# MAIN
#
#
################################################################################
MAIN
  DEFINE dct DICTIONARY
  DEFINE jObj util.JSONObject
  DEFINE l_jObj_import util.JSONObject
  DEFINE k, v STRING	#only used to add additional pairs of data
	DEFINE i int
  
	OPTIONS INPUT WRAP
	
	OPEN WINDOW cm_dictionary_json WITH FORM "form/cm_dictionary_json"  
#  -- JSON -> Dictionary #Populate dictionary with test data using json
  CALL util.JSON.parse("{\"title\":\"Mr.\",\"fname\":\"Hubert\",\"lname\":\"Hölzl\",\"city\":\"Munich\",\"pcode\":\"85464\",\"country\":\"Germany\",\"income\":\"182321.34\"}", dct)
  DISPLAY dct	#display contents data of dictionary

	DISPLAY dct["title"] TO f_title
	DISPLAY dct["fname"] TO f_fname
	DISPLAY dct["lname"] TO f_lname
	DISPLAY dct["city"] TO f_city
	DISPLAY dct["pcode"] TO f_pcode
	DISPLAY dct["country"] TO f_country
	DISPLAY dct["income"] TO f_income

	INPUT ARRAY dct WITHOUT DEFAULTS FROM inp.* # ATTRIBUTES(UNBUFFERED, APPEND ROW = true, auto append = true)
		ON ACTION "SORT Val Asc"
			CALL dct.ValueSort(1) #asc

		ON ACTION "SORT Val Desc"
			CALL dct.ValueSort(0) #desc

		ON ACTION "json"
			LET l_jObj_import= '"{"city":"Hamburg","country":"Germany","fname":"Swen","income":"193.34","lname":"Fischkopf","pcode":"24123","title":"Dr."}"'
			CALL l_jObj_import.ToFGL(dct)
	
		ON ACTION "dict->json"
			DISPLAY NULL TO f_json_obj
	  	LET jObj = util.JSONObject.fromFGL(dct) #populate json object with data from dictonary
	  	DISPLAY jObj.toString()	TO f_json_obj #display data from json objectobj

		ON ACTION "clear_output"
			DISPLAY NULL TO f_json_obj

    ON ACTION "ADD"
      PROMPT "Enter KEY" FOR k
      PROMPT "Enter VALUE" FOR v
      CALL dct.Insert(k, v)
      DISPLAY "\nHashMap data:"
      DISPLAY dct.GetLength()
      DISPLAY dct
      
      FOR i = 1 TO dct.GetLength()
         DISPLAY "'", dct.GetKey(i), "' - '", dct.GetValue(i), "'"
      END FOR
    	
	END INPUT		
  
END MAIN
################################################################################
# END MAIN
################################################################################