DEFINE dict_value TYPE AS
    RECORD
      f1 INT,
      f2 STRING
    END RECORD
DEFINE modu_console STRING    

################################################################################
# MAIN
#
#
################################################################################
MAIN
  DEFINE tmp_rec OF dict_value
  DEFINE dct DICTIONARY OF dict_value
  
  DEFINE l_idx SMALLINT
  DEFINE i smallint
  DEFINE l_console_txt STRING
  
  OPEN WINDOW dictonary WITH FORM "form/cm_dictonary"
	CALL ui.Interface.setText("Dictonary")
	CALL fgl_settitle("Dictionary")
	  
  LET dct["key_1"].f1 = 5
  LET dct["key_1"].f2 = "str1"
  
  LET dct["key_2"].f1 = 2
  LET dct["key_2"].f2 = "str2"
  
  LET dct["key_3"].f1 = 9
  LET dct["key_3"].f2 = "str3"
  
  DISPLAY ARRAY dct TO inp.* ATTRIBUTE(UNBUFFERED)
  	BEFORE ROW
  		LET l_idx = arr_curr()
  		DISPLAY l_idx TO f_arr_curr
  		DISPLAY dct.GetKey(l_idx) TO f_key
  		
  		CALL dct.GetValue(l_idx) RETURNING tmp_rec.*
  		
  		DISPLAY tmp_rec.*
  		DISPLAY tmp_rec.f1 TO f_f1
  		DISPLAY tmp_rec.f2 TO f_f2

			LET l_console_txt = dct.GetKey(l_idx), "' - '", dct.GetValue(l_idx), "'"
 			LET l_console_txt = l_console_txt, "\n-----------------------------------------"
			#LET l_console_txt = NULL
		  FOR i = 1 TO dct.GetLength()
		    LET l_console_txt = l_console_txt, "\n", "'", dct.GetKey(i), "' - '", dct.GetValue(i), "'"
		  END FOR
  		CALL out_console(l_console_txt)
  		 
  	ON ACTION "Sort by key"
  		MESSAGE "Sorting by key:"
  		CALL DisplayDictionary(dct)

  	ON ACTION "Sort by val ASC"
		  MESSAGE "\nSorting by value Asc:"
		  CALL dct.ValueSort(1)
		  CALL DisplayDictionary(dct)

  	ON ACTION "Sort by val DESC"
		  MESSAGE "\nSorting by value Desc:"
		  CALL dct.ValueSort(0)
		  CALL DisplayDictionary(dct)

  	ON ACTION "Compare Asc"
		  MESSAGE "\nSorting Asc by 'f2' field using function CompareAsc:"
		  CALL dct.CustomSort("CompareAsc")      
		  CALL DisplayDictionary(dct)
		  
  	ON ACTION "Compare Desc"
		  MESSAGE "\nSorting Asc by 'f2' field using function CompareDesc:"
		  CALL dct.CustomSort("CompareDesc")      
		  CALL DisplayDictionary(dct)
  
  END DISPLAY

END MAIN
################################################################################
# END MAIN
################################################################################


################################################################################
# FUNCTION DisplayDictionary(dct)
#
#
################################################################################
FUNCTION DisplayDictionary(dct)
  DEFINE dct DICTIONARY
  DEFINE i INT
  DEFINE l_console_txt STRING
  
  LET l_console_txt = NULL
  
  FOR i = 1 TO dct.GetLength()
    LET l_console_txt = trim(l_console_txt), "\n", "'", dct.GetKey(i), "' - '", dct.GetValue(i), "'"
  END FOR
  
  CALL out_console(l_console_txt)
  
END FUNCTION
################################################################################
# END FUNCTION DisplayDictionary(dct)
################################################################################


################################################################################
# FUNCTION CompareAsc(a, b)
#
#
################################################################################
FUNCTION CompareAsc(a, b)
  DEFINE a, b RECORD key REFERENCE, val dict_value END RECORD
  RETURN a.val.f2 < b.val.f2
END FUNCTION
################################################################################
# END FUNCTION CompareAsc(a, b)
################################################################################


################################################################################
# FUNCTION CompareDesc(a, b)
#
#
################################################################################
FUNCTION CompareDesc(a, b)
  DEFINE a, b RECORD key REFERENCE, val dict_value END RECORD
  RETURN a.val.f2 >= b.val.f2
END FUNCTION
################################################################################
# END FUNCTION CompareDesc(a, b)
################################################################################

################################################################################
# FUNCTION out_console(p_out_txt)
#
#
################################################################################
FUNCTION out_console(p_out_txt)
	DEFINE p_out_txt STRING

	LET modu_console = p_out_txt, "\n------------------------------------------\n", trim(modu_console)  
	DISPLAY modu_console TO f_console
	
END FUNCTION
################################################################################
# END FUNCTION out_console(p_out_txt)
################################################################################