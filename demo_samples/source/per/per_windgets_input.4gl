##################################################################################
# GLOBAL Scope Variables
##################################################################################
GLOBALS
	DEFINE i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10 INT
	DEFINE c30,c31,c32,c33,c34 CHAR(3)

	DEFINE d21_00,d21_05,d21_10,d21_15,d21_20 DECIMAL(2,1)
	DEFINE d32_00,d32_05,d32_10,d32_15,d32_20,d32_25,d32_30,d32_35,d32_40,d32_45,d32_50 DECIMAL(2,1)
	
	DEFINE da0,da1,da2,da3,da4,da5 DATE
	DEFINE iNC0,iNC1,iNC2,iNC3,iNC4 STRING
	DEFINE iNS0,iNS1,iNS2,iNS3,iNS4 STRING

END GLOBALS

##################################################################################
# FUNCTION formHandler(pInputMode,pFormName)
#
#
##################################################################################
FUNCTION formHandler(pInputMode,pFormName)
	DEFINE pFormName STRING
	DEFINE pInputMode INT

	CALL init_data()	
	
	#LET pFormName = pFormName.lower()
	CASE pFormName
	
		WHEN "per_widgets_comboListBox_01"
			IF pInputMode = 0 THEN
				DISPLAY i0,i1,i2,i3 TO r1,r2,r3,r4
			ELSE
				INPUT i0,i1,i2,i3 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF

		WHEN "per_widgets_comboListBox_02"
			IF pInputMode = 0 THEN
				DISPLAY c31,c32,c33,c34 TO r1,r2,r3,r4
			ELSE
				INPUT c31,c32,c33,c34 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF
		
		WHEN "per_widgets_comboListBox_03"
			IF pInputMode = 0 THEN
				DISPLAY d21_05,d21_10,d21_15,d21_20 TO r1,r2,r3,r4
			ELSE
				INPUT d21_05,d21_10,d21_15,d21_20 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF			
		
		WHEN "per_widgets_comboListBox_04"
			IF pInputMode = 0 THEN
				DISPLAY da1,da2,da3,da4 TO r1,r2,r3,r4
			ELSE
				INPUT da1,da2,da3,da4 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF				

		WHEN "per_widgets_comboListBox_05"
			IF pInputMode = 0 THEN
				DISPLAY i1,i2,i3,i4 TO r1,r2,r3,r4
			ELSE
				INPUT i1,i2,i3,i4 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF


		WHEN "per_widgets_comboListBox_06"
			IF pInputMode = 0 THEN
				DISPLAY iNC1,iNS2,d21_15,iNC4 TO r1,r2,r3,r4
			ELSE
				INPUT iNC1,iNS2,d21_15,iNC4 WITHOUT DEFAULTS FROM r1,r2,r3,r4
			END IF

		WHEN "per_widgets_comboListBox_07"
			IF pInputMode = 0 THEN
				DISPLAY d32_10,d32_20,d32_30,d32_40,d32_50 TO r1,r2,r3,r4,r5
			ELSE
				INPUT d32_10,d32_20,d32_30,d32_40,d32_50 WITHOUT DEFAULTS FROM r1,r2,r3,r4, r5
			END IF

		WHEN "per_widgets_comboListBox_08"
			IF pInputMode = 0 THEN
				DISPLAY da1,da2,da3,da4,da5 TO r1,r2,r3,r4,r5
			ELSE
				INPUT da1,da2,da3,da4,da5 WITHOUT DEFAULTS FROM r1,r2,r3,r4,r5
			END IF
				
	END CASE

END FUNCTION
##################################################################################
# END FUNCTION formHandler(pInputMode,pFormName)
##################################################################################


##################################################################################
# FUNCTION init_data()
#
#
##################################################################################
FUNCTION init_data()

	LET i0 = 0
	LET i1 = 1
	LET i2 = 2
	LET i3 = 3
	LET i4 = 4
	LET i5 = 5
	LET i6 = 6
	LET i7 = 7
	LET i8 = 8
	LET i9 = 9
	LET i10 = 10
	
	LET c30 = ""
	LET c31 = "AAA"
	LET c32 = "ABB"
	LET c33 = "BAA"
	LET c34 = "BBB" 
	
	LET d21_00 = 0.0
	LET d21_05 = 0.5
	LET d21_10 = 1.0
	LET d21_15 = 1.5
	LET d21_20 = 2.0
	
	
	LET d32_00 = 0.00
	LET d32_05 = 0.50
	LET d32_10 = 1.00
	LET d32_15 = 1.50
	LET d32_20 = 2.00
	LET d32_25 = 2.50
	LET d32_30 = 3.00
	LET d32_35 = 3.50
	LET d32_40 = 4.00
	LET d32_45 = 4.50
	LET d32_50 = 5.00
	
	LET da0 = NULL
	LET da1 = "6/6/2019"
	LET da2 = "12/12/2019"
	LET da3 = "12/25/2020"
	LET da4 = "06/06/2021" 
	LET da5 = "09/09/2021"
	 
	LET iNC0 = "Null"
	LET iNC1 = "One"
	LET iNC2 = "Two"
	LET iNC3 = "Three"
	LET iNC4 = "Four"
	
	LET iNS0 = "null"
	LET iNS1 = "one"
	LET iNS2 = "two"
	LET iNS3 = "three"
	LET iNs4 = "four"

END FUNCTION
##################################################################################
# END FUNCTION init_data()
##################################################################################


################################################################################
# FUNCTION initWidget()
#
# Init functions for forms (defined within the form i.e.  INITIALIZER=initWidget;
################################################################################
FUNCTION initWidget()
	CALL fgl_winmessage("initWidget()","initWidget()","info")
END FUNCTION
################################################################################
# END FUNCTION initWidget()
################################################################################