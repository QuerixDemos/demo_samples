PRIVATE DEFINE m_rec RECORD
		signature STRING,
		s_description STRING,
		l_description STRING,
		sample_code STRING
	END RECORD


############################################################################
# MAIN
#
#
############################################################################
	MAIN
	DEFINE l_listChoice STRING

	OPTIONS INPUT WRAP

	OPEN WINDOW w WITH FORM "dt_string_method"

	INPUT l_listChoice WITHOUT DEFAULTS FROM listChoice ATTRIBUTE(UNBUFFERED)
		ON CHANGE listChoice
			CALL menu_process(l_listChoice)
	END INPUT

END MAIN
############################################################################
# END MAIN
############################################################################


############################################################################
# FUNCTION menu_process(p_choice STRING)
#
#
############################################################################
FUNCTION menu_process(p_choice STRING)
	DEFINE l_result STRING
	DEFINE l_tmp_str STRING

	CASE p_choice.toLowerCase()
		WHEN "append"
			LET l_result = string_append()
		WHEN "equals"
			LET l_result = string_equals()
		WHEN "equalsignorecase"
			LET l_result = string_equalsIgnoreCase()
		WHEN "getcharat"
			LET l_result = string_getCharAt()
		WHEN "getindexof"
			LET l_result = string_getIndexOf()			
		WHEN "getlength"
			LET l_result = string_getLength()		
		WHEN "matches"
			LET l_result = string_matches()		
		WHEN "replaceall"
			LET l_result = string_replaceAll()
		WHEN "replacefirst"
			LET l_result = string_replaceFirst()
		WHEN "substring"
			LET l_result = string_subString()
		WHEN "tolowercase"
			LET l_result = string_toLowerCase()
		WHEN "touppercase"
			LET l_result = string_toUpperCase()
		WHEN "trim"
			LET l_result = string_trim()
		WHEN "trimwhitespace"
			LET l_result = string_trimWhiteSpace()
		WHEN "trimleft"
			LET l_result = string_trimLeft()
		WHEN "trimleftwhitespace"
			LET l_result = string_trimLeftWhiteSpace()
		WHEN "trimright"
			LET l_result = string_trimRight()
		WHEN "trimrightwhitespace"
			LET l_result = string_trimRightWhiteSpace()
		WHEN "split"
			LET l_result = char_split()


	OTHERWISE
			LET l_tmp_str = "Unknown choice: ",  p_choice
			CALL fgl_winmessage("Internal 4GL Error",l_tmp_str,"ERROR")
	END CASE

	DISPLAY m_rec.signature TO signature
	DISPLAY m_rec.s_description TO s_description	
	DISPLAY m_rec.l_description TO l_description	
	DISPLAY m_rec.sample_code TO sample_code	

	DISPLAY l_result TO result 
END FUNCTION
############################################################################
# END FUNCTION menu_process(p_choice STRING)
#
#
############################################################################


############################################################################
# FUNCTION string_append() RETURNS STRING
#
#
############################################################################
FUNCTION string_append() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	
	LET m_rec.signature = "append( str STRING ) RETURNS STRING"
	LET m_rec.s_description = "Concatenates a string."
	LET m_rec.l_description = 
		"This method concatenates a string to the current STRING variable and returns the resulting string. ",
		"The original STRING variable is not modified. ",
		"Appending a NULL will have no effect: the original string is returned."

	LET m_rec.sample_code = 
		"LET l_string = \"The initial text string\"\n",
		"LET r_result = l_string.append(\" with an appended string!\")\n"

	LET l_string = "The initial text string"
	LET r_result = l_string.append(" with an appended string!")

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_append() RETURNS STRING
############################################################################

############################################################################
# FUNCTION string_equals() RETURNS STRING
#
#
############################################################################
FUNCTION string_equals() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	
	LET m_rec.signature = "equals( str STRING ) RETURNS BOOLEAN"
	LET m_rec.s_description = "Compares a string to the content of a string variable."
	LET m_rec.l_description = 
		"equals() is used to compare the current string to another string value.\n", 
		"The method returns an integer value, which is 1 (TRUE) if the compared values are equal and 0 (FALSE) if not:"

	LET m_rec.sample_code = 
		"LET l_string = \"Dog\"\n",
		"IF l_string.equals(\"Dog\") ... RETURNS = TRUE\n",
		"IF l_string.equals(\"DOG\") ... RETURNS = FALSE\n",
		"IF l_string.equals(\"Cat\") ... RETURNS = FALSE\n"

	LET l_string = "Dog"

	LET l_string1 = "Dog"
	LET l_string2 = "DOG"
	LET l_string3 = "Cat"

	IF l_string.equals("Dog") THEN LET l_string1 = l_string1, "= TRUE" ELSE LET l_string1 = l_string1, "= FALSE" END IF
	IF l_string.equals("DOG") THEN LET l_string2 = l_string2, "= TRUE" ELSE LET l_string2 = l_string2, "= FALSE" END IF
	IF l_string.equals("Cat") THEN LET l_string3 = l_string3, "= TRUE" ELSE LET l_string3 = l_string3, "= FALSE" END IF		

	LET r_result = l_string1, "\n", l_string2, "\n", l_string3

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_equals() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_equalsIgnoreCase() RETURNS STRING
#
#
############################################################################
FUNCTION string_equalsIgnoreCase() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	
	LET m_rec.signature = "equalsIgnoreCase( str STRING )	RETURNS BOOLEAN"
	LET m_rec.s_description = "Makes a case-insensitive string comparison."
	LET m_rec.l_description = 
		"equalsIgnoreCase() is similar to equals() but is not case sensitive.\n",
		"It is used to compare the current string to another string value.\n",
		"The method returns an integer value, which is 1 (TRUE) if the compared values are equal and 0 (FALSE) if not:"

	LET m_rec.sample_code = 
		"LET l_string = \"Dog\"\n",
		"IF l_string.equalsIgnoreCase(\"Dog\") ... RETURNS = TRUE\n",
		"IF l_string.equalsIgnoreCase(\"DOG\") ... RETURNS = FALSE\n",
		"IF l_string.equalsIgnoreCase(\"Cat\") ... RETURNS = FALSE\n"

	LET l_string = "Dog"
	LET l_string1 = "Dog"
	LET l_string2 = "DOG"
	LET l_string3 = "Cat"

	IF l_string.equalsIgnoreCase("Dog") THEN LET l_string1 = l_string1, "= TRUE" ELSE LET l_string1 = l_string1, "= FALSE" END IF
	IF l_string.equalsIgnoreCase("DOG") THEN LET l_string2 = l_string2, "= TRUE" ELSE LET l_string2 = l_string2, "= FALSE" END IF
	IF l_string.equalsIgnoreCase("Cat") THEN LET l_string3 = l_string3, "= TRUE" ELSE LET l_string3 = l_string3, "= FALSE" END IF				

	LET r_result = l_string1, "\n", l_string2, "\n", l_string3

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_equalsIgnoreCase() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_getCharAt() RETURNS STRING
#
#
############################################################################
FUNCTION string_getCharAt() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string0 STRING
	DEFINE l_nchar1 NCHAR
	DEFINE l_nchar2 NCHAR
	DEFINE l_nchar3 NCHAR
	
	LET m_rec.signature = "getCharAt( index INTEGER )	RETURNS CHAR(1)"
	LET m_rec.s_description = "Returns the character at the specified position."
	LET m_rec.l_description = 
		"getCharAt() is used to return a character that takes the specified position in the string variable value.\n",
		"The method needs an integer argument that specifies the position.\n",
		"Here, 1 stands for the first character in the buffer sting.\n",
		"If the specified character does not exist, the method returns NULL."

	LET m_rec.sample_code = 
		"LET l_string0 = \"1234567890123456789012345678901234567890123456\"",
		"LET l_string  = \"TestingStrings German:äöüß French:çéâêîôûàèìòù\"",
		"LET l_nchar1 = l_string.getCharAt(5)",
		"LET l_nchar2 = l_string.getCharAt(25)",
		"LET l_nchar3 = l_string.getCharAt(35)"

	LET l_string0 = "1234567890123456789012345678901234567890123456"
	LET l_string  = "TestingStrings German:äöüß French:çéâêîôûàèìòù"

	LET l_nchar1 = l_string.getCharAt(5)
	LET l_nchar2 = l_string.getCharAt(25)
	LET l_nchar3 = l_string.getCharAt(35)

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string.getCharAt(5) = ", l_nchar1, "\n",
	"l_string.getCharAt(25) = ", l_nchar2, "\n",
	"l_string.getCharAt(35) = ", l_nchar3

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_getCharAt() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_getIndexOf() RETURNS STRING
#
#
############################################################################
FUNCTION string_getIndexOf() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string0 STRING
	DEFINE l_pos1 SMALLINT
	DEFINE l_pos2 SMALLINT
	DEFINE l_pos3 SMALLINT
	
	LET m_rec.signature = "getIndexOf(str STRING,startIndex INTEGER ) RETURNS INTEGER"
	LET m_rec.s_description = "Returns the position of a substring."
	LET m_rec.l_description = 
		"getIndexOf() is used to return an integer number which identifies the position of a specified sub-string:",
		"String_var.GetIndexOf(substring, position)",
		"The method needs two arguments:",
		"substring",
		"specifies a quoted string or a variable containing the sub-string that will be searched for",
		"position",
		"is an integer value indicating the position in the buffer string at which the search should begin",
		"To start the search from the very beginning of the buffer string, specify the position argument as 1."

	LET m_rec.sample_code = 
		"LET l_string0 = \"1234567890123456789012345678901234567890123456\"",
		"LET l_string  = \"Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This\"",
		"LET l_nchar1 = l_string.getIndexOf(\"Looking For This\",1)",
		"LET l_nchar2 = l_string.getIndexOf(\"Looking For This\",l_pos1+1)",
		"LET l_nchar3 = l_string.getIndexOf(\"French\",1)"

	LET l_string0 = "123456789012345678901234567890123456789012345678901234567890123456789012345678901"

	LET l_string  = "Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This"

	LET l_pos1 = l_string.getIndexOf("Looking For This",1)
	LET l_pos2 = l_string.getIndexOf("Looking For This",l_pos1+1) #again
	LET l_pos3 = l_string.getIndexOf("French",1)

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string.getIndexOf(\"Looking For This\",1) = ", trim(l_pos1), "\n",
	"l_string.getIndexOf(\"Looking For This\",l_pos1+1) = ", trim(l_pos2), "\n",
	"l_string.getIndexOf(\"French\",1) = ", trim(l_pos3)

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_getIndexOf() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_getLength() RETURNS STRING
#
#
############################################################################
FUNCTION string_getLength() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING		
	DEFINE l_length1 SMALLINT
	DEFINE l_length2 SMALLINT
	DEFINE l_length3 SMALLINT
	
	LET m_rec.signature = "getLength( )	RETURNS INTEGER"
	LET m_rec.s_description = "Returns the length of the string."
	LET m_rec.l_description = 
		"getLength() is used to return an integer value identifying the total number of characters in the string:\n",
		"LET ln = string_val.GetLength()\n",
		"Trailing spaces are also taken into account. \n",
		"If the string is empty, the method will return 0. This method accepts no parameters."

	LET l_string1 = "My short string" #15
	LET l_string2 = "My medium sized German:äöüß string" #24
	LET l_string3 = "My very long German:äöüß and French:çéâêîôûàèìòù" #38		

	LET l_length1 = l_string1.getLength()
	LET l_length2 = l_string2.getLength() 
	LET l_length3 = l_string3.getLength()

	LET m_rec.sample_code = 
	"LET l_string1 = \"My short string #15 \"", "\n",
	"LET l_string2 = \"My medium sized German:äöüß string #24 \"", "\n",
	"LET l_string3 = \"My very long German:äöüß and French:çéâêîôûàèìòù #38 \"", "\n",

	"LET l_length1 = l_string1.getLength()", "\n",
	"LET l_length2 = l_string2.getLength()", "\n",
	"LET l_length3 = l_string3.getLength()", "\n",
	"RETURNS are ", trim(l_length1), " , ", trim(l_length2), " and ", trim(l_length3)

	LET r_result = 
	"l_string1.getLength() RETURNS ", trim(l_length1), "\n",
	"l_string2.getLength() RETURNS ", trim(l_length2), "\n",
	"l_string3.getLength() RETURNS ", trim(l_length3), "\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_getLength() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_matches() RETURNS STRING
#
#
############################################################################
FUNCTION string_matches() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_regexString STRING
	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_ret_boolean1 BOOLEAN
	DEFINE l_ret_boolean2 BOOLEAN
	DEFINE l_ret_boolean3 BOOLEAN

	CALL fgl_winmessage("strings.matches not enabled","The String method matches() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
	{
	LET m_rec.signature = "matches(	regex STRING ) RETURNS BOOLEAN"
	LET m_rec.s_description = "Tests if the string matches a regular expression."
	LET m_rec.l_description = 
		"This method scans the current string and returns TRUE, when the string matches the regular expression passed as parameter.",
		"Note: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of "\\b" or '\\b' )

	LET l_regexString = "^[A-Z0-9+_.-]+@[A-Z0-9.-]+$"
	LET l_string1  = "info@querix.com"
	LET l_string2  = "infoATquerix.com"
	LET l_string3  = "in fo@querix.com"

	LET l_ret_boolean1 = l_string1.matches(l_regexString)
	LET l_ret_boolean2 = l_string2.matches(l_regexString)
	LET l_ret_boolean3 = l_string3.matches(l_regexString)

	LET m_rec.sample_code = 
		"LET l_regexString = \"^[A-Z0-9+_.-]+@[A-Z0-9.-]+$\"\n",
		"LET l_string1  = \"info@querix.com\"\n",
		"LET l_string2  = \"infoATquerix.com\"\n",
		"LET l_string3  = \"in fo@querix.com\"\n",
		"LET l_ret_boolean1 = l_string1.matches(", trim(l_string1), "\n)\n",
		"LET l_ret_boolean2 = l_string2.matches(", trim(l_string2), "\n)\n",
		"LET l_ret_boolean3 = l_string3.matches(", trim(l_string3), "\n)\n",
		trim(l_ret_boolean1), " = l_string1.matches(", trim(l_string1), "\n)\n",
		trim(l_ret_boolean2), " = l_string2.matches(", trim(l_string2), "\n)\n",
		trim(l_ret_boolean3), " = l_string3.matches(", trim(l_string3), "\n)\n"

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string1.matches(\"",trim(l_string1), "\" = ", trim(l_ret_boolean1) , "\n",
	"l_string2.matches(\"",trim(l_string2), "\" = ", trim(l_ret_boolean2) , "\n",
	"l_string3.matches(\"",trim(l_string3), "\" = ", trim(l_ret_boolean3) , "\n"

	RETURN r_result
	}
END FUNCTION
############################################################################
# END FUNCTION string_matches() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_replaceAll() RETURNS STRING
#
#
############################################################################
FUNCTION string_replaceAll() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string0 STRING
	DEFINE l_pos1 SMALLINT
	DEFINE l_pos2 SMALLINT
	DEFINE l_pos3 SMALLINT

	CALL fgl_winmessage("strings.replaceAll() not enabled","The String method replaceAll() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
{
	LET m_rec.signature = "replaceAll( regex STRING, replacement STRING ) RETURNS STRING"
	LET m_rec.s_description = "Replace all substrings matching a regular expression."
	LET m_rec.l_description = 
		"replaceAll( regex STRING, replacement STRING ) RETURNS STRING\n",
		"regex is the regular expression to find substrings that must be replaced. See Regular expression patterns.\n",
		"replacement is the replacement string.\n",
		"Usage\n",
		"This method scans the current string value for substrings matching the regular expression passed as first parameter, replaces all these matching substrings by the replacement string, and returns the new resulting string.\n",
		"The replacement string can reference captured groups with the $num notation, where num is the ordinal position of the group delimited by parentheses in the regular expression, and where $0 represents the whole matching string.\n",
		"Note: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of \"\\b\" or \'\\b\' )\n"

	LET m_rec.sample_code = 
		"LET l_string  = \"Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This\"",
		"LET l_nchar1 = l_string.replaceAll(\"Looking For This\",1)",
		"LET l_nchar2 = l_string.replaceAll(\"Looking For This\",l_pos1+1)",
		"LET l_nchar3 = l_string.replaceAll(\"French\",1)"

	LET l_string0 = "123456789012345678901234567890123456789012345678901234567890123456789012345678901"

	LET l_string  = "Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This"

	LET l_pos1 = l_string.replaceAll("Looking For This",1)
	LET l_pos2 = l_string.replaceAll("Looking For This",l_pos1+1) #again
	LET l_pos3 = l_string.replaceAll("French",1)

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string.replaceAll(\"Looking For This\",1) = ", trim(l_pos1), "\n",
	"l_string.replaceAll(\"Looking For This\",l_pos1+1) = ", trim(l_pos2), "\n",
	"l_string.replaceAll(\"French\",1) = ", trim(l_pos3)

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_replaceAll() RETURNS STRING
############################################################################




############################################################################
# FUNCTION string_replaceFirst() RETURNS STRING
#
#
############################################################################
FUNCTION string_replaceFirst() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string0 STRING
	DEFINE l_pos1 SMALLINT
	DEFINE l_pos2 SMALLINT
	DEFINE l_pos3 SMALLINT

	CALL fgl_winmessage("strings.replaceFirst() not enabled","The String method replaceFirst() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
{
	LET m_rec.signature = "replaceFirst( regex STRING, replacement STRING ) RETURNS STRING"
	LET m_rec.s_description = "Replace a substring matching a regular expression."
	LET m_rec.l_description = 
	"replaceFirst( regex STRING, replacement STRING ) RETURNS STRING", "\n"
	"Replace a substring matching a regular expression.", "\n"
	"regex is the regular expression to find substrings that must be replaced. See Regular expression patterns.", "\n"
	"replacement is the replacement string.", "\n"
	"Usage", "\n"
	"This method scans the current string value for the first substring matching the regular expression passed as first parameter, replaces the matching substring by the replacement string, and returns the new resulting string.", "\n"
	"The replacement string can reference captured groups with the $num notation, where num is the ordinal position of the group delimited by parentheses in the regular expression, and where $0 represents the whole matching string.", "\n"
	"Note: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of "\\b" or '\\b' )ote: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of \"\\b\" or \'\\b\' )\n"", "\n"

	LET m_rec.sample_code = 
		"LET l_string  = \"Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This\"",
		"LET l_nchar1 = l_string.replaceFirst(\"Looking For This\",1)",
		"LET l_nchar2 = l_string.replaceFirst(\"Looking For This\",l_pos1+1)",
		"LET l_nchar3 = l_string.replaceFirst(\"French\",1)"

	LET l_string0 = "123456789012345678901234567890123456789012345678901234567890123456789012345678901"

	LET l_string  = "Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This"

	LET l_pos1 = l_string.replaceFirst("Looking For This",1)
	LET l_pos2 = l_string.replaceFirst("Looking For This",l_pos1+1) #again
	LET l_pos3 = l_string.replaceFirst("French",1)

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string.replaceFirst(\"Looking For This\",1) = ", trim(l_pos1), "\n",
	"l_string.replaceFirst(\"Looking For This\",l_pos1+1) = ", trim(l_pos2), "\n",
	"l_string.replaceFirst(\"French\",1) = ", trim(l_pos3)

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_replaceFirst() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_subString() RETURNS STRING
#
#
############################################################################
FUNCTION string_subString() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING
	DEFINE l_string0 STRING
	DEFINE l_pos1 SMALLINT
	DEFINE l_pos2 SMALLINT
	DEFINE l_pos3 SMALLINT

	CALL fgl_winmessage("strings.subString() not enabled","The String method subString() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
	
{
	LET m_rec.signature = "subString( regex STRING, replacement STRING ) RETURNS STRING"
	LET m_rec.s_description = "Replace a substring matching a regular expression."
	LET m_rec.l_description = 
	"subString( regex STRING, replacement STRING ) RETURNS STRING", "\n"
	"Replace a substring matching a regular expression.", "\n"
	"regex is the regular expression to find substrings that must be replaced. See Regular expression patterns.", "\n"
	"replacement is the replacement string.", "\n"
	"Usage", "\n"
	"This method scans the current string value for the first substring matching the regular expression passed as first parameter, replaces the matching substring by the replacement string, and returns the new resulting string.", "\n"
	"The replacement string can reference captured groups with the $num notation, where num is the ordinal position of the group delimited by parentheses in the regular expression, and where $0 represents the whole matching string.", "\n"
	"Note: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of "\\b" or '\\b' )ote: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\b` instead of \"\\b\" or \'\\b\' )\n"", "\n"

	LET m_rec.sample_code = 
		"LET l_string  = \"Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This\"",
		"LET l_nchar1 = l_string.subString(\"Looking For This\",1)",
		"LET l_nchar2 = l_string.subString(\"Looking For This\",l_pos1+1)",
		"LET l_nchar3 = l_string.subString(\"French\",1)"

	LET l_string0 = "123456789012345678901234567890123456789012345678901234567890123456789012345678901"

	LET l_string  = "Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This"

	LET l_pos1 = l_string.subString("Looking For This",1)
	LET l_pos2 = l_string.subString("Looking For This",l_pos1+1) #again
	LET l_pos3 = l_string.subString("French",1)

	LET r_result = l_string0 , "\n", l_string, "\n",
	"l_string.subString(\"Looking For This\",1) = ", trim(l_pos1), "\n",
	"l_string.subString(\"Looking For This\",l_pos1+1) = ", trim(l_pos2), "\n",
	"l_string.subString(\"French\",1) = ", trim(l_pos3)

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_subString() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_toLowerCase() RETURNS STRING
#
#
############################################################################
FUNCTION string_toLowerCase() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	LET m_rec.signature = "toLowerCase( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the string converted to lower case."
	LET m_rec.l_description = 
		"toLowerCase() is used to convert the characters of the string value to the lower case.", "\n",
		"The lower case characters, symbols and numbers will not be converted:", "\n",
		"DISPLAY string_val.ToLowerCase()", "\n",
		"The method does not need any arguments and does not return anything.", "\n",
		"If the string is NULL, the method returns NULL"


	LET l_string1  = "Querix Lycia 4GL - ÖÄÜöäüß - çéâêîôûàèìòù"
	LET l_string2  = "querix lycia 4gl - ÖÄÜöäüß - çéâêîôûàèìòù"
	LET l_string3  = "QUERIX LYCIA 4GL - ÖÄÜöäüß - çéâêîôûàèìòù"

	LET l_string1r = l_string1.toLowerCase()
	LET l_string2r = l_string2.toLowerCase()
	LET l_string3r = l_string3.toLowerCase()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"Querix Lycia 4GL - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string2  = \"querix lycia 4gl - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string3  = \"QUERIX LYCIA 4GL - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string1r = l_string1.toLowerCase()", "\n",
		"LET l_string2r = l_string2.toLowerCase()", "\n",
		"LET l_string3r = l_string3.toLowerCase()", "\n",
		"l_string1r =", trim(l_string1r), "\n",
		"l_string2r =", trim(l_string2r), "\n",
		"l_string3r =", trim(l_string3r), "\n"

	LET r_result = 
		"l_string1.toLowerCase() RETURNS ", trim(l_string1r), " # l_string1=", trim(l_string1),  "\n",
		"l_string2.toLowerCase() RETURNS ", trim(l_string2r), " # l_string2=", trim(l_string2),  "\n",
		"l_string3.toLowerCase() RETURNS ", trim(l_string3r), " # l_string3=", trim(l_string3),  "\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_toLowerCase() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_toUpperCase() RETURNS STRING
#
#
############################################################################
FUNCTION string_toUpperCase() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	LET m_rec.signature = "toUpperCase( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the string converted to lower case."
	LET m_rec.l_description = 
		"toUpperCase() is used to convert the characters of the string value to the upper case.", "\n",
		"The upper case characters, symbols and numbers will not be converted:", "\n",
		"DISPLAY string_val.toUpperCase()", "\n",
		"The method does not need any arguments and returns the converted string.", "\n",
		"If the string is NULL, the method returns NULL"


	LET l_string1  = "Querix Lycia 4GL - ÖÄÜöäüß - çéâêîôûàèìòù"
	LET l_string2  = "querix lycia 4gl - ÖÄÜöäüß - çéâêîôûàèìòù"
	LET l_string3  = "QUERIX LYCIA 4GL - ÖÄÜöäüß - çéâêîôûàèìòù"

	LET l_string1r = l_string1.toUpperCase()
	LET l_string2r = l_string2.toUpperCase()
	LET l_string3r = l_string3.toUpperCase()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"Querix Lycia 4GL - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string2  = \"querix lycia 4gl - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string3  = \"QUERIX LYCIA 4GL - ÖÄÜöäüß - çéâêîôûàèìòù\"", "\n",
		"LET l_string1r = l_string1.toUpperCase()", "\n",
		"LET l_string2r = l_string2.toUpperCase()", "\n",
		"LET l_string3r = l_string3.toUpperCase()", "\n",
		"l_string1r =", trim(l_string1r), "\n",
		"l_string2r =", trim(l_string2r), "\n",
		"l_string3r =", trim(l_string3r), "\n"

	LET r_result = 
		"l_string1.toUpperCase() RETURNS ", trim(l_string1r), " # l_string1=", trim(l_string1),  "\n",
		"l_string2.toUpperCase() RETURNS ", trim(l_string2r), " # l_string2=", trim(l_string2),  "\n",
		"l_string3.toUpperCase() RETURNS ", trim(l_string3r), " # l_string3=", trim(l_string3),  "\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_toUpperCase() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_trim() RETURNS STRING
#
#
############################################################################
FUNCTION string_trim() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	LET m_rec.signature = "trim( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string with removed leading and trailing spaces (blank spaces ASCII 32)"
	LET m_rec.l_description = 
		"The trim() method removes the leading and trailing blank space characters of the current STRING variable and returns a new string.", "\n",
		"The method removes only blank space (ASCII(32)) characters. ", "\n",
		"Characters tab (\t), newline (\\n), carriage-return (\\r) and form-feed (\\f) are not removed.", "\n",
		"If the original STRING variable is NULL, the result will be NULL."

	LET l_string1  = "   Querix (UK) Ltd   -   Lycia 4GL environment   "
	LET l_string2  = "    Querix (UK) Ltd   -   Lycia 4GL environment    "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trim()
	LET l_string2r = l_string2.trim()
	LET l_string3r = l_string3.trim()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"   Querix (UK) Ltd   -   Lycia 4GL environment   \"", "\n",
		"LET l_string2  = \"    Querix (UK) Ltd   -   Lycia 4GL environment    \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trim()", "\n",
		"LET l_string2r = l_string2.trim()", "\n",
		"LET l_string3r = l_string3.trim()", "\n",
		"l_string1r =", trim(l_string1r), "\n",
		"l_string2r =", trim(l_string2r), "\n",
		"l_string3r =", trim(l_string3r), "\n"

	LET r_result = 
		"l_string1.trim() RETURNS -->", trim(l_string1r), "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trim() RETURNS -->", trim(l_string2r), "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trim() RETURNS -->", trim(l_string3r), "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_trim() RETURNS STRING
############################################################################




############################################################################
# FUNCTION string_trimWhiteSpace() RETURNS STRING
#
#
############################################################################
FUNCTION string_trimWhiteSpace() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	CALL fgl_winmessage("strings.trimWhiteSpace() not enabled","The String method trimWhiteSpace() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
{
	LET m_rec.signature = "trimWhiteSpace( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string but with leading and trailing whitespace characters removed."
	LET m_rec.l_description = 
	"trimWhiteSpace( )	RETURNS STRING", "\n",
	"Returns the same string but with leading and trailing whitespace characters removed.", "\n",
	"The trimWhiteSpace() method removes the leading and trailing whitespace characters of the current STRING variable and returns a new string.", "\n",
	"The method considers as whitespace characters all characters less than or equal to blank space (ASCII(32)). This includes tab (\\t), newline (\\n), carriage-return (\\r) and form-feed (\\f).", "\n",
	"If the original STRING variable is NULL, the result will be NULL.", "\n"

	LET l_string1  = " \n   Querix (UK) Ltd   -   Lycia 4GL environment \n   "
	LET l_string2  = "  \n   Querix (UK) Ltd   -   Lycia 4GL environment  \n   "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trimWhiteSpace()
	LET l_string2r = l_string2.trimWhiteSpace()
	LET l_string3r = l_string3.trimWhiteSpace()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \" \\n   Querix (UK) Ltd   -   Lycia 4GL environment \\n   \"", "\n",
		"LET l_string2  = \"  \\n   Querix (UK) Ltd   -   Lycia 4GL environment  \\n   \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trimWhiteSpace()", "\n",
		"LET l_string2r = l_string2.trimWhiteSpace()", "\n",
		"LET l_string3r = l_string3.trimWhiteSpace()", "\n",
		"l_string1r =", trimWhiteSpace(l_string1r), "\n",
		"l_string2r =", trimWhiteSpace(l_string2r), "\n",
		"l_string3r =", trimWhiteSpace(l_string3r), "\n"

	LET r_result = 
		"l_string1.trimWhiteSpace() RETURNS -->", trimWhiteSpace(l_string1r), "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trimWhiteSpace() RETURNS -->", trimWhiteSpace(l_string2r), "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trimWhiteSpace() RETURNS -->", trimWhiteSpace(l_string3r), "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_trimWhiteSpace() RETURNS STRING
############################################################################


############################################################################
# FUNCTION string_trimLeft() RETURNS STRING
#
#
############################################################################
FUNCTION string_trimLeft() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	LET m_rec.signature = "trimLeft( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string with removed leading spaces (blank spaces ASCII 32)"
	LET m_rec.l_description = 
		"The trimLeft() method removes only the leading blank space characters (leaving the trailing spaces intact) of the current STRING variable and returns a new string.", "\n",
		"The method removes only blank space (ASCII(32)) characters. ", "\n",
		"Characters tab (\t), newline (\\n), carriage-return (\\r) and form-feed (\\f) are not removed.", "\n",
		"If the original STRING variable is NULL, the result will be NULL."

	LET l_string1  = "   Querix (UK) Ltd   -   Lycia 4GL environment   "
	LET l_string2  = "    Querix (UK) Ltd   -   Lycia 4GL environment    "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trimLeft()
	LET l_string2r = l_string2.trimLeft()
	LET l_string3r = l_string3.trimLeft()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"   Querix (UK) Ltd   -   Lycia 4GL environment   \"", "\n",
		"LET l_string2  = \"    Querix (UK) Ltd   -   Lycia 4GL environment    \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trimLeft()", "\n",
		"LET l_string2r = l_string2.trimLeft()", "\n",
		"LET l_string3r = l_string3.trimLeft()", "\n",
		"l_string1r =", l_string1r, "\n",
		"l_string2r =", l_string2r, "\n",
		"l_string3r =", l_string3r, "\n"

	LET r_result = 
		"l_string1.trimLeft() RETURNS -->", l_string1r, "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trimLeft() RETURNS -->", l_string2r, "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trimLeft() RETURNS -->", l_string3r, "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_trimLeft() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_trimLeftWhiteSpace() RETURNS STRING
#
#
############################################################################
FUNCTION string_trimLeftWhiteSpace() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	CALL fgl_winmessage("strings.trimWhiteSpace() not enabled","The String method trimWhiteSpace() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
{
	LET m_rec.signature = "trimLeftWhiteSpace( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string with removed leading spaces (blank spaces ASCII 32)"
	LET m_rec.l_description = 
		"The trimLeftWhiteSpace() method removes only the leading white space characters (leaving the trailing spaces intact) of the current STRING variable and returns a new string.", "\n",
		"The method removes blank space (ASCII(32)) characters. ", "\n",
		"AND Characters tab (\t), newline (\\n), carriage-return (\\r) and form-feed (\\f) are not removed.", "\n",
		"If the original STRING variable is NULL, the result will be NULL."

	LET l_string1  = "   Querix (UK) Ltd   -   Lycia 4GL environment   "
	LET l_string2  = "    Querix (UK) Ltd   -   Lycia 4GL environment    "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trimLeftWhiteSpace()
	LET l_string2r = l_string2.trimLeftWhiteSpace()
	LET l_string3r = l_string3.trimLeftWhiteSpace()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"   Querix (UK) Ltd   -   Lycia 4GL environment   \"", "\n",
		"LET l_string2  = \"    Querix (UK) Ltd   -   Lycia 4GL environment    \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trimLeftWhiteSpace()", "\n",
		"LET l_string2r = l_string2.trimLeftWhiteSpace()", "\n",
		"LET l_string3r = l_string3.trimLeftWhiteSpace()", "\n",
		"l_string1r =", trimLeftWhiteSpace(l_string1r), "\n",
		"l_string2r =", trimLeftWhiteSpace(l_string2r), "\n",
		"l_string3r =", trimLeftWhiteSpace(l_string3r), "\n"

	LET r_result = 
		"l_string1.trimLeftWhiteSpace() RETURNS -->", trimLeftWhiteSpace(l_string1r), "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trimLeftWhiteSpace() RETURNS -->", trimLeftWhiteSpace(l_string2r), "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trimLeftWhiteSpace() RETURNS -->", trimLeftWhiteSpace(l_string3r), "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_trimLeftWhiteSpace() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_trimRight() RETURNS STRING
#
#
############################################################################
FUNCTION string_trimRight() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	LET m_rec.signature = "trimRight( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string with removed trailing spaces (blank spaces ASCII 32)"
	LET m_rec.l_description = 
		"The trimRight() method removes only the trailing blank space characters (leaving the trailing spaces intact) of the current STRING variable and returns a new string.", "\n",
		"The method removes only blank space (ASCII(32)) characters. ", "\n",
		"Characters tab (\t), newline (\\n), carriage-return (\\r) and form-feed (\\f) are not removed.", "\n",
		"If the original STRING variable is NULL, the result will be NULL."

	LET l_string1  = "   Querix (UK) Ltd   -   Lycia 4GL environment   "
	LET l_string2  = "    Querix (UK) Ltd   -   Lycia 4GL environment    "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trimRight()
	LET l_string2r = l_string2.trimRight()
	LET l_string3r = l_string3.trimRight()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"   Querix (UK) Ltd   -   Lycia 4GL environment   \"", "\n",
		"LET l_string2  = \"    Querix (UK) Ltd   -   Lycia 4GL environment    \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trimRight()", "\n",
		"LET l_string2r = l_string2.trimRight()", "\n",
		"LET l_string3r = l_string3.trimRight()", "\n",
		"# l_string1r =", l_string1r, "\n",
		"# l_string2r =", l_string2r, "\n",
		"# l_string3r =", l_string3r, "\n"

	LET r_result = 
		"l_string1.trimRight() RETURNS -->", l_string1r, "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trimRight() RETURNS -->", l_string2r, "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trimRight() RETURNS -->", l_string3r, "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION string_trimRight() RETURNS STRING
############################################################################



############################################################################
# FUNCTION string_trimRightWhiteSpace() RETURNS STRING
#
#
############################################################################
FUNCTION string_trimRightWhiteSpace() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_string STRING

	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_string1r STRING
	DEFINE l_string2r STRING
	DEFINE l_string3r STRING

	CALL fgl_winmessage("strings.trimRightWhiteSpace() not enabled","The String method trimRightWhiteSpace() is currently not enabled","info")
	LET m_rec.signature = ""
	LET m_rec.s_description = ""
	LET m_rec.l_description = ""
	RETURN NULL
{
	LET m_rec.signature = "trimRightWhiteSpace( )	RETURNS STRING"
	LET m_rec.s_description = "Returns the same string with removed trailing spaces (blank spaces ASCII 32)"
	LET m_rec.l_description = 
		"The trimRightWhiteSpace() method removes only the trailing white space characters (leaving the trailing spaces intact) of the current STRING variable and returns a new string.", "\n",
		"The method removes blank space (ASCII(32)) characters. ", "\n",
		"AND Characters tab (\t), newline (\\n), carriage-return (\\r) and form-feed (\\f) are not removed.", "\n",
		"If the original STRING variable is NULL, the result will be NULL."

	LET l_string1  = "   Querix (UK) Ltd   -   Lycia 4GL environment   "
	LET l_string2  = "    Querix (UK) Ltd   -   Lycia 4GL environment    "
	LET l_string3  = "   \n   Querix (UK) Ltd   -   Lycia 4GL environment   \n   "

	LET l_string1r = l_string1.trimRightWhiteSpace()
	LET l_string2r = l_string2.trimRightWhiteSpace()
	LET l_string3r = l_string3.trimRightWhiteSpace()
		

	LET m_rec.sample_code = 
		"LET l_string1  = \"   Querix (UK) Ltd   -   Lycia 4GL environment   \"", "\n",
		"LET l_string2  = \"    Querix (UK) Ltd   -   Lycia 4GL environment    \"", "\n",
		"LET l_string3  = \"   \\n   Querix (UK) Ltd   -   Lycia 4GL environment   \\n   \"", "\n",
		"LET l_string1r = l_string1.trimRightWhiteSpace()", "\n",
		"LET l_string2r = l_string2.trimRightWhiteSpace()", "\n",
		"LET l_string3r = l_string3.trimRightWhiteSpace()", "\n",
		"l_string1r =", trimRightWhiteSpace(l_string1r), "\n",
		"l_string2r =", trimRightWhiteSpace(l_string2r), "\n",
		"l_string3r =", trimRightWhiteSpace(l_string3r), "\n"

	LET r_result = 
		"l_string1.trimRightWhiteSpace() RETURNS -->", trimRightWhiteSpace(l_string1r), "<-- # l_string1 = -->", l_string1,  "<--\n",
		"l_string2.trimRightWhiteSpace() RETURNS -->", trimRightWhiteSpace(l_string2r), "<-- # l_string2 = -->", l_string2,  "<--\n",
		"l_string3.trimRightWhiteSpace() RETURNS -->", trimRightWhiteSpace(l_string3r), "<-- # l_string3 = -->", l_string3,  "<--\n"

	RETURN r_result
}
END FUNCTION
############################################################################
# END FUNCTION string_trimRightWhiteSpace() RETURNS STRING
############################################################################


############################################################################
# FUNCTION char_split() RETURNS STRING
#
#
############################################################################
FUNCTION char_split() RETURNS STRING
	DEFINE r_result STRING
	DEFINE l_ret_output STRING
	DEFINE l_string1 STRING
	DEFINE l_string2 STRING
	DEFINE l_string3 STRING
	DEFINE l_arg1 STRING
	DEFINE l_arg2 STRING
	DEFINE l_arg3 STRING

	DEFINE l_pos1 SMALLINT
	DEFINE l_pos2 SMALLINT
	DEFINE l_pos3 SMALLINT
	DEFINE l_arr_rec1 DYNAMIC ARRAY OF STRING
	DEFINE l_arr_rec2 DYNAMIC ARRAY OF STRING
	DEFINE l_arr_rec3 DYNAMIC ARRAY OF STRING
	DEFINE i INT
	DEFINE RegEx util.RegEx
	DEFINE match util.MATCH_RESULTS

	LET m_rec.signature = "split( regex STRING, replacement STRING ) RETURNS STRING"
	LET m_rec.s_description = "Replace a split matching a regular expression."
	LET m_rec.l_description = 
	"split( regex STRING, replacement STRING ) RETURNS STRING", "\n",
	"Replace a split matching a regular expression.", "\n",
	"regex is the regular expression to find splits that must be replaced. See Regular expression patterns.", "\n",
	"replacement is the replacement string.", "\n",
	"Usage", "\n",
	"This method scans the current string value for the first split matching the regular expression passed as first parameter, replaces the matching split by the replacement string, and returns the new resulting string.", "\n",
	"The replacement string can reference captured groups with the $num notation, where num is the ordinal position of the group delimited by parentheses in the regular expression, and where $0 represents the whole matching string.", "\n",
	"Note: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( `\\b` instead of \"\\b\" or \'\\b\' )ote: When using single or double quoted string literals, backslash is interpreted as escape character. Consider using back quotes as delimiters for regular expressions strings, and use backslash characters directly as required by the regexp syntax ( \`\b\` instead of \"\\b\" or \'\\b\' )\n"

	#Example 1
	#Use split() as a tokenizer/csv file handler
	#Argument is the separator symbol
	LET l_arg1 = ","
	LET l_string1="AA,BB,CC,DD"
	LET l_arr_rec1 = l_string1.split(",")	

	LET l_ret_output = l_ret_output, "---------- \nExample 1 " 
	LET l_ret_output = l_ret_output,"\n", "LET l_string1=\"", l_string1 ,"\""  
	LET l_ret_output = l_ret_output,"\n", "LET l_arr_rec1 = l_string1.split(\"", trim(l_arg1), "\")"  

	FOR i=1 TO l_arr_rec1.getLength()
			DISPLAY l_arr_rec1[i]
			LET l_ret_output = l_ret_output, "\n", "l_arr_rec1[", trim(i), "] = ", l_arr_rec1[i]
	END FOR

	#Example 2	
	#Use split() as a tokenizer/csv file handler
	#Argument is an expression
	LET l_string2 = "aa bb;cc:dd"
	LET l_arg2 = "[ ;:]"
	LET l_ret_output = l_ret_output,"\n", "---------- \nExample 2 - Expression String ", l_arg2
	LET l_ret_output = l_ret_output,"\n", "LET l_string2=\"", l_string2 ,"\"" 
	LET l_ret_output = l_ret_output,"\n", "LET l_arr_rec2 = l_string2.split(\"", trim(l_arg2), "\")"

	LET l_arr_rec2 = l_string2.split(l_arg2)
	FOR i=1 TO l_arr_rec2.getLength()
			LET l_ret_output = l_ret_output, "\n", "l_arr_rec2[", trim(i), "] = ", l_arr_rec2[i]
	END FOR

	#Example 3
	#Use split() as a tokenizer/csv file handler
	#Argument is an expression
	LET l_string3 = "aa bb;cc:dd"	
	LET regex = /[ ;:]/
	#LET regex = util.Regex.compile('[ ;:]')
	#LET regex = util.Regexp.compile('^[ABC]XX$')
	
	#LET l_arg3 = "[ ;:]"
	LET l_ret_output = l_ret_output,"\n", "---------- \nExample 3 = REGEX object argument "
	LET l_ret_output = l_ret_output,"\n", "LET regex = /[ ;:]/"
	LET l_ret_output = l_ret_output,"\n", "LET l_arr_rec3 = l_string3.split(\"", trim(l_arg3), "\")" 

	LET l_arr_rec3 = l_string3.split(regex)
	FOR i=1 TO l_arr_rec3.getLength()
		LET l_ret_output = l_ret_output, "\n", "l_arr_rec3[", trim(i), "] = ", l_arr_rec3[i]
	END FOR


	LET m_rec.sample_code = 
		"LET l_string  = \"Testing Strings Looking For This German:äöüß French:çéâêîôûàèìòù Looking For This\"",
		"LET l_nchar1 = l_string.split(\"Looking For This\",1)",
		"LET l_nchar2 = l_string.split(\"Looking For This\",l_pos1+1)",
		"LET l_nchar3 = l_string.split(\"French\",1)"


#	LET l_pos1 = l_string.split("Looking For This",1)
#	LET l_pos2 = l_string.split("Looking For This",l_pos1+1) #again
#	LET l_pos3 = l_string.split("French",1)
#
#	LET r_result = l_string0 , "\n", l_string, "\n",
#	"l_string.split(\"Looking For This\",1) = ", trim(l_pos1), "\n",
#	"l_string.split(\"Looking For This\",l_pos1+1) = ", trim(l_pos2), "\n",
#	"l_string.split(\"French\",1) = ", trim(l_pos3)

	LET r_result = l_ret_output
	RETURN r_result

END FUNCTION
############################################################################
# END FUNCTION char_split() RETURNS STRING
############################################################################