################################################################################
# Module Scope Variables
################################################################################
DEFINE modu_log STRING
DEFINE modu_log_line SMALLINT

################################################################################
# MAIN
#
#
################################################################################
MAIN
	DEFINE l_drag_index, l_drop_index, i INT
	DEFINE l_drag_source STRING 
	DEFINE l_rec_drag_value RECORD
			pic  STRING,
			item STRING
		END RECORD        
	DEFINE l_arr_rec_left DYNAMIC ARRAY OF RECORD
			pic2  STRING,
			item2 STRING
		END RECORD
	DEFINE l_arr_rec_right DYNAMIC ARRAY OF RECORD
			pic3  STRING,
			item3 STRING
		END RECORD
	DEFINE l_arr_rec_warehouse DYNAMIC ARRAY OF RECORD
    	pic1  STRING,
    	item1 STRING
		END RECORD
             
	DEFINE l_ui_dnd ui.DragDrop

	CONSTANT S_LEFT="sr_left"
	CONSTANT S_RIGHT="sr_right"
	CONSTANT S_WRHS="sr_wrhs"
     
	OPTIONS INPUT WRAP
	
	CALL ui_init()

	OPEN window w_drag_and_drop with FORM "form/sta_display_array_drag_and_drop"
	CALL ui.Interface.setText("Drag&Drop")
     
	FOR i = 1 TO 10
		IF i mod 2 =0 THEN
			LET l_arr_rec_warehouse[i].pic1 = "image/cargo1.png"
			LET l_arr_rec_warehouse[i].item1 = "cargo",trim(i)
		ELSE
			LET l_arr_rec_warehouse[i].pic1 = "image/cargo2.png"
			LET l_arr_rec_warehouse[i].item1 = "cargo",trim(i)
		END IF
	END FOR

	INITIALIZE l_drag_index TO NULL
     
	CALL log_append("Program starts...",NULL)

	DIALOG ATTRIBUTE(UNBUFFERED) #-- DIALOG ------------------------------

		DISPLAY ARRAY l_arr_rec_warehouse TO sr_wrhs.* #-------------------------------
			ON DRAG_START(l_ui_dnd)
				CALL log_append("EVENT: DRAG START (left)","-----------------")
				LET l_drag_source = S_WRHS
				LET l_drag_index = arr_curr()
				LET l_rec_drag_value.* = l_arr_rec_warehouse[l_drag_index].*
             
			ON DRAG_FINISHED(l_ui_dnd)
				CALL log_append("EVENT: DRAG_FINISHED","-----------------")
				INITIALIZE l_drag_source TO NULL
         
			ON DRAG_OVER(l_ui_dnd)
				CALL log_append("EVENT: DRAG_OVER (left)","-----------------")         

			ON DRAG_ENTER(l_ui_dnd)
				CALL log_append("EVENT: DRAG_ENTER (left)","-----------------")         

				IF l_drag_source IS NULL THEN
					CALL l_ui_dnd.setOperation(NULL)
				END IF

			ON DROP(l_ui_dnd)
				CALL log_append("EVENT: DROP (left)","-----------------")         

				IF l_drag_source == S_WRHS THEN
					CALL l_ui_dnd.dropInternal()
				ELSE
					LET l_drop_index = l_ui_dnd.getLocationRow()
					CALL DIALOG.insertRow(S_WRHS, l_drop_index)
					CALL DIALOG.setCurrentRow(S_WRHS, l_drop_index)
					LET   l_arr_rec_warehouse[l_drop_index].* = l_rec_drag_value.* 
			
					CALL log_append("DELETE FROM:",l_drag_source)         
						
					CALL DIALOG.deleteRow(l_drag_source, l_drag_index)
				END IF

		END DISPLAY

		DISPLAY ARRAY l_arr_rec_left TO sr_left.* #-------------------------------
			ON DRAG_START(l_ui_dnd)
				CALL log_append("EVENT: DRAG_START (left)","-----------------") 
			      
				LET l_drag_source = S_LEFT
				LET l_drag_index = arr_curr()
        LET l_rec_drag_value.* = l_arr_rec_left[l_drag_index].*

			ON DRAG_FINISHED(l_ui_dnd)
				CALL log_append("EVENT: DRAG_FINISHED (left)","-----------------") 
				INITIALIZE l_drag_source TO NULL
			 
			ON DRAG_OVER(l_ui_dnd)
				CALL log_append("EVENT: DRAG_OVER (left)","-----------------") 
			
			ON DRAG_ENTER(l_ui_dnd)
				CALL log_append("EVENT: DRAG_ENTER (left)","-----------------") 
			
				IF l_drag_source IS NULL THEN
					CALL l_ui_dnd.setOperation(NULL)
				END IF

			ON DROP(l_ui_dnd)
				CALL log_append("EVENT: DROP (left)","-----------------") 

				IF l_drag_source == S_LEFT THEN
					CALL l_ui_dnd.dropInternal()
				ELSE
					LET l_drop_index = l_ui_dnd.getLocationRow()
					CALL DIALOG.insertRow(S_LEFT, l_drop_index)
					CALL DIALOG.setCurrentRow(S_LEFT, l_drop_index)
					LET l_arr_rec_left[l_drop_index].* = l_rec_drag_value.*
					CALL log_append("DELETE FROM:",l_drag_source) 
		
					CALL DIALOG.deleteRow(l_drag_source, l_drag_index)
				END IF

			END DISPLAY


			DISPLAY ARRAY l_arr_rec_right TO sr_right.* #----------------------------------------------------

				ON DRAG_START(l_ui_dnd)
					CALL log_append("EVENT: DRAG START (right)","-----------------")		
					LET l_drag_source = S_RIGHT
					LET l_drag_index = arr_curr()
					LET l_rec_drag_value.* = l_arr_rec_right[l_drag_index].*

				ON DRAG_FINISHED(l_ui_dnd)
					CALL log_append("EVENT: DRAG FINISHED","-----------------")				

					DISPLAY "Right drag finished"
					INITIALIZE l_drag_source TO NULL

				ON DRAG_OVER(l_ui_dnd)
					CALL log_append("EVENT: DRAG OVER (Right)","-----------------")			

				ON DRAG_ENTER(l_ui_dnd)
					CALL log_append("EVENT: DRAG ENTER (Right)","-----------------")

					IF l_drag_source IS NULL THEN
						CALL l_ui_dnd.setOperation(NULL)
					END IF
             
				ON DROP(l_ui_dnd)
					CALL log_append("EVENT: DROP (right)","-----------------")

					IF l_drag_source == S_RIGHT THEN
						CALL l_ui_dnd.dropInternal()
					ELSE
						LET l_drop_index = l_ui_dnd.getLocationRow()
						CALL DIALOG.insertRow(S_RIGHT, l_drop_index)
						CALL DIALOG.setCurrentRow(S_RIGHT, l_drop_index)
						LET l_arr_rec_right[l_drop_index].* = l_rec_drag_value.*
							
						CALL log_append("DELETE FROM:",l_drag_source)
						CALL DIALOG.deleteRow(l_drag_source, l_drag_index)
					END IF
				
		END DISPLAY #-----------------------------------------------------------------

		BEFORE DIALOG
			CALL dialog.setActionHidden("firstrow",TRUE)
			CALL dialog.setActionHidden("lastrow",TRUE)
			
		ON ACTION CANCEL
			EXIT DIALOG

		ON ACTION ACCEPT
			EXIT DIALOG


    ON ACTION "HELP"
    	CALL onlineHelp("drag-and-drop",NULL)
		    	
	END DIALOG
END MAIN
################################################################################
# END MAIN
################################################################################


################################################################################
# FUNCTION log_append(p_s1,p_s2)
#
#
################################################################################
FUNCTION log_append(p_s1,p_s2)
 	DEFINE p_S1 STRING
 	DEFINE p_S2 STRING

	LET modu_log_line = modu_log_line + 1
	LET modu_log = modu_log_line USING "<<<<&", " ", trim(p_s1), " - ", trim(p_s2), " \n", modu_log
	DISPLAY modu_log TO log
 
END FUNCTION
################################################################################
# END FUNCTION log_append(p_s1,p_s2)
################################################################################