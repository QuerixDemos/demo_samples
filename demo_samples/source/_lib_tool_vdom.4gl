###########################################################################
# Module Scope Variables
###########################################################################
DEFINE ml_vdom SMALLINT  --vdom = 1 or 0
DEFINE ml_vdom_set BOOLEAN --true = variable initialized/set false=not initialized


###########################################################################
# FUNCTION get_vDom()
#
# Accessor Method for ml_vdom 
###########################################################################
FUNCTION get_vDom()
	
	IF ml_vdom_set = FALSE THEN
		IF fgl_getenv("VDOM")	 IS NULL THEN
			#CALL fgl_winmessage("VDOM not specified","You need to specify the value of VDOM\nIn the environment or in the URL!","error")
			CALL set_vDom(0)		
		ELSE
			CALL set_vDom(fgl_getenv("VDOM"))	
		END IF
	END IF

	RETURN ml_vdom
END FUNCTION

###########################################################################
# FUNCTION get_vdom_set()
#
# Accessor Method for ml_vdom_set  = status, if vdom was set
###########################################################################
FUNCTION get_vdom_set()
		RETURN ml_vdom_set
END FUNCTION


###########################################################################
# FUNCTION set_vDom(p_vDom)
#
# Accessor Method for ml_vdom 
###########################################################################
FUNCTION set_vDom(p_vDom)
	DEFINE p_vDom SMALLINT

	IF p_vDom = 0 OR p_vDom = 1 THEN
		LET ml_vdom = p_vDom
		CALL fgl_setenv("VDOM",ml_vdom)
		LET ml_vdom_set = TRUE		
	END IF
END FUNCTION
